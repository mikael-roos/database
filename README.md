Course database
==================

Course material for a database course.

Online pages starts at:

* https://mikael-roos.gitlab.io/database/

<!--
TODO

* Från kmom05 i webtec, databas om namn, för att komma igång med enklare delar i en databas. Kan vara en bra start i en databaskurs.

* java recept-databas, kan vara en enklare start på crud. Kan även utvecklas vidare till en eshop.

-->