--
-- Create a user with full grants.
--
DROP USER IF EXISTS 'dbadm'@'localhost';
DROP USER IF EXISTS 'dbadm'@'%';

CREATE USER 'dbadm'@'localhost' IDENTIFIED BY 'P@ssw0rd';
CREATE USER 'dbadm'@'%'         IDENTIFIED BY 'P@ssw0rd';

GRANT ALL PRIVILEGES ON *.* TO 'dbadm'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'dbadm'@'%'         WITH GRANT OPTION;

FLUSH PRIVILEGES;

SELECT USER();

SHOW GRANTS \G;
