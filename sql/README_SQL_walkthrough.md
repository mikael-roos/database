Walkthrough of SQL commands
================================


SELECT
--------------------------------

### SQL DQL (Data Wuery Language)

SELECT
FROM table
WHERE
GROUP BY
HAVING
ORDER BY

SELECT DISTINCT (only unique rows)


INSERT, UPDATE, DELETE
--------------------------------

### SQL DML (Data Manipulation Language)

INSERT INTO table
VALUES
(1,2,3),
(1,2,3)

DELETE FROM table
WHERE

UPDATE table
SET
    column = 'value',
    column = 'value'
WHERE


CREATE, DROP, ALTER
--------------------------------

### SQL DDL (Data Definition Language)

Create the schema of the databases.

DROP TABLE
ALTER TABLE
CREATE TABLE table1 (
    col_id INT AUTO_INCREMENT PRIMARY KEY,

    column1 datatype restrictions NOT NULL DEFAULT 42,
    column2 datatype,
    column3 datatype UNIQUE,

    PRIMARY KEY (column1, column2),

    FOREIGN KEY column2 REFERENCES (table2.column2),

    KEY column1 -- index
)

CREATE TABLE table2 (
    column2 INT AUTO_INCREMENT PRIMARY KEY
)

DROP VIEW
ALTER VIEW
CREATE VIEW AS


INDEX
------------------------

-- 1 rows in resultset
-- 1 rows processed
SELECT * FROM table2 WHERE column2 = 42

-- 1000 rows in resultset
-- 1000 rows processed
SELECT * FROM table1

-- 1 rows in resultset
-- 1000 rows processed
-- 1 rows processed UNIQUE (INDEX)
SELECT * FROM table1
WHERE column3 = 42


JOIN
------------------------

SELECT *
FROM table1 AS t1
    INNER JOIN table2 AS t2
        ON t1.column2 = t2.column2
;



LAST THINGS
------------------------

VIEW
Subquery
UNION
OUTER JOIN

JOIN = INNER JOIN
LEFT JOIN = LEFT OUTER JOIN
RIGHT JOIN = RIGHT OUTER JOIN
