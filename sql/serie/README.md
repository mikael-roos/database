---
revision: 
    "2024-01-17": "(A, mos) Moved from another document and improved."
---
Game serie
==============================

![work](.img/work.png)

Get material from old lab on creating a game serie.

[[_TOC_]]



<!--
Video
------------------------------

This is a recorded presentation, 12 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/tELvRoiho9I/0.jpg)](https://www.youtube.com/watch?v=tELvRoiho9I)
-->



Prepare
------------------------------

You have installed MariaDB or MySQL and you have acces to the database using a terminal client or a desktop client.

