--
-- Use the database classicmodels from MySQL Tutorial to show examples on
-- JOIN, UNION and sub query
-- https://www.mysqltutorial.org/mysql-sample-database.aspx
--
SHOW DATABASES;

USE classicmodels;

-- SELECT FROM
-- JOIN ON
-- WHERE
-- GROUP BY
-- HAVING
-- ORDER BY


-- [INNER] JOIN
-- LEFT|RIGHT [OUTER] JOIN
-- FULL OUTER JOIN
-- UNION [ALL | DISTINCT]
-- SELF JOIN
-- NATURAL JOIN
-- Sub query
-- GROUP BY - HAVING
-- Tripple & more join

-- Show some content from the tables
SELECT * FROM customers;
SELECT * FROM employees;
SELECT * FROM orders;
SELECT * FROM products;
SELECT * FROM productlines;

-- [INNER] JOIN
-- Show customers and the name of their sales responsible contact.
SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid
FROM customers
;

SELECT
	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM employees
;

SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	INNER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
;

SELECT
	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM employees
;



-- LEFT|RIGHT OUTER JOIN
SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	LEFT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
;

SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	RIGHT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
;


-- Show the sales reps without a customer relation.
SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	RIGHT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
WHERE
	jobTitle = 'Sales Rep'
    AND customerName IS NULL
;

-- Show customers without sales rep.
SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	LEFT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
WHERE
	salesRepEmployeeNumber IS NULL
;


-- LEFT|RIGHT [OUTER] JOIN
-- FULL OUTER JOIN
-- UNION

-- Show the sales reps without a customer relation.
SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	RIGHT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
-- WHERE
	-- jobTitle = 'Sales Rep'
    -- AND customerName IS NULL
;

-- Show customers without sales rep.
SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	LEFT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
-- WHERE
	-- salesRepEmployeeNumber IS NULL
;

-- FULL OUTER JOIN
-- Show the sales reps without a customer relation.
(SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	RIGHT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
-- WHERE
	-- jobTitle = 'Sales Rep'
    -- AND customerName IS NULL
)

UNION

-- Show customers without sales rep.
(SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	LEFT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
-- WHERE
	-- salesRepEmployeeNumber IS NULL
)
;


-- FULL OUTER JOIN
-- Show the sales reps without a customer relation.
(SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	RIGHT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
-- WHERE
	-- jobTitle = 'Sales Rep'
    -- AND customerName IS NULL
)

UNION

-- Show customers without sales rep.
(SELECT
	customerName AS 'Customer name',
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Customer contact person',
    salesRepEmployeeNumber AS salesrepid,
   	CONCAT(firstName, ' ', lastName) AS Name,
	employeeNumber AS 'Employee id',
    jobTitle AS Title
FROM customers AS c
	LEFT OUTER JOIN employees AS e
		ON e.employeeNumber = c.salesRepEmployeeNumber
-- WHERE
	-- salesRepEmployeeNumber IS NULL
)
;



-- UNION [ALL | DISTINCT]
SELECT
    CONCAT(contactFirstName, ' ', contactLastName) AS 'Name'
FROM customers
;

SELECT
   	CONCAT(firstName, ' ', lastName) AS 'Name'
FROM employees
;

(SELECT
    -- CONCAT(contactFirstName, ' ', contactLastName) AS 'Name'
    CONCAT(contactFirstName) AS 'Name'
FROM customers)

UNION

(SELECT
   	-- CONCAT(firstName, ' ', lastName) AS 'Name'
   	CONCAT(firstName) AS 'Name'
FROM employees)

ORDER BY Name
;


-- SELF JOIN
SELECT * FROM employees;
SELECT
	e.employeeNumber AS 'Employee id',
   	CONCAT(e.firstName, ' ', e.lastName) AS 'Name',
    e.jobTitle AS 'Title',
    e.reportsTo AS 'Manager id',
	CONCAT(manager.firstName, ' ', manager.lastName) AS 'Manager name',
	manager.jobTitle AS 'Manager title'
FROM employees AS e
	INNER JOIN employees AS manager
		-- ON e.employeeNumber = manager.reportsTo
        ON e.reportsTo = manager.employeeNumber
ORDER BY
	CONCAT(manager.firstName, ' ', manager.lastName) ASC
;







-- Sub query

-- HAVING?
-- Which salesperson has sold the most in last year
-- What are the most sold products in number or order value?

-- TRIPLE JOIN (order -> products)
-- Show all details for an order
