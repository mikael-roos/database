---
revision: 
    "2024-01-30": "(A, mos) Moved from another document, rewritten and enhanced."
---
Classic models database: Get going with UNION, JOIN and subquery
==============================

![done](.img/done.png)

This exercise is about get going with the classic models database and how to use SQL to inspect it. You will see how the backup file is imported to create a database and SQL is used to inspect the database. We use SELECT statements to build up reports and we see how to join two and three tables and how union differ with join.

Perform the exercises and save all your SQL to a file.

[[_TOC_]]



Prepare
------------------------------

You have installed MariaDB or MySQL and you have access to the database using a terminal client or a desktop client.

The SQL code used in this exercise is available in the file [`dml.sql`](./dml.sql), use it as a way to view suggestions on solutions.



The classic models database
------------------------------

You can read about the [classicmodels database](https://www.mysqltutorial.org/getting-started-with-mysql/mysql-sample-database/). It is a database example that is commonly used to practice the basics of working with a database.

You should have access to the backup file of the world database which can be downloaded from the above site.

This repo includes a copy of the backup file at [`database/classicalmodels.sql`](../../database/world.sql).



Import the database
------------------------------

Create a working directory and ensure that you save all the SQL you write into the file `dml.sql`.

Start by importing the backup of the database. You can place the backupfile in your working directory and label it `classicmodels.sql`.

Open up the backup file in your texteditor and inspect it. Do you see it contains SQL statement and that it starts to create a database called `classicmodels`?

Now proceed to import the database. Either from the terminal.

```
mariadb < classicmodels.sql
```

Or from within the terminal client.

```sql
MariaDB [classicmodels]> source classicmodels.sql
```

You can verify that it works by checking what tables you have in the database.

```sql
SHOW TABLES;
```

It should look something like this.

```
+-------------------------+
| Tables_in_classicmodels |
+-------------------------+
| customers               |
| employees               |
| offices                 |
| orderdetails            |
| orders                  |
| payments                |
| productlines            |
| products                |
+-------------------------+
```



Verify the database schema and the content of the tables
------------------------------

When you first meet a new database you might want to know the structure of the schema. You can view that by checking how the tables were created and what they contain.

Run the following statements to inspect a few tables.

```sql
SHOW CREATE TABLE `customers` \G
SHOW CREATE TABLE `employees` \G
```

It might be helpful to save the output from those commands into a separate file, it can speed up to find the different table and column names when you work with the database.

The check what type of data that is included in the tables. Execute the following statements for the customers and employees table.

```sql
SELECT * FROM `customers` LIMIT 5;
SELECT COUNT(`customerNumber`) FROM `customers` LIMIT 5;
```

These small statements might give you a hint on what the database contains and hw the data looks like.

Save all SQL statements into your file. You might need them later on.

This far we can say the following of the tables in the database.

```
* customers: stores customer’s data.
* employees: stores employee information and the organization structure such as who reports to whom.
* offices:
* orderdetails:
* orders:
* payments:
* productlines:
* products:
```

These types of oneliners, explaining the purpose of each table, is quite useful as a documentation of a database.



Reverse engineer the database model
------------------------------

If you have access to the desktop client Workbench, then you can try to extract the database model by reverse engineer the database schema and get a picture of how the database model looks like.

![classicalmodels model](.img/er.png)

_Figure. The database model for the classicalmodels database, reversed engineered by Workbench._

You can see that Workbench detected how the tables are organised and linked to each other. This image might be useful when you now starts to work with the database, you can see the table names and the names of the columns.



UNION - Create a phone list to customers and employees
------------------------------

The company wants to create a phone list to all the customers and to all the employees and they have asked you to create a report looking like this.

First the employees that only has an internal extension.

```
+-------------------+-------+
| Name              | Phone |
+-------------------+-------+
| Diane Murphy      | x5800 |
| Mary Patterson    | x4611 |
| Jeff Firrelli     | x9273 |
...
```

Then the customers representative with their phone.

```
+-----------------+--------------+
| Name            | Phone        |
+-----------------+--------------+
| Carine  Schmitt | 40.32.2555   |
| Jean King       | 7025551838   |
| Peter Ferguson  | 03 9520 4555 |
...  and the rest
```

Right after you deliver the two reports to your manager they ask you to consolidate all into one single report and order it by the name.

This is where UNION might come in handy, it can be used to union several separate reports into one single report. The requirement is that all parts has the same column names.

This is how to perform a UNION from two different SELECT statements.

```sql
(
    SELECT
        CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
        `Extension` AS 'Phone'
    FROM `employees`
)
UNION
(
    SELECT
        CONCAT(`contactFirstName`, ' ', `contactLastName`) AS 'Name',
        `phone` AS 'Phone'
    FROM `customers`
)
ORDER BY
    `Name`
;
```

You can see it as two separate reports that are unioned together.

The result can look like this if we limit the output.

```
+--------------------+-----------------+
| Name               | Phone           |
+--------------------+-----------------+
| Adrian Huxley      | +61 2 9495 8555 |
| Akiko Shimamura    | +81 3 3584 0555 |
| Alejandra  Camino  | (91) 745 6555   |
| Alexander  Feuer   | 0342-555176     |
| Alexander  Semenov | +7 812 293 0521 |
| Allen Nelson       | 6175558555      |
| Andy Fixter        | x101            |
+--------------------+-----------------+
```



SELECT INTO OUTFILE as CSV
------------------------------

**Beware. The exported files are saved on the database host and not the host of the client.**

<!--
* Remove this, might not work in wsl?
-->

The manager seems satisfied but now they want to include the details into an excel sheet and they are asking for a .csv file. That is a comma spearated value file that is easy to import into an excel sheet or other applications.

You dig into the manual and finds [SELECT INTO OUTFILE](https://mariadb.com/kb/en/select-into-outfile/).

You start to create a view of the phone-report, just to make it easier to work with. The result could look like this.

```
MariaDB [classicmodels]> SELECT * FROM `phonelist` LIMIT 5;
+--------------------+-----------------+
| Name               | Phone           |
+--------------------+-----------------+
| Adrian Huxley      | +61 2 9495 8555 |
| Akiko Shimamura    | +81 3 3584 0555 |
| Alejandra  Camino  | (91) 745 6555   |
| Alexander  Feuer   | 0342-555176     |
| Alexander  Semenov | +7 812 293 0521 |
+--------------------+-----------------+
```

After some work with the manual you find out that the csv file can be created like this.

```sql
SELECT 
    * 
INTO OUTFILE '/tmp/phonelist.csv'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
FROM `phonelist`
;
```

Depending on what grants you database user has, you might need to add some grants to allow for exporting to a file. I am using the user maria and it can connect from both localhost and any host (%) so I grant to both users.

```sql
GRANT FILE ON *.* TO `maria`@'localhost';
GRANT FILE ON *.* TO `maria`@'%';
```

It looks like this when you execute the statement.

```
MariaDB [classicmodels]> SELECT 
    ->     * 
    -> INTO OUTFILE '/tmp/phonelist.csv'
    ->     FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    ->     LINES TERMINATED BY '\n'
    -> FROM `phonelist`
    -> ;
Query OK, 145 rows affected (0,001 sec)
```

Then enter the terminal and write the terminal command `head -5 /tmp/phonelist.csv` to view what the file contains (or open the file in your text editor).

```
$ head -5 /tmp/phonelist.csv 
"Adrian Huxley","+61 2 9495 8555"
"Akiko Shimamura","+81 3 3584 0555"
"Alejandra  Camino","(91) 745 6555"
"Alexander  Feuer","0342-555176"
"Alexander  Semenov","+7 812 293 0521"
```

The file that was exported is actually owned by the same user that runs the database server.

This file is a csv file which can be imported into an excel sheet.

```
$ ls -l /tmp/phonelist.csv 
-rw-r--r-- 1 mysql mysql 4,3K jan 23 12:09 /tmp/phonelist.csv
```

You need to use sudo to remove the file.

```
sudo rm /tmp/phonelist.csv
```



JOIN employees with the office
------------------------------

The employees are connected to an office. In the ER diagram this relation is displayed like this.

![employees-office](.img/employee-offices.png)

_Figure. The relation between offices and employees._

Looking at the tables we notice the following two keys that might connect the two tables.

* employees:officeCode (foreign key)
* offices:officeCode (primary key)

Lets review the content of the tables.

```sql
SELECT 
    `officeCode`,
    `city`,
    `territory`
FROM `offices`
;
```

Executing the statement and we see that it appears to be 7 offices, spread over the world and the code seems to be a number between 1 and 7.

```
MariaDB [classicmodels]> SELECT 
    ->     `officeCode`,
    ->     `city`,
    ->     `territory`
    -> FROM `offices`
    -> ;
+------------+---------------+-----------+
| officeCode | city          | territory |
+------------+---------------+-----------+
| 1          | San Francisco | NA        |
| 2          | Boston        | NA        |
| 3          | NYC           | NA        |
| 4          | Paris         | EMEA      |
| 5          | Tokyo         | Japan     |
| 6          | Sydney        | APAC      |
| 7          | London        | EMEA      |
+------------+---------------+-----------+
```

Lets review the related content of the employees table.

```sql
SELECT COUNT(`employeeNumber`) FROM `employees`;

SELECT 
    `firstName`,
    `lastName`,
    `officeCode`
FROM `employees`
LIMIT 10
;
```

There seems to be 23 employees.

```
MariaDB [classicmodels]> SELECT COUNT(`employeeNumber`) FROM `employees`;
+-------------------------+
| COUNT(`employeeNumber`) |
+-------------------------+
|                      23 |
+-------------------------+
```

They seems to be spread over various offices.

```
MariaDB [classicmodels]> SELECT 
    ->     `firstName`,
    ->     `lastName`,
    ->     `officeCode`
    -> FROM `employees`
    -> LIMIT 10
    -> ;
+-----------+-----------+------------+
| firstName | lastName  | officeCode |
+-----------+-----------+------------+
| Diane     | Murphy    | 1          |
| Mary      | Patterson | 1          |
| Jeff      | Firrelli  | 1          |
| William   | Patterson | 6          |
| Gerard    | Bondur    | 4          |
| Anthony   | Bow       | 1          |
| Leslie    | Jennings  | 1          |
| Leslie    | Thompson  | 1          |
| Julie     | Firrelli  | 2          |
| Steve     | Patterson | 2          |
+-----------+-----------+------------+
```

Lets join the two tables to create a report to see what employee is working at what office in what territory.

Try to create the SQL query on your own and the result can look like this. My solution is available if you scroll further down. Try to create exactly the same report.

```
+-----------+-----------+---------------------+---------------+-----------+
| firstName | lastName  | jobTitle            | city          | territory |
+-----------+-----------+---------------------+---------------+-----------+
| Gerard    | Bondur    | Sale Manager (EMEA) | Paris         | EMEA      |
| Loui      | Bondur    | Sales Rep           | Paris         | EMEA      |
| Larry     | Bott      | Sales Rep           | London        | EMEA      |
| Anthony   | Bow       | Sales Manager (NA)  | San Francisco | NA        |
| Pamela    | Castillo  | Sales Rep           | Paris         | EMEA      |
| Jeff      | Firrelli  | VP Marketing        | San Francisco | NA        |
| Julie     | Firrelli  | Sales Rep           | Boston        | NA        |
| Andy      | Fixter    | Sales Rep           | Sydney        | APAC      |
| Martin    | Gerard    | Sales Rep           | Paris         | EMEA      |
| Gerard    | Hernandez | Sales Rep           | Paris         | EMEA      |
+-----------+-----------+---------------------+---------------+-----------+
```

So, we got a nice report saying at what office each employee works. Control that you get 23 employees in your report.

This was my way to solve the report above.

```sql
SELECT 
    e.`firstName`,
    e.`lastName`,
    e.`jobTitle`,
    o.`city`,
    o.`territory`
FROM `employees` AS e
    INNER JOIN `offices` AS o
        ON e.`officeCode` = o.`officeCode`
ORDER BY 
    `lastName`,
    `firstname`
;
```



JOIN employees with the boss
------------------------------

If you look at the ER diagram for the table employees you will find a recursive relation that points back to the table. What can that be?

![employee-boss](.img/employee-boss.png)

_Figure. A recursive relation on top of the table._

This relation seems to be valid by the column `reportsTo`. Lets investigate it.

```sql
SELECT 
    `employeeNumber`,
    `firstName`,
    `lastName`,
    `jobTitle`,
    `reportsTo`
FROM `employees`
LIMIT 10
;
```

The result seems to be that an employee reports to another employee. The President reports to none. The VP Sales and VP Marketing both reports to the President. There seems to be some logic in the relation. 

```
+----------------+-----------+-----------+----------------------+-----------+
| employeeNumber | firstName | lastName  | jobTitle             | reportsTo |
+----------------+-----------+-----------+----------------------+-----------+
|           1002 | Diane     | Murphy    | President            |      NULL |
|           1056 | Mary      | Patterson | VP Sales             |      1002 |
|           1076 | Jeff      | Firrelli  | VP Marketing         |      1002 |
|           1088 | William   | Patterson | Sales Manager (APAC) |      1056 |
|           1102 | Gerard    | Bondur    | Sale Manager (EMEA)  |      1056 |
|           1143 | Anthony   | Bow       | Sales Manager (NA)   |      1056 |
|           1165 | Leslie    | Jennings  | Sales Rep            |      1143 |
|           1166 | Leslie    | Thompson  | Sales Rep            |      1143 |
|           1188 | Julie     | Firrelli  | Sales Rep            |      1143 |
|           1216 | Steve     | Patterson | Sales Rep            |      1143 |
+----------------+-----------+-----------+----------------------+-----------+
```

We can investigate if there is a foreign key constraint in the table, that will provide further details on if there is an actual match.

```sql
SHOW CREATE TABLE employees \G
```

The part we are interested in is the foreign key constraints.

```
CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`reportsTo`) REFERENCES `employees` (`employeeNumber`),
CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`officeCode`) REFERENCES `offices` (`officeCode`)
```

The first one shows that the column `reportsTo` references the column `employeeNumber` in the table `employees`.

The second one shows the relation that `officeCode` references the column `officeCode` in the table `offices`.

If is not mandatory to set constraints for a foreign key relation, but is helpful when investigating an existing database and the database engine can use the constraints to help keeping the data in the database in order.

So, now we can extend our report of employee and office to also include the manager that each employee reports to. That will be a tripple join and try to create the report like this. Try to create the statement on your own. My solution is further down.

The trick when joining the same table twice is to use `AS` to give the tables different names in the statement.

```
+------------------+-----------+---------------+------------------------------------+
| Name             | Title     | Office        | Boss                               |
+------------------+-----------+---------------+------------------------------------+
| Larry Bott       | Sales Rep | London (EMEA) | Gerard Bondur, Sale Manager (EMEA) |
| Barry Jones      | Sales Rep | London (EMEA) | Gerard Bondur, Sale Manager (EMEA) |
| Loui Bondur      | Sales Rep | Paris (EMEA)  | Gerard Bondur, Sale Manager (EMEA) |
| Pamela Castillo  | Sales Rep | Paris (EMEA)  | Gerard Bondur, Sale Manager (EMEA) |
| Martin Gerard    | Sales Rep | Paris (EMEA)  | Gerard Bondur, Sale Manager (EMEA) |
| Gerard Hernandez | Sales Rep | Paris (EMEA)  | Gerard Bondur, Sale Manager (EMEA) |
| Julie Firrelli   | Sales Rep | Boston (NA)   | Anthony Bow, Sales Manager (NA)    |
| Steve Patterson  | Sales Rep | Boston (NA)   | Anthony Bow, Sales Manager (NA)    |
| Foon Yue Tseng   | Sales Rep | NYC (NA)      | Anthony Bow, Sales Manager (NA)    |
| George Vanauf    | Sales Rep | NYC (NA)      | Anthony Bow, Sales Manager (NA)    |
+------------------+-----------+---------------+------------------------------------+
```

Great. Now we have a report showing who is boss over who.

My solution to the report was this.

```sql
SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    CONCAT(o.`city`, ' (', o.`territory`, ')') AS 'Office',
    CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`) AS 'Boss'
FROM `employees` AS e
    INNER JOIN `offices` AS o
        ON e.`officeCode` = o.`officeCode`
    INNER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
ORDER BY 
    boss.`lastName`,
    boss.`firstname`,
    o.`city`,
    e.`lastName`,
    e.`firstname`
LIMIT 10
;
```



Was an employee missing from the report?
------------------------------

If you check your report from above once more, how many rows does it have and how many employees was it? Right, 22 versus 23 and it seems that some employee is missing from the report of employees and bosses.

Can you find which is the employee that is missing from the report?

Well, it is actually the President. Lets see what details we have on that individual.

```
MariaDB [classicmodels]> SELECT 
    ->     CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    ->     e.`jobTitle` AS 'Title',
    ->     e.`reportsTo`
    -> FROM `employees` AS e
    -> WHERE
    ->     e.`jobTitle` = 'President'
    -> ;
+--------------+-----------+-----------+
| Name         | Title     | reportsTo |
+--------------+-----------+-----------+
| Diane Murphy | President |      NULL |
+--------------+-----------+-----------+
```

Ah, ok. There is a NULL value in the column reportsTo. The President reports to none.

When we do a join we only get rows that match on the ON condition.

```sql
FROM `employees` AS e
    INNER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
```

In this case we do not get a match and as the result the President is left out of the report.

To our aid comes OUTER JOIN that also includes the rows that do not match, or rows that has a NULL value.

There are two ways to do OUTER JOIN.

1. LEFT OUTER JOIN (for all rows in the left table)
1. RIGHT OUTER JOIN (for all rows in the right table)

The difference is what table (left or right) that is displaying all its rows.

To update the statement we change the join to a LEFT OUTER JOIN saying to show all rows from the employee table, even if it does not have a boss (null value in the column reportsTo).

This is how to write a OUTER JOIN.

```sql
SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`) AS 'Boss'
FROM `employees` AS e
    LEFT OUTER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
ORDER BY 
    boss.`lastName`,
    boss.`firstname`,
    e.`lastName`,
    e.`firstname`
LIMIT 10
;
```

Now the results shows all the rows in the employee table, even if it does not have a match in the ON condition.

```
+------------------+-----------+------------------------------------+
| Name             | Title     | Boss                               |
+------------------+-----------+------------------------------------+
| Diane Murphy     | President | NULL                               |
| Loui Bondur      | Sales Rep | Gerard Bondur, Sale Manager (EMEA) |
| Larry Bott       | Sales Rep | Gerard Bondur, Sale Manager (EMEA) |
| Pamela Castillo  | Sales Rep | Gerard Bondur, Sale Manager (EMEA) |
| Martin Gerard    | Sales Rep | Gerard Bondur, Sale Manager (EMEA) |
| Gerard Hernandez | Sales Rep | Gerard Bondur, Sale Manager (EMEA) |
| Barry Jones      | Sales Rep | Gerard Bondur, Sale Manager (EMEA) |
| Julie Firrelli   | Sales Rep | Anthony Bow, Sales Manager (NA)    |
| Leslie Jennings  | Sales Rep | Anthony Bow, Sales Manager (NA)    |
| Steve Patterson  | Sales Rep | Anthony Bow, Sales Manager (NA)    |
+------------------+-----------+------------------------------------+
```

TO finalize the report we can remove the output of NULL using the function COALESCE that gets the first non null value from a sequence of values. We can update the statement to replace the NULL value with an empty string.

```sql
    COALESCE(CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`), '') AS 'Boss'
```

It looks like this now.

```
+------------------+-----------+------------------------------------+
| Name             | Title     | Boss                               |
+------------------+-----------+------------------------------------+
| Diane Murphy     | President |                                    |
```

Well, it was a rather long expressen, but the the function COALESCE works like this.

```
SELECT COALESCE(col1, col2, expression1, expression2, col3);
```

The first value that is not null will be displayed.

You can off course do a triple join and mix inner and outer join. Try to create a statement that creates this report.

```
+------------------+-----------+--------------------+------------------------------------+
| Name             | Title     | Office             | Boss                               |
+------------------+-----------+--------------------+------------------------------------+
| Diane Murphy     | President | San Francisco (NA) |                                    |
| Larry Bott       | Sales Rep | London (EMEA)      | Gerard Bondur, Sale Manager (EMEA) |
| Barry Jones      | Sales Rep | London (EMEA)      | Gerard Bondur, Sale Manager (EMEA) |
| Loui Bondur      | Sales Rep | Paris (EMEA)       | Gerard Bondur, Sale Manager (EMEA) |
| Pamela Castillo  | Sales Rep | Paris (EMEA)       | Gerard Bondur, Sale Manager (EMEA) |
| Martin Gerard    | Sales Rep | Paris (EMEA)       | Gerard Bondur, Sale Manager (EMEA) |
| Gerard Hernandez | Sales Rep | Paris (EMEA)       | Gerard Bondur, Sale Manager (EMEA) |
| Julie Firrelli   | Sales Rep | Boston (NA)        | Anthony Bow, Sales Manager (NA)    |
| Steve Patterson  | Sales Rep | Boston (NA)        | Anthony Bow, Sales Manager (NA)    |
| Foon Yue Tseng   | Sales Rep | NYC (NA)           | Anthony Bow, Sales Manager (NA)    |
+------------------+-----------+--------------------+------------------------------------+
```



Subquery
------------------------------

A sub query is a query within another query. You can nest as many queries as you wish by wrapping paranthesis around the query.

This is a rouch example on how you combine subqueries.

```sql
SELECT
    * 
FROM ( subquery ) AS q1
    INNER JOIN ( subquery ) AS q2
        on q1.col1 = q2.col2
WHERE
    id IN ( subquery )
;
```

To show how it works we can find the employees that is directly reporting to the President.

First we find the employeeNumber of the President.

```sql
SELECT 
    `employeeNumber`
FROM `employees`
WHERE
     `jobTitle` = 'President'
;
```

The result is like this.

```
+----------------+
| employeeNumber |
+----------------+
|           1002 |
+----------------+
```

The we write another query to find out who is reporting directly to the President, like this.

First we hard code the employeeNumber to 1002.

```sql
SELECT 
    `employeeNumber`,
    CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
    `jobTitle` AS 'Title',
    `reportsTo`
FROM `employees`
WHERE
    `reportsTo` = 1002
;
```

The result comes out like this. It appears to be two employees that report directly to the President.

```
+----------------+----------------+--------------+-----------+
| employeeNumber | Name           | Title        | reportsTo |
+----------------+----------------+--------------+-----------+
|           1056 | Mary Patterson | VP Sales     |      1002 |
|           1076 | Jeff Firrelli  | VP Marketing |      1002 |
+----------------+----------------+--------------+-----------+
```

Now we use a subquery to make the statement less hard coded. This i s a matter of combining the two queries.

```sql
SELECT 
    `employeeNumber`,
    CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
    `jobTitle` AS 'Title',
    `reportsTo`
FROM `employees`
WHERE
    `reportsTo` = (
        SELECT 
            `employeeNumber`
        FROM `employees`
        WHERE
            `jobTitle` = 'President'
    )
;
```

If you run the query you will see it provides the same result.

The subquery above is returning only one value. Lets change it so it returns several values by checking who reports to the VP Sales and VP marketing.

The change is related to the part with the subquery, like this.

```sql
WHERE
    `reportsTo` IN (
        SELECT 
            `employeeNumber`
        FROM `employees`
        WHERE
            `jobTitle` LIKE 'VP%'
    )
```

Combine the query and you will get this result. It seems like none is reporting to the VP Marketing.

```
+----------------+-------------------+----------------------+-----------+
| employeeNumber | Name              | Title                | reportsTo |
+----------------+-------------------+----------------------+-----------+
|           1088 | William Patterson | Sales Manager (APAC) |      1056 |
|           1102 | Gerard Bondur     | Sale Manager (EMEA)  |      1056 |
|           1143 | Anthony Bow       | Sales Manager (NA)   |      1056 |
|           1621 | Mami Nishi        | Sales Rep            |      1056 |
+----------------+-------------------+----------------------+-----------+
```

A subquery can "return" one or several values and it can return entire rows. Here follows an example on a subquery that returns a set of rows.

```sql
SELECT
    `employeeNumber`,
    `jobTitle`
FROM (
    SELECT 
        * 
    FROM `employees`
    WHERE
        `jobTitle` LIKE 'VP%'
) AS t1
;
```

When a subquery is used in the FROM or JOIN part it might need a table name that you give it using AS.

Sometimes a join can achive the same thing as a subquery. It is therefor possible to rewrite your statement to use either subqueries or joins. Let the situation decide what you choose. Your query might look different written in join or as a sub query.

There might be various ways to achive the wanted result.



Summary
------------------------------

You have worked with a large example of a database called classical models and you have investigated parts of it and how to create various reports by combining the content of several tables using UNION, INNER and OUTER JOINS and subqueries.

