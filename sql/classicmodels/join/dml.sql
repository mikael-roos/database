--
-- Work with the classic models database
--

--
-- Import the database
--
SOURCE classic_models.sql;

SHOW TABLES;

SHOW CREATE TABLE `customers` \G
SHOW CREATE TABLE `employees` \G

SELECT * FROM `customers` LIMIT 5;
SELECT COUNT(`customerNumber`) FROM `customers` LIMIT 5;

SELECT * FROM `employees` LIMIT 5;
SELECT COUNT(`employeeNumber`) FROM `employees` LIMIT 5;



--
-- UNION mail list
--
SELECT
    CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
    `Extension` AS 'Phone'
FROM `employees`
;

SELECT
    CONCAT(`contactFirstName`, ' ', `contactLastName`) AS 'Name',
    `phone` AS 'Phone'
FROM `customers`
;

(
    SELECT
        CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
        `Extension` AS 'Phone'
    FROM `employees`
)
UNION
(
    SELECT
        CONCAT(`contactFirstName`, ' ', `contactLastName`) AS 'Name',
        `phone` AS 'Phone'
    FROM `customers`
)
ORDER BY
    `Name`
LIMIT 7
;



--
-- SELECT INTO OUTFILE
--
DROP VIEW IF EXISTS `phonelist`;
CREATE VIEW `phonelist` 
AS
(
    SELECT
        CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
        `Extension` AS 'Phone'
    FROM `employees`
)
UNION
(
    SELECT
        CONCAT(`contactFirstName`, ' ', `contactLastName`) AS 'Name',
        `phone` AS 'Phone'
    FROM `customers`
)
ORDER BY
    `Name`
;

SELECT * FROM `phonelist` LIMIT 5;

GRANT FILE ON *.* TO `maria`@'localhost';
GRANT FILE ON *.* TO `maria`@'%';

SELECT 
    * 
INTO OUTFILE '/tmp/phonelist.csv'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
FROM `phonelist`
;



--
-- JOIN employees with the office
--
SELECT COUNT(`officeCode`) FROM `offices`;
SELECT 
    `officeCode`,
    `city`,
    `territory`
FROM `offices`
;

SELECT COUNT(`employeeNumber`) FROM `employees`;
SELECT 
    `firstName`,
    `lastName`,
    `officeCode`
FROM `employees`
LIMIT 10
;

SELECT 
    e.`firstName`,
    e.`lastName`,
    e.`jobTitle`,
    o.`city`,
    o.`territory`
FROM `employees` AS e
    INNER JOIN `offices` AS o
        ON e.`officeCode` = o.`officeCode`
ORDER BY 
    `lastName`,
    `firstname`
LIMIT 10
;



--
-- JOIN employees with the boss
--
SELECT 
    `employeeNumber`,
    `firstName`,
    `lastName`,
    `jobTitle`,
    `reportsTo`
FROM `employees`
LIMIT 10
;

SHOW CREATE TABLE employees \G

SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    CONCAT(o.`city`, ' (', o.`territory`, ')') AS 'Office',
    CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`) AS 'Boss'
FROM `employees` AS e
    INNER JOIN `offices` AS o
        ON e.`officeCode` = o.`officeCode`
    INNER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
ORDER BY 
    boss.`lastName`,
    boss.`firstname`,
    o.`city`,
    e.`lastName`,
    e.`firstname`
LIMIT 10
;



--
-- Was an employee missing from the report?
--
SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    e.`reportsTo`
FROM `employees` AS e
WHERE
    e.`jobTitle` = 'President'
;

SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`) AS 'Boss'
FROM `employees` AS e
    LEFT OUTER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
ORDER BY 
    boss.`lastName`,
    boss.`firstname`,
    e.`lastName`,
    e.`firstname`
LIMIT 10
;

SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    COALESCE(CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`), '') AS 'Boss'
FROM `employees` AS e
    LEFT OUTER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
ORDER BY 
    boss.`lastName`,
    boss.`firstname`,
    e.`lastName`,
    e.`firstname`
LIMIT 10
;

SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    CONCAT(o.`city`, ' (', o.`territory`, ')') AS 'Office',
    COALESCE(CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`), '') AS 'Boss'
FROM `employees` AS e
    INNER JOIN `offices` AS o
        ON e.`officeCode` = o.`officeCode`
    LEFT OUTER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
ORDER BY 
    boss.`lastName`,
    boss.`firstname`,
    o.`city`,
    e.`lastName`,
    e.`firstname`
LIMIT 10
;


--
-- Subquery
--
SELECT
    * 
FROM ( subquery ) AS q1
    INNER JOIN ( subquery ) AS q2
        on q1.col1 = q2.col2
WHERE
    id IN ( subquery )
;

SELECT 
    `employeeNumber`
FROM `employees`
WHERE
     `jobTitle` = 'President'
;

SELECT 
    `employeeNumber`,
    CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
    `jobTitle` AS 'Title',
    `reportsTo`
FROM `employees`
WHERE
    `reportsTo` = 1002
;

SELECT 
    `employeeNumber`,
    CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
    `jobTitle` AS 'Title',
    `reportsTo`
FROM `employees`
WHERE
    `reportsTo` = (
        SELECT 
            `employeeNumber`
        FROM `employees`
        WHERE
            `jobTitle` = 'President'
    )
;

SELECT 
    `employeeNumber`,
    CONCAT(`firstName`, ' ', `lastName`) AS 'Name',
    `jobTitle` AS 'Title',
    `reportsTo`
FROM `employees`
WHERE
    `reportsTo` IN (
        SELECT 
            `employeeNumber`
        FROM `employees`
        WHERE
            `jobTitle` LIKE 'VP%'
    )
;

SELECT
    `employeeNumber`,
    `jobTitle`
FROM (
    SELECT 
        * 
    FROM `employees`
    WHERE
        `jobTitle` LIKE 'VP%'
) AS t1
;
