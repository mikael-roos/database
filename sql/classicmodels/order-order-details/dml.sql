--
-- Work with the classic models database through order and orderdetails
--

--
-- Show sales staff
--
SELECT 
    CONCAT(e.`firstName`, ' ', e.`lastName`) AS 'Name',
    e.`jobTitle` AS 'Title',
    COALESCE(CONCAT(boss.`firstName`, ' ', boss.`lastName`, ', ', boss.`jobTitle`), '') AS 'Boss'
FROM `employees` AS e
    LEFT OUTER JOIN `employees` AS boss
        ON e.`reportsTo` = boss.`employeeNumber`
WHERE
    e.`jobTitle` = 'Sales Rep'
ORDER BY 
    boss.`lastName`,
    boss.`firstname`,
    e.`lastName`,
    e.`firstname`
-- LIMIT 10
;



--
-- Show customers
--
SELECT COUNT(`customerNumber`) FROM customers;

SELECT
    c.`customerName`,
    c.`country`,
    c.`salesRepEmployeeNumber`
FROM customers As c
LIMIT 10
;



--
-- Highest order value
--
SELECT COUNT(`orderNumber`) FROM orders;

SELECT
    o.`orderNumber`,
    o.`orderDate`,
    ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'Sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
GROUP BY
    o.`orderNumber`
ORDER BY
    Sum -- DESC
LIMIT 10
;



--
-- Customer buying the most
--
SELECT
    c.`customerNumber`,
    c.`customerName`,
    c.`country`,
    -- o.`orderNumber`,
    -- o.`orderDate`,
    COUNT(o.`orderNumber`) AS 'Num orders',
    ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'Sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
    INNER JOIN customers AS c
        ON c.`customerNumber` = o.`customerNumber`
GROUP BY
    c.`customerNumber`
ORDER BY
    Sum DESC
LIMIT 10
;



--
-- Sales rep with the buying customers
--
SELECT
    CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'Sales rep',
    -- c.`customerNumber`,
    c.`customerName`,
    c.`country`,
    -- o.`orderNumber`,
    -- o.`orderDate`,
    COUNT(o.`orderNumber`) AS 'Num orders',
    ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'Sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
    INNER JOIN customers AS c
        ON c.`customerNumber` = o.`customerNumber`
    INNER JOIN employees AS e
        ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
GROUP BY
    c.`customerNumber`
ORDER BY
    Sum DESC
LIMIT 10
;

SELECT
    CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'Sales rep',
    -- c.`customerNumber`,
    -- c.`customerName`,
    -- c.`country`,
    -- o.`orderNumber`,
    -- o.`orderDate`,
    COUNT(o.`orderNumber`) AS 'Num orders',
    ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'Sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
    INNER JOIN customers AS c
        ON c.`customerNumber` = o.`customerNumber`
    RIGHt OUTER JOIN employees AS e
        ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
WHERE
    e.`jobTitle` = 'Sales Rep'
GROUP BY
    e.`employeeNumber`
ORDER BY
    Sum DESC
-- LIMIT 10
;



--
-- Top salesmen by year
--
SELECT
    -- CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'Sales rep',
    -- c.`customerNumber`,
    -- c.`customerName`,
    -- c.`country`,
    -- o.`orderNumber`,
    YEAR(o.`orderDate`) AS 'Year',
    COUNT(o.`orderNumber`) AS 'Num orders',
    ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'Sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
    INNER JOIN customers AS c
        ON c.`customerNumber` = o.`customerNumber`
    -- RIGHT OUTER JOIN employees AS e
    INNER JOIN employees AS e
        ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
WHERE
    e.`jobTitle` = 'Sales Rep'
GROUP BY
    YEAR(o.`orderDate`) -- ,
    -- e.`employeeNumber`
ORDER BY
    Year DESC
-- LIMIT 10
;

SELECT
    CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'Sales rep',
    -- c.`customerNumber`,
    -- c.`customerName`,
    -- c.`country`,
    -- o.`orderNumber`,
    YEAR(o.`orderDate`) AS 'Year',
    COUNT(o.`orderNumber`) AS 'Num orders',
    ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'Sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
    INNER JOIN customers AS c
        ON c.`customerNumber` = o.`customerNumber`
    -- RIGHT OUTER JOIN employees AS e
    INNER JOIN employees AS e
        ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
WHERE
    e.`jobTitle` = 'Sales Rep'
    AND YEAR(o.`orderDate`) = 2005
GROUP BY
    YEAR(o.`orderDate`),
    e.`employeeNumber`
ORDER BY
    Sum DESC
-- LIMIT 10
;



--
-- Transpose years
--
SELECT
    CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'Sales rep',
    -- c.`customerNumber`,
    -- c.`customerName`,
    -- c.`country`,
    -- o.`orderNumber`,
    YEAR(o.`orderDate`) AS 'Year',
    -- COUNT(o.`orderNumber`) AS 'Num orders',
    ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'Sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
    INNER JOIN customers AS c
        ON c.`customerNumber` = o.`customerNumber`
    -- RIGHT OUTER JOIN employees AS e
    INNER JOIN employees AS e
        ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
WHERE
    e.`jobTitle` = 'Sales Rep'
    -- AND YEAR(o.`orderDate`) = 2005
GROUP BY
    YEAR(o.`orderDate`),
    e.`employeeNumber`
-- ORDER BY
    -- Sum DESC
-- LIMIT 10
;

SELECT
    *
FROM 
(
    SELECT
        sales_rep,
        LPAD(FORMAT(SUM(IF(the_year = '2003', the_sum, 0)), 0), 8) AS '2003',
        LPAD(FORMAT(SUM(IF(the_year = '2004', the_sum, 0)), 0), 8) AS '2004',
        LPAD(FORMAT(SUM(IF(the_year = '2005', the_sum, 0)), 0), 8) AS '2005'

    FROM
    (
        SELECT
            e.employeeNumber AS employeeNumber,
            CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'sales_rep',
            -- c.`customerNumber`,
            -- c.`customerName`,
            -- c.`country`,
            -- o.`orderNumber`,
            YEAR(o.`orderDate`) AS 'the_year',
            -- COUNT(o.`orderNumber`) AS 'Num orders',
            ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'the_sum'
        FROM orders AS o
            INNER JOIN orderdetails AS d
                ON o.`orderNumber` = d.`orderNumber`
            INNER JOIN customers AS c
                ON c.`customerNumber` = o.`customerNumber`
            RIGHT OUTER JOIN employees AS e
            -- INNER JOIN employees AS e
                ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
        WHERE
            e.`jobTitle` = 'Sales Rep'
            -- AND YEAR(o.`orderDate`) = 2005
        GROUP BY
            YEAR(o.`orderDate`),
            e.`employeeNumber`
        -- ORDER BY
            -- Sum DESC
        -- LIMIT 10
    ) AS t1
    GROUP BY
        employeeNumber
) AS t1
ORDER BY
    `2003` DESC
;

-- Clean up
SELECT
    *
FROM (
    SELECT
        sales_rep,
        LPAD(FORMAT(SUM(IF(the_year = '2003', the_sum, 0)), 0), 8) AS '2003',
        LPAD(FORMAT(SUM(IF(the_year = '2004', the_sum, 0)), 0), 8) AS '2004',
        LPAD(FORMAT(SUM(IF(the_year = '2005', the_sum, 0)), 0), 8) AS '2005'
    FROM (
        SELECT
            e.employeeNumber AS employeeNumber,
            CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'sales_rep',
            YEAR(o.`orderDate`) AS 'the_year',
            ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'the_sum'
        FROM orders AS o
            INNER JOIN orderdetails AS d
                ON o.`orderNumber` = d.`orderNumber`
            INNER JOIN customers AS c
                ON c.`customerNumber` = o.`customerNumber`
            RIGHT OUTER JOIN employees AS e
                ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
        WHERE
            e.`jobTitle` = 'Sales Rep'
        GROUP BY
            YEAR(o.`orderDate`),
            e.`employeeNumber`
    ) AS t1
    GROUP BY
        employeeNumber
) AS t1
ORDER BY
    `2003` DESC
;



--
-- Small transpose example
--
SELECT
    YEAR(o.`orderDate`) AS 'the_year',
    SUM(d.`quantityOrdered` * d.`priceEach`) AS 'the_sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
GROUP BY
    YEAR(o.`orderDate`) 
;

SELECT
    FORMAT(SUM(IF(the_year = '2003', the_sum, 0)), 0) AS '2003',
    FORMAT(SUM(IF(the_year = '2004', the_sum, 0)), 0) AS '2004',
    FORMAT(SUM(IF(the_year = '2005', the_sum, 0)), 0) AS '2005'
FROM
(
    SELECT
        YEAR(o.`orderDate`) AS 'the_year',
        SUM(d.`quantityOrdered` * d.`priceEach`) AS 'the_sum'
    FROM orders AS o
        INNER JOIN orderdetails AS d
            ON o.`orderNumber` = d.`orderNumber`
    GROUP BY
        YEAR(o.`orderDate`) 
) AS t1
;
