---
revision: 
    "2024-02-06": "(A, mos) First release."
---
Classic models database: Customer, orders and orderdetails
==============================

![work](.img/pivot.png)

This exercise is about a quest of joining tables to find out the answer to a question of "Who is the top salesperson".

The database is the classic models database and you will review how to join tables to find out the answer to the questions.

At the end you will practice the concept of pivot tables, or transposing rows into a table like (spreadsheet like) output.

Perform the exercises and save all your SQL to a file.

[[_TOC_]]

<!--
TODO

* Work though it and be more verbose on each part
* Show more details on the order and order rows to visualise how an order is setup
* Show more images on each part of the ER that is the focus. Talk about the keys.

-->



Prepare
------------------------------

You have installed MariaDB or MySQL and you have access to the database using a terminal client or a desktop client.

You have access to the database classic models.

The SQL code used in this exercise is available in the file [`dml.sql`](./dml.sql), use it as a way to view suggestions on solutions.

You should have worked through the exercise [Classic models database: Get going with UNION, JOIN and subquery](../join/README.md).



ER diagram
------------------------------

The ER diagram for the classic models database looks like this.

![er](.img/er.png)

_Figure. ER diagram for classic models._


The tables we are most interested in are these:

1. employees - here are the sales staff
1. customers - each customer has a sales representative
1. orders - the customer places orders containing products
1. orderdetails - since order/products is a N:M relation we need a connection table
1. products - the product sold, within an order

Lets dig into this, table by table and join by join.



Salesmen (and saleswomen)
------------------------------

Ok, we call them sales staff and we can reuse an earlier statement to review who they are and in what region they work and whos the boss.

```
+------------------+-----------+-----------------------------------------+
| Name             | Title     | Boss                                    |
+------------------+-----------+-----------------------------------------+
| Loui Bondur      | Sales Rep | Gerard Bondur, Sale Manager (EMEA)      |
| Larry Bott       | Sales Rep | Gerard Bondur, Sale Manager (EMEA)      |
| Pamela Castillo  | Sales Rep | Gerard Bondur, Sale Manager (EMEA)      |
| Martin Gerard    | Sales Rep | Gerard Bondur, Sale Manager (EMEA)      |
| Gerard Hernandez | Sales Rep | Gerard Bondur, Sale Manager (EMEA)      |
| Barry Jones      | Sales Rep | Gerard Bondur, Sale Manager (EMEA)      |
| Julie Firrelli   | Sales Rep | Anthony Bow, Sales Manager (NA)         |
| Leslie Jennings  | Sales Rep | Anthony Bow, Sales Manager (NA)         |
| Steve Patterson  | Sales Rep | Anthony Bow, Sales Manager (NA)         |
| Leslie Thompson  | Sales Rep | Anthony Bow, Sales Manager (NA)         |
| Foon Yue Tseng   | Sales Rep | Anthony Bow, Sales Manager (NA)         |
| George Vanauf    | Sales Rep | Anthony Bow, Sales Manager (NA)         |
| Yoshimi Kato     | Sales Rep | Mami Nishi, Sales Rep                   |
| Mami Nishi       | Sales Rep | Mary Patterson, VP Sales                |
| Andy Fixter      | Sales Rep | William Patterson, Sales Manager (APAC) |
| Tom King         | Sales Rep | William Patterson, Sales Manager (APAC) |
| Peter Marsh      | Sales Rep | William Patterson, Sales Manager (APAC) |
+------------------+-----------+-----------------------------------------+
```

There are 17 employees having a title of sales representative. Perhaps some of these will be the winner.



Customers
------------------------------

We review the customer table to see how many they are and how they link to a sales representative.

There seems to be 122 customers.

```
MariaDB [classicmodels]> SELECT COUNT(`customerNumber`) FROM customers;
+-------------------------+
| COUNT(`customerNumber`) |
+-------------------------+
|                     122 |
+-------------------------+
```

It seems that some customers does not have a sales representative. That is visible where the customer has a NULL value in the foreign key `salesRepEmployeeNumber`.

```
MariaDB [classicmodels]> SELECT
    ->     c.`customerName`,
    ->     c.`country`,
    ->     c.`salesRepEmployeeNumber`
    -> FROM customers As c
    -> LIMIT 10
    -> ;
+------------------------------+-----------+------------------------+
| customerName                 | country   | salesRepEmployeeNumber |
+------------------------------+-----------+------------------------+
| Atelier graphique            | France    |                   1370 |
| Signal Gift Stores           | USA       |                   1166 |
| Australian Collectors, Co.   | Australia |                   1611 |
| La Rochelle Gifts            | France    |                   1370 |
| Baane Mini Imports           | Norway    |                   1504 |
| Mini Gifts Distributors Ltd. | USA       |                   1165 |
| Havel & Zbyszek Co           | Poland    |                   NULL |
| Blauer See Auto, Co.         | Germany   |                   1504 |
| Mini Wheels Co.              | USA       |                   1165 |
| Land of Toys Inc.            | USA       |                   1323 |
+------------------------------+-----------+------------------------+
```

Ok, lets see if the customer that buys the most actually have a sales rep.



Order
------------------------------

The customer places an order that contains order details. An order contains several order details, or "order rows". Each order detail contains a product, quantity and price. We need to join the order with the order rows to sum the order value.

There seems to be 326 orders.

```
MariaDB [classicmodels]> SELECT COUNT(`orderNumber`) FROM orders;
+----------------------+
| COUNT(`orderNumber`) |
+----------------------+
|                  326 |
+----------------------+
```

<!-- Show the order with the order details, more details -->



Sum the order value
------------------------------

<!-- Enhance and explain -->

We can produce a statement that shows the orders with the highest order values. It can look like this.

```
+-------------+------------+----------+
| orderNumber | orderDate  | sum      |
+-------------+------------+----------+
|       10165 | 2003-10-22 | 67392.85 |
|       10287 | 2004-08-30 | 61402.00 |
|       10310 | 2004-10-16 | 61234.67 |
|       10212 | 2004-01-16 | 59830.55 |
|       10207 | 2003-12-09 | 59265.14 |
|       10127 | 2003-06-03 | 58841.35 |
|       10204 | 2003-12-02 | 58793.53 |
|       10126 | 2003-05-28 | 57131.92 |
|       10222 | 2004-02-19 | 56822.65 |
|       10142 | 2003-08-08 | 56052.56 |
+-------------+------------+----------+
```

We can also show the orders with the lowest order value.

```
+-------------+------------+---------+
| orderNumber | orderDate  | Sum     |
+-------------+------------+---------+
|       10408 | 2005-04-22 |  615.45 |
|       10144 | 2003-08-13 | 1128.20 |
|       10158 | 2003-10-10 | 1491.38 |
|       10116 | 2003-04-11 | 1627.56 |
|       10345 | 2004-11-25 | 1676.14 |
|       10242 | 2004-04-20 | 1679.92 |
|       10364 | 2005-01-06 | 1834.56 |
|       10286 | 2004-08-28 | 1960.80 |
|       10409 | 2005-04-23 | 2326.18 |
|       10317 | 2004-11-02 | 2434.25 |
+-------------+------------+---------+
```



Customer with the highest total order values
------------------------------

We can now join the statement with the customers and aggregate to find the customers buying the most or the least.

We can see that these customers are buying the least.

```
+----------------+------------------------------------+---------+------------+----------+
| customerNumber | customerName                       | country | Num orders | Sum      |
+----------------+------------------------------------+---------+------------+----------+
|            219 | Boards & Toys Co.                  | USA     |          3 |  7918.60 |
|            198 | Auto-Moto Classics Inc.            | USA     |          8 | 21554.26 |
|            103 | Atelier graphique                  | France  |          7 | 22314.36 |
|            473 | Frau da Collezione                 | Italy   |          8 | 25358.32 |
|            381 | Royale Belge                       | Belgium |          8 | 29217.18 |
|            456 | Microscale Inc.                    | USA     |         10 | 29230.43 |
|            489 | Double Decker Gift Stores, Ltd     | UK      |         12 | 29586.15 |
|            415 | Bavarian Collectables Imports, Co. | Germany |         14 | 31310.09 |
|            173 | Cambridge Collectables Co.         | USA     |         11 | 32198.69 |
|            347 | Men 'R' US Retailers, Ltd.         | USA     |         14 | 41506.19 |
+----------------+------------------------------------+---------+------------+----------+
```

The customers that orders the most are these.

```
+----------------+------------------------------+-------------+------------+-----------+
| customerNumber | customerName                 | country     | Num orders | Sum       |
+----------------+------------------------------+-------------+------------+-----------+
|            141 | Euro+ Shopping Channel       | Spain       |        259 | 820689.54 |
|            124 | Mini Gifts Distributors Ltd. | USA         |        180 | 591827.34 |
|            114 | Australian Collectors, Co.   | Australia   |         55 | 180585.07 |
|            151 | Muscle Machine Inc           | USA         |         48 | 177913.95 |
|            119 | La Rochelle Gifts            | France      |         53 | 158573.12 |
|            148 | Dragon Souveniers, Ltd.      | Singapore   |         43 | 156251.03 |
|            323 | Down Under Souveniers, Inc   | New Zealand |         46 | 154622.08 |
|            131 | Land of Toys Inc.            | USA         |         49 | 149085.15 |
|            187 | AV Stores, Co.               | UK          |         51 | 148410.09 |
|            450 | The Sharp Gifts Warehouse    | USA         |         40 | 143536.27 |
+----------------+------------------------------+-------------+------------+-----------+
```

Nice, it seems like we are looking for the sales reps to some of the top buying customers perhaps.



Top (and bottom) sales representative
------------------------------

Well, we are close now, we just need to join with the sales reps for each customer and then focus the aggregation on the sales reps.

Lets start to reuse the above statement and just add the name of the sales rep.

```
+------------------------------+------------------------------+-------------+------------+-----------+
| Sales rep                    | customerName                 | country     | Num orders | Sum       |
+------------------------------+------------------------------+-------------+------------+-----------+
| Gerard Hernandez (Sales Rep) | Euro+ Shopping Channel       | Spain       |        259 | 820689.54 |
| Leslie Jennings (Sales Rep)  | Mini Gifts Distributors Ltd. | USA         |        180 | 591827.34 |
| Andy Fixter (Sales Rep)      | Australian Collectors, Co.   | Australia   |         55 | 180585.07 |
| Foon Yue Tseng (Sales Rep)   | Muscle Machine Inc           | USA         |         48 | 177913.95 |
| Gerard Hernandez (Sales Rep) | La Rochelle Gifts            | France      |         53 | 158573.12 |
| Mami Nishi (Sales Rep)       | Dragon Souveniers, Ltd.      | Singapore   |         43 | 156251.03 |
| Peter Marsh (Sales Rep)      | Down Under Souveniers, Inc   | New Zealand |         46 | 154622.08 |
| George Vanauf (Sales Rep)    | Land of Toys Inc.            | USA         |         49 | 149085.15 |
| Larry Bott (Sales Rep)       | AV Stores, Co.               | UK          |         51 | 148410.09 |
| Leslie Jennings (Sales Rep)  | The Sharp Gifts Warehouse    | USA         |         40 | 143536.27 |
+------------------------------+------------------------------+-------------+------------+-----------+
```

Then we group the statement by the sales rep instead of the customer.

```
+------------------------------+------------+------------+
| Sales rep                    | Num orders | Sum        |
+------------------------------+------------+------------+
| Gerard Hernandez (Sales Rep) |        396 | 1258577.81 |
| Leslie Jennings (Sales Rep)  |        331 | 1081530.54 |
| Pamela Castillo (Sales Rep)  |        272 |  868220.55 |
| Larry Bott (Sales Rep)       |        236 |  732096.79 |
| Barry Jones (Sales Rep)      |        220 |  704853.91 |
| George Vanauf (Sales Rep)    |        211 |  669377.05 |
| Peter Marsh (Sales Rep)      |        185 |  584593.76 |
| Loui Bondur (Sales Rep)      |        177 |  569485.75 |
| Andy Fixter (Sales Rep)      |        185 |  562582.59 |
| Steve Patterson (Sales Rep)  |        152 |  505875.42 |
| Foon Yue Tseng (Sales Rep)   |        142 |  488212.67 |
| Mami Nishi (Sales Rep)       |        137 |  457110.07 |
| Martin Gerard (Sales Rep)    |        114 |  387477.47 |
| Julie Firrelli (Sales Rep)   |        124 |  386663.20 |
| Leslie Thompson (Sales Rep)  |        114 |  347533.03 |
+------------------------------+------------+------------+
```

There seems to be 15 sales reps that has made any sales. Perhaps we can do an outer join to see if there are any sales rep without any customers or orders.

Such a report can look like this.

```
+------------------------------+------------+------------+
| Sales rep                    | Num orders | Sum        |
+------------------------------+------------+------------+
| Gerard Hernandez (Sales Rep) |        396 | 1258577.81 |
| Leslie Jennings (Sales Rep)  |        331 | 1081530.54 |
| Pamela Castillo (Sales Rep)  |        272 |  868220.55 |
| Larry Bott (Sales Rep)       |        236 |  732096.79 |
| Barry Jones (Sales Rep)      |        220 |  704853.91 |
| George Vanauf (Sales Rep)    |        211 |  669377.05 |
| Peter Marsh (Sales Rep)      |        185 |  584593.76 |
| Loui Bondur (Sales Rep)      |        177 |  569485.75 |
| Andy Fixter (Sales Rep)      |        185 |  562582.59 |
| Steve Patterson (Sales Rep)  |        152 |  505875.42 |
| Foon Yue Tseng (Sales Rep)   |        142 |  488212.67 |
| Mami Nishi (Sales Rep)       |        137 |  457110.07 |
| Martin Gerard (Sales Rep)    |        114 |  387477.47 |
| Julie Firrelli (Sales Rep)   |        124 |  386663.20 |
| Leslie Thompson (Sales Rep)  |        114 |  347533.03 |
| Yoshimi Kato (Sales Rep)     |          0 |       NULL |
| Tom King (Sales Rep)         |          0 |       NULL |
+------------------------------+------------+------------+
```

Ah, two sales reps has connection with no orders.

So finally we have a winner of the top sales person.

However, this is for all time sales and if we look at the order date it seems to be spread over several years.

```
+------+------------+------------+
| Year | Num orders | Sum        |
+------+------------+------------+
| 2005 |        523 | 1770936.71 |
| 2004 |       1421 | 4515905.51 |
| 2003 |       1052 | 3317348.39 |
+------+------------+------------+
```

We could dig deeper to find out which sales staff has the highest sales each year. Gerhard and Leslie is in top of 2005.

```
+------------------------------+------+------------+-----------+  
| Sales rep                    | Year | Num orders | Sum       |  
+------------------------------+------+------------+-----------+  
| Gerard Hernandez (Sales Rep) | 2005 |        127 | 428755.70 |  
| Leslie Jennings (Sales Rep)  | 2005 |         96 | 335940.47 |  
| Peter Marsh (Sales Rep)      | 2005 |         47 | 167669.84 |  
+------------------------------+------+------------+-----------+  
```



Transpose rows to columns
------------------------------

There is a technique to transpose rows to columns. This is also called to pivot values of a linear list into a spreadsheet like array.

The result from pivoting into a "table like output" for the years could look like this.

```
+------------------------------+----------+----------+----------+ 
| sales_rep                    | 2003     | 2004     | 2005     | 
+------------------------------+----------+----------+----------+ 
| Leslie Jennings (Sales Rep)  |  413,220 |  332,370 |  335,940 | 
| Pamela Castillo (Sales Rep)  |  317,105 |  409,910 |  141,206 | 
| Gerard Hernandez (Sales Rep) |  295,246 |  534,576 |  428,756 | 
| Barry Jones (Sales Rep)      |  288,015 |  388,872 |   27,967 | 
| Mami Nishi (Sales Rep)       |  267,249 |  151,761 |   38,099 | 
| Larry Bott (Sales Rep)       |  261,537 |  317,142 |  153,418 | 
| Andy Fixter (Sales Rep)      |  226,808 |  204,213 |  131,561 | 
| Foon Yue Tseng (Sales Rep)   |  221,887 |  237,255 |   29,070 | 
| Julie Firrelli (Sales Rep)   |  220,117 |  129,916 |   36,630 | 
| Martin Gerard (Sales Rep)    |  179,649 |  207,829 |        0 | 
| Loui Bondur (Sales Rep)      |  177,960 |  312,915 |   78,610 | 
| George Vanauf (Sales Rep)    |  169,288 |  428,063 |   72,026 | 
| Leslie Thompson (Sales Rep)  |  119,461 |  185,038 |   43,033 | 
| Steve Patterson (Sales Rep)  |   81,664 |  337,261 |   86,950 | 
| Peter Marsh (Sales Rep)      |   78,141 |  338,783 |  167,670 | 
| Yoshimi Kato (Sales Rep)     |        0 |        0 |        0 | 
| Tom King (Sales Rep)         |        0 |        0 |        0 | 
+------------------------------+----------+----------+----------+ 
```

Above report is ordered by '2003' and this technique might be helpful when display a lot of values that is easier to read "table wise" instead or row wise.

The technique is to first produce the rows.

Then transpose the rows into columns using statements like these:


```
SELECT
    sales_rep,
    SUM(IF(the_year = '2003', the_sum, 0) AS '2003',
    SUM(IF(the_year = '2004', the_sum, 0) AS '2004',
    SUM(IF(the_year = '2005', the_sum, 0) AS '2005'
FROM 
(
    sub query with all rows
) AS t1
GROUP BY
    employeeNumber
;
```

We can make a smaller example to show the principal behind this. Lets start with the raw order values for each year, in a row like output.

```
+----------+--------------------+ 
| the_year | the_sum            | 
+----------+--------------------+ 
|     2003 |  3317348.389999999 | 
|     2004 |  4515905.510000002 | 
|     2005 | 1770936.7099999993 | 
+----------+--------------------+ 
```

The statement for this can look like this.

```sql
SELECT
    YEAR(o.`orderDate`) AS 'the_year',
    SUM(d.`quantityOrdered` * d.`priceEach`) AS 'the_sum'
FROM orders AS o
    INNER JOIN orderdetails AS d
        ON o.`orderNumber` = d.`orderNumber`
GROUP BY
    YEAR(o.`orderDate`) 
;
```

Then we use the SUM(IF()) statement to transpose the values into a table like output.

The statement can look like this.

```sql
SELECT
    FORMAT(SUM(IF(the_year = '2003', the_sum, 0)), 0) AS '2003',
    FORMAT(SUM(IF(the_year = '2004', the_sum, 0)), 0) AS '2004',
    FORMAT(SUM(IF(the_year = '2005', the_sum, 0)), 0) AS '2005'
FROM
(
    'the subquery with the row values'
) AS t1
;
```

In full the statement look like this.

```sql
SELECT
    FORMAT(SUM(IF(the_year = '2003', the_sum, 0)), 0) AS '2003',
    FORMAT(SUM(IF(the_year = '2004', the_sum, 0)), 0) AS '2004',
    FORMAT(SUM(IF(the_year = '2005', the_sum, 0)), 0) AS '2005'
FROM
(
    SELECT
        YEAR(o.`orderDate`) AS 'the_year',
        SUM(d.`quantityOrdered` * d.`priceEach`) AS 'the_sum'
    FROM orders AS o
        INNER JOIN orderdetails AS d
            ON o.`orderNumber` = d.`orderNumber`
    GROUP BY
        YEAR(o.`orderDate`) 
) AS t1
;
```

The transposed result look like this.

```
+-----------+-----------+-----------+    
| 2003      | 2004      | 2005      |    
+-----------+-----------+-----------+    
| 3,317,348 | 4,515,906 | 1,770,937 |    
+-----------+-----------+-----------+    
```

If you want to try the full transposed report of all sales reps divided per year, then the statement can look like this. It is three queries wrapped together.

```sql
SELECT
    *
FROM (
    SELECT
        sales_rep,
        LPAD(FORMAT(SUM(IF(the_year = '2003', the_sum, 0)), 0), 8) AS '2003',
        LPAD(FORMAT(SUM(IF(the_year = '2004', the_sum, 0)), 0), 8) AS '2004',
        LPAD(FORMAT(SUM(IF(the_year = '2005', the_sum, 0)), 0), 8) AS '2005'
    FROM (
        SELECT
            e.employeeNumber AS employeeNumber,
            CONCAT(e.`firstName`, ' ', e.`lastName`, ' (', e.`jobTitle`, ')' ) AS 'sales_rep',
            YEAR(o.`orderDate`) AS 'the_year',
            ROUND(SUM(d.`quantityOrdered` * d.`priceEach`), 2) AS 'the_sum'
        FROM orders AS o
            INNER JOIN orderdetails AS d
                ON o.`orderNumber` = d.`orderNumber`
            INNER JOIN customers AS c
                ON c.`customerNumber` = o.`customerNumber`
            RIGHT OUTER JOIN employees AS e
                ON e.`employeeNumber` = c.`salesRepEmployeeNumber`
        WHERE
            e.`jobTitle` = 'Sales Rep'
        GROUP BY
            YEAR(o.`orderDate`),
            e.`employeeNumber`
    ) AS t1
    GROUP BY
        employeeNumber
) AS t1
ORDER BY
    `2005` DESC
;
```

The result looks like this, ordered by the year 2005.

```
+------------------------------+----------+----------+----------+  
| sales_rep                    | 2003     | 2004     | 2005     |  
+------------------------------+----------+----------+----------+  
| Gerard Hernandez (Sales Rep) |  295,246 |  534,576 |  428,756 |  
| Leslie Jennings (Sales Rep)  |  413,220 |  332,370 |  335,940 |  
| Peter Marsh (Sales Rep)      |   78,141 |  338,783 |  167,670 |  
| Larry Bott (Sales Rep)       |  261,537 |  317,142 |  153,418 |  
| Pamela Castillo (Sales Rep)  |  317,105 |  409,910 |  141,206 |  
| Andy Fixter (Sales Rep)      |  226,808 |  204,213 |  131,561 |  
| Steve Patterson (Sales Rep)  |   81,664 |  337,261 |   86,950 |  
| Loui Bondur (Sales Rep)      |  177,960 |  312,915 |   78,610 |  
| George Vanauf (Sales Rep)    |  169,288 |  428,063 |   72,026 |  
| Leslie Thompson (Sales Rep)  |  119,461 |  185,038 |   43,033 |  
| Mami Nishi (Sales Rep)       |  267,249 |  151,761 |   38,099 |  
| Julie Firrelli (Sales Rep)   |  220,117 |  129,916 |   36,630 |  
| Foon Yue Tseng (Sales Rep)   |  221,887 |  237,255 |   29,070 |  
| Barry Jones (Sales Rep)      |  288,015 |  388,872 |   27,967 |  
| Martin Gerard (Sales Rep)    |  179,649 |  207,829 |        0 |  
| Yoshimi Kato (Sales Rep)     |        0 |        0 |        0 |  
| Tom King (Sales Rep)         |        0 |        0 |        0 |  
+------------------------------+----------+----------+----------+  
```



Summary
------------------------------

You have worked with an example of a database called classical models and you have investigated how to join several tables in the quest of finding an answer to a question about the data. We also learned about transposing rows to columns.

