--
-- Test natural join
--
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
    `employee_id` INT PRIMARY KEY,
    `name` VARCHAR(50),
    `office_id` INT
);

DROP TABLE IF EXISTS `office`;
CREATE TABLE `office` (
    `office_id` INT PRIMARY KEY,
    `office_name` VARCHAR(50),
    `location` VARCHAR(50)
);

-- Add data
INSERT INTO `employee` VALUES 
    (1, 'Alice', 101), 
    (2, 'Bob', 102),
    (3, 'Charlie', 103)
;

INSERT INTO `office` VALUES 
    (101, 'HQ', 'New York'),
    (102, 'Branch Office', 'Los Angeles'),
    (103, 'Remote Office', 'San Francisco')
;

-- Try NATURAL JOIN
SELECT 
    e.name AS 'Name', 
    o.office_name AS 'Office', 
    o.location AS 'Location'
FROM 
    employee AS e
    NATURAL JOIN office AS o
;
