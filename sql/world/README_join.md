World database and SQL with JOIN
======================

Exercises with the world database.

Perform the exercises and save all your SQL to a file.



Get going
----------------------

Import the database and show the tables.

```
+-----------------+
| Tables_in_world |
+-----------------+
| city            |
| country         |
| countrylanguage |
+-----------------+
```

Show how each table is created. Learn what columns exists in each table.



How to JOIN tables
----------------------

### JOIN two tables

Start by trying out the following SELECT statements.

```
-- Show all columns in the table
SELECT * FROM country LIMIT 3;

-- Show some columns in the table
SELECT code, name FROM country LIMIT 3;

-- Show some columns and give them a name for visibility
SELECT
    code AS 'Country code',
    name AS 'Name'
FROM country
LIMIT 3;
```

Prepare and execute the following SELECT statments.

* Create a SQL statement that shows the 7 (first) countries in the country table.

<!--
SELECT
    code AS Code,
    name AS Name,
    surfaceArea AS Area,
    indepYear AS IndepYear,
    lifeExpectancy AS LifeExpect
FROM country
LIMIT 7
;
-->
```
+------+----------------------+------------+-----------+------------+
| Code | Name                 | Area       | IndepYear | LifeExpect |
+------+----------------------+------------+-----------+------------+
| ABW  | Aruba                |     193.00 |      NULL |       78.4 |
| AFG  | Afghanistan          |  652090.00 |      1919 |       45.9 |
| AGO  | Angola               | 1246700.00 |      1975 |       38.3 |
| AIA  | Anguilla             |      96.00 |      NULL |       76.1 |
| ALB  | Albania              |   28748.00 |      1912 |       71.6 |
| AND  | Andorra              |     468.00 |      1278 |       83.5 |
| ANT  | Netherlands Antilles |     800.00 |      NULL |       74.7 |
+------+----------------------+------------+-----------+------------+
7 rows in set (0.000 sec)
```



1. Show the county name, country code and the name of the official language for all countries.
2. Shows the city name for all tuples in the city table, also show the full name of the country where the city is located.
3. How many cities are there in Sweden?
4. Show all the district names from the country where the name of the country is
Sweden. Make sure to only show each district name once.
5. Show the name of all countries and for each country display all languages spoken in that country.
6. Show all districts in Sweden together with average population for each district.
7. Selects all cities in Denmark and for each tuple show all information about the city and about Denmark.
8. Which country has the biggest population in Europe?
9. List the 10 biggest country (in terms of population) in Asia. (Use LIMIT to get 10 rows of the result)

10. Find out which countries that have no languages saved in the database. Use Left-join. You must see the name of your new country.
