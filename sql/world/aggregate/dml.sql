--
-- Exercise on aggregate and builtin functions
--

--
-- Import the database
--
-- SOURCE world.sql;



--
-- AVG
--
SELECT
    AVG(`LifeExpectancy`) AS 'Life expectancy'
FROM `country`
;

SELECT
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
;

SELECT
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Continent`
ORDER BY
    AVG(`LifeExpectancy`) DESC
;

SELECT
    `Name` AS 'Country',
    `Continent`,
    `LifeExpectancy` AS 'Life expectancy'
FROM `country`
WHERE
    `Continent` = 'Antarctica'
ORDER BY
    `LifeExpectancy` DESC
;

SELECT
    `Name` AS 'Country',
    `Continent`,
    `LifeExpectancy` AS 'Life expectancy'
FROM `country`
WHERE
    `Continent` = 'Micronesia/Caribbean'
ORDER BY
    `LifeExpectancy` DESC
;

SELECT
    `Name` AS 'Country',
    `Continent`,
    `LifeExpectancy` AS 'Life expectancy'
FROM `country`
WHERE
    `Region` = 'Micronesia/Caribbean'
ORDER BY
    `LifeExpectancy` DESC
;

SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Region`
ORDER BY
    AVG(`LifeExpectancy`) DESC -- ASC
LIMIT 5
;

SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
WHERE 
    `LifeExpectancy` IS NOT NULL
GROUP BY `Region`
ORDER BY
    AVG(`LifeExpectancy`)
LIMIT 5
;

SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Region`
HAVING 
    AVG(`LifeExpectancy`) IS NOT NULL
ORDER BY
    AVG(`LifeExpectancy`)
LIMIT 5
;

SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Region`
HAVING 
    AVG(`LifeExpectancy`) IS NOT NULL
    AND AVG(`LifeExpectancy`) >= 60
    AND AVG(`LifeExpectancy`) <= 65
ORDER BY
    AVG(`LifeExpectancy`) DESC
LIMIT 5
;



--
-- COUNT
--
SELECT
    COUNT(`Code`) AS 'Num of countries'
FROM `country`
WHERE
    `LifeExpectancy` IS NOT NULL
;

SELECT
    COUNT(`Code`) AS 'Num of countries'
FROM `country`
;

SELECT
    `Region`,
    `Continent`,
    COUNT(`Code`) AS 'Num of countries'
FROM `country`
WHERE
    `LifeExpectancy` IS NOT NULL
GROUP BY `Region`
ORDER BY
    COUNT(`Code`) DESC
LIMIT 7
;

SELECT
    `Region`,
    `Continent`,
    COUNT(`Code`) AS 'Num of countries'
FROM `country`
WHERE
    `LifeExpectancy` IS NOT NULL
    AND `LifeExpectancy` >= 75
GROUP BY `Region`
ORDER BY
    COUNT(`Code`) DESC
LIMIT 7
;



--
-- SUM
--
SELECT
    SUM(`Population`)
FROM `country`
;

SELECT
    FORMAT(SUM(`Population`), 0) AS 'Population'
FROM `country`
;

SELECT
    `Continent`,
    FORMAT(SUM(`Population`), 0) AS 'Population'
FROM `country`
GROUP BY `Continent`
ORDER BY
    SUM(`Population`) DESC
;

SELECT
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population'
FROM `country`
GROUP BY `Continent`
ORDER BY
    SUM(`Population`) DESC
;

SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population'
FROM `country`
GROUP BY `Region`
ORDER BY
    SUM(`Population`) DESC
LIMIT 7
;

SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population'
FROM `country`
GROUP BY `Region`
HAVING
    SUM(`Population`) > 0
ORDER BY
    SUM(`Population`) ASC
LIMIT 7
;



--
-- Combine all
--
SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population',
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy',
    COUNT(`Code`) AS 'Num of countries'
FROM `country`
GROUP BY `Region`
ORDER BY
    SUM(`Population`) DESC,
    COUNT(`Code`) DESC,
    AVG(`LifeExpectancy`)
LIMIT 7
;



--
-- CREATE VIEW
--
DROP VIEW IF EXISTS `report`;
CREATE VIEW `report`
AS
SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population',
    ROUND(AVG(`LifeExpectancy`), 1) AS 'LifeExpectancy',
    COUNT(`Code`) AS 'NumCountries'
FROM `country`
GROUP BY `Region`
;

SELECT * FROM `report`;

SELECT
    *
FROM `report`
ORDER BY
    `Population` DESC
LIMIT 3
;

SELECT
    *
FROM `report`
WHERE 
    `LifeExpectancy` >= 75
ORDER BY
    `LifeExpectancy` DESC
LIMIT 3
;

SHOW FULL TABLES;

SHOW FULL TABLES 
WHERE Table_Type LIKE 'VIEW';

SHOW CREATE VIEW report \G


--
-- CONCAT
--
SELECT 
    CONCAT(
        'The country ',
        `name`,
        ' (',
        `code`,
        ') is ruled by ',
        `HeadOfState`,
        '.'
    ) AS 'Details'
FROM `country`
WHERE
    `Region` LIKE 'Nordic%'
;
