---
revision: 
    "2024-01-23": "(A, mos) Moved from another document, rewritten."
---
World database and SQL: Aggregate and builtin functions
==============================

![work](.img/work.png)

This exercise is about practicing aggregate and builtin functions to work with the data in the database and prepare proper reports. At the end we create and work with a view.

Perform the exercises and save all your SQL to a file.

[[_TOC_]]

<!--
TODO

* 
-->



Prepare
------------------------------

You have installed MariaDB or MySQL and you have access to the database using a terminal client or a desktop client.

You have access to the world database and you have worked through the exercise "[World database and SQL: Get going](../get-going/README.md)".

The SQL code used in this exercise is available in the file [`dml.sql`](./dml.sql), use it as a way to view suggestions on solutions.



About aggregating results
------------------------------

The exercise "[World database and SQL: Get going](../get-going/README.md)" ended with a report that looks a bit like this.

First the SQL code.

```sql
--
-- Inspect lifeExpectancy
--
SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital',
    GROUP_CONCAT(la.`Language`) AS 'Language',
    co.`Region`,
    co.`Continent`,
    co.`LifeExpectancy`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    `IsOfficial` = 'T'
    AND `LifeExpectancy` IS NOT NULL
    -- AND co.`Region` LIKE 'Nordic%'
    -- AND co.`Continent` = 'Europe'
GROUP BY
    co.`code`
ORDER BY
    co.`LifeExpectancy` DESC
LIMIT 10
;
```

Then the report.

```
+------+-------------+------------------+-------------------------------+---------------------------+---------------+----------------+
| Code | Country     | Capital          | Language                      | Region                    | Continent     | LifeExpectancy |
+------+-------------+------------------+-------------------------------+---------------------------+---------------+----------------+
| AND  | Andorra     | Andorra la Vella | Catalan                       | Southern Europe           | Europe        |           83.5 |
| MAC  | Macao       | Macao            | Portuguese                    | Eastern Asia              | Asia          |           81.6 |
| SMR  | San Marino  | San Marino       | Italian                       | Southern Europe           | Europe        |           81.1 |
| JPN  | Japan       | Tokyo            | Japanese                      | Eastern Asia              | Asia          |           80.7 |
| SGP  | Singapore   | Singapore        | Chinese,Malay,Tamil           | Southeast Asia            | Asia          |           80.1 |
| AUS  | Australia   | Canberra         | English                       | Australia and New Zealand | Oceania       |           79.8 |
| CHE  | Switzerland | Bern             | French,German,Italian,Romansh | Western Europe            | Europe        |           79.6 |
| SWE  | Sweden      | Stockholm        | Swedish                       | Nordic Countries          | Europe        |           79.6 |
| HKG  | Hong Kong   | Victoria         | English                       | Eastern Asia              | Asia          |           79.5 |
| CAN  | Canada      | Ottawa           | English,French                | North America             | North America |           79.4 |
+------+-------------+------------------+-------------------------------+---------------------------+---------------+----------------+
```

This provides us with a report of the expected life expectancy of people living in specific countries. But what if we would like to see the same figures, but aggregated for a specific region or a specific continent?

To our aid comes [the aggregate SQL functions](https://mariadb.com/kb/en/aggregate-functions/) like SUM, AVG and COUNT.



AVG()
------------------------------

We can use the [aggregate function AVG()](https://mariadb.com/kb/en/avg/) to calculate the average life expectancy for the countries in a continent or a region.

Lets start with a real simple example.

```sql
SELECT
    AVG(`LifeExpectancy`) AS 'Life expectancy'
FROM `country`
;
```

The result looks like this.

```
+-----------------+
| Life expectancy |
+-----------------+
|        66.48604 |
+-----------------+
```

We can use the [builtin function](https://mariadb.com/kb/en/built-in-functions/) [ROUND()](https://mariadb.com/kb/en/round/) to only show one decimal. This can be used to enhance the readability of a report.

```sql
SELECT
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
;
```

Then we get the following output.

```
+-----------------+
| Life expectancy |
+-----------------+
|            66.5 |
+-----------------+
```

Ok, we now see what the average life expectancy is in all countries in the world. But what if we want to group the values by the region or continent?



GROUP BY
------------------------------

To view the average life expectency b the region we create a SQL statement like this.

```sql
SELECT
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Continent`
ORDER BY
    AVG(`LifeExpectancy`) DESC
;
```

The clause GROUP BY states what column we want to group the AVG by. Without it we will get the AVG value for all rows in the table.

The report can look like this.

```
+---------------+-----------------+
| Continent     | Life expectancy |
+---------------+-----------------+
| Europe        |            75.1 |
| North America |            73.0 |
| South America |            70.9 |
| Oceania       |            69.7 |
| Asia          |            67.4 |
| Africa        |            52.6 |
| Antarctica    |            NULL |
+---------------+-----------------+
```

It seems to be a rather big difference between living in Africa and Europe.

The NULL value for Antarctica seems intresting, try to create a statment that shows what countries are located in Antarctica to find out why it looks like that.

Ah, it seems like the countries in that continent doe all have NULL values for the life expectancy.

```
MariaDB [world]> SELECT
    ->     `Name` AS 'Country',
    ->     `Continent`,
    ->     `LifeExpectancy` AS 'Life expectancy'
    -> FROM `country`
    -> WHERE
    ->     `Continent` = 'Antarctica'
    -> ORDER BY
    ->     `LifeExpectancy` DESC
    -> ;
+----------------------------------------------+------------+-----------------+
| Country                                      | Continent  | Life expectancy |
+----------------------------------------------+------------+-----------------+
| Antarctica                                   | Antarctica |            NULL |
| French Southern territories                  | Antarctica |            NULL |
| Bouvet Island                                | Antarctica |            NULL |
| Heard Island and McDonald Islands            | Antarctica |            NULL |
| South Georgia and the South Sandwich Islands | Antarctica |            NULL |
+----------------------------------------------+------------+-----------------+
```

Ok, lets see the expected life expectancy by the region instead. Try to modify the statement above to get the following reports.

Show the top 5 regions

```
+---------------------------+-----------+-----------------+
| Region                    | Continent | Life expectancy |
+---------------------------+-----------+-----------------+
| Australia and New Zealand | Oceania   |            78.8 |
| Nordic Countries          | Europe    |            78.3 |
| Western Europe            | Europe    |            78.3 |
| British Islands           | Europe    |            77.3 |
| Southern Europe           | Europe    |            76.5 |
+---------------------------+-----------+-----------------+
```

Show the bottom 5 regions.

```
+----------------------+------------+-----------------+
| Region               | Continent  | Life expectancy |
+----------------------+------------+-----------------+
| Antarctica           | Antarctica |            NULL |
| Micronesia/Caribbean | Oceania    |            NULL |
| Southern Africa      | Africa     |            44.8 |
| Central Africa       | Africa     |            50.3 |
| Eastern Africa       | Africa     |            50.8 |
+----------------------+------------+-----------------+
```

Your statement could look something like this.

```sql
SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Region`
ORDER BY
    AVG(`LifeExpectancy`) DESC -- ASC
LIMIT 5
;
```

I say that the rows with the NULL values add little value, how can we remove them from the report?



WHERE versus HAVING
------------------------------

To remove the NULL values I first think of WHERE. A solution could look like this.

```sql
SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
WHERE 
    `LifeExpectancy` IS NOT NULL
GROUP BY `Region`
ORDER BY
    AVG(`LifeExpectancy`)
LIMIT 5
;
```

The results comes out like this.

```
+---------------------------+-----------+-----------------+
| Region                    | Continent | Life expectancy |
+---------------------------+-----------+-----------------+
| Southern Africa           | Africa    |            44.8 |
| Central Africa            | Africa    |            50.3 |
| Eastern Africa            | Africa    |            50.8 |
| Western Africa            | Africa    |            52.7 |
| Southern and Central Asia | Asia      |            61.4 |
+---------------------------+-----------+-----------------+
```

The WHERE part works to remove all countries that has the LifeExpectancy set to NULL. Therefore no countries that have NULL is included in the aggrerated values.

The other way to do the same, at least in this context, is to use HAVING which is like WHERE but it works on the aggregated value insted.

Here is SQL to do it.

```sql
SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Region`
HAVING 
    AVG(`LifeExpectancy`) IS NOT NULL
ORDER BY
    AVG(`LifeExpectancy`)
LIMIT 5
;
```

Run it to verify that you get the same report as above. 

The HAVING clause is working on the aggregated values on and in this example we get the same output. But lets have another example where we for some reason want to see the regions that have a life expectancy between 50 and 55 years. How could we do that with HAVING?

Well, consider that HAVING works on the aggregated value and work with it as you would with the WHERE statement.

Here is an example doing that.

```sql
SELECT
    `Region`,
    `Continent`,
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy'
FROM `country`
GROUP BY `Region`
HAVING 
    AVG(`LifeExpectancy`) IS NOT NULL
    AND AVG(`LifeExpectancy`) >= 60
    AND AVG(`LifeExpectancy`) <= 65
ORDER BY
    AVG(`LifeExpectancy`) DESC
LIMIT 5
;
```

The result can look like this.

```
+---------------------------+-----------+-----------------+
| Region                    | Continent | Life expectancy |
+---------------------------+-----------+-----------------+
| Southeast Asia            | Asia      |            64.4 |
| Southern and Central Asia | Asia      |            61.4 |
+---------------------------+-----------+-----------------+
```

To summarize, HAVING is like WHERE, but for the aggregated values.



COUNT()
------------------------------

It could be interesting to see how many countries that have a life expectancy above 75 years and how many have less.

Lets start with a simple one and count all the countries that has values for the life expectancy.

```sql
SELECT
    COUNT(`Code`) AS 'Countries with life expectancy'
FROM `country`
;
```

We get the total or countries like this.

```
+------------------+
| Num of countries |
+------------------+
|              222 |
+------------------+
```

Verify how many countries you have in total.

```
MariaDB [world]> SELECT
    ->     COUNT(`Code`) AS 'Num of countries'
    -> FROM `country`
    -> ;
+------------------+
| Num of countries |
+------------------+
|              239 |
+------------------+
```

Ok, now lets create a statement that shows the number of countries grouped by the region.

The result can look like this when we order and limit the output.

```
MariaDB [world]> SELECT
    ->     `Region`,
    ->     `Continent`,
    ->     COUNT(`Code`) AS 'Num of countries'
    -> FROM `country`
    -> WHERE
    ->     `LifeExpectancy` IS NOT NULL
    -> GROUP BY `Region`
    -> ORDER BY
    ->     COUNT(`Code`) DESC
    -> LIMIT 7
    -> ;
+---------------------------+---------------+------------------+
| Region                    | Continent     | Num of countries |
+---------------------------+---------------+------------------+
| Caribbean                 | North America |               24 |
| Eastern Africa            | Africa        |               19 |
| Middle East               | Asia          |               18 |
| Western Africa            | Africa        |               17 |
| Southern and Central Asia | Asia          |               14 |
| Southern Europe           | Europe        |               14 |
| South America             | South America |               13 |
+---------------------------+---------------+------------------+
```

Then we update the statement to include only those countries that have a life expectancy above 75.

The report can then look like this.

```
MariaDB [world]> SELECT
    ->     `Region`,
    ->     `Continent`,
    ->     COUNT(`Code`) AS 'Num of countries'
    -> FROM `country`
    -> WHERE
    ->     `LifeExpectancy` IS NOT NULL
    ->     AND `LifeExpectancy` >= 75
    -> GROUP BY `Region`
    -> ORDER BY
    ->     COUNT(`Code`) DESC
    -> LIMIT 7
    -> ;
+------------------+---------------+------------------+
| Region           | Continent     | Num of countries |
+------------------+---------------+------------------+
| Caribbean        | North America |               11 |
| Western Europe   | Europe        |                9 |
| Southern Europe  | Europe        |                8 |
| Nordic Countries | Europe        |                6 |
| South America    | South America |                4 |
| North America    | North America |                4 |
| Middle East      | Asia          |                4 |
+------------------+---------------+------------------+
```

The above report could say to us in what regions we can find the most countries where you are expected to live the longest.



SUM()
------------------------------

By using SUM we can find out how many people there are all over the world. Lets calculate it all to start with.

```
MariaDB [world]> SELECT
    ->     SUM(`Population`)
    -> FROM `country`
    -> ;
+-------------------+
| SUM(`Population`) |
+-------------------+
|        6078749450 |
+-------------------+
```

Lets use the builtin function [FORMAT](https://mariadb.com/kb/en/format/) to present that large number a bit better.

```sql
SELECT
    FORMAT(SUM(`Population`), 0) AS 'Population'
FROM `country`
;
```

Now we get an improved way to read that large number.

```
+---------------+
| Population    |
+---------------+
| 6,078,749,450 |
+---------------+
```

We can now use this to create a report with the population grouped by each continent.

```
MariaDB [world]> SELECT
    ->     `Continent`,
    ->     FORMAT(SUM(`Population`), 0) AS 'Population'
    -> FROM `country`
    -> GROUP BY `Continent`
    -> ORDER BY
    ->     SUM(`Population`) DESC
    -> ;
+---------------+---------------+
| Continent     | Population    |
+---------------+---------------+
| Asia          | 3,705,025,700 |
| Africa        | 784,475,000   |
| Europe        | 730,074,600   |
| North America | 482,993,000   |
| South America | 345,780,000   |
| Oceania       | 30,401,150    |
| Antarctica    | 0             |
+---------------+---------------+
```

It would be much nicer if we could align the numbers to the right and this can be done the the builtin function [LPAD](https://mariadb.com/kb/en/lpad/).

```sql
SELECT
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population'
FROM `country`
GROUP BY `Continent`
ORDER BY
    SUM(`Population`) DESC
;
```

We need to hardcode the amount of figures to pad. For sure, we could calculate even that but it becomes a bit harder.

This is the output of how many people live in each continent.

```
+---------------+--------------+
| Continent     | Population   |
+---------------+--------------+
| Asia          | 3,705,025,70 |
| Africa        |  784,475,000 |
| Europe        |  730,074,600 |
| North America |  482,993,000 |
| South America |  345,780,000 |
| Oceania       |   30,401,150 |
| Antarctica    |            0 |
+---------------+--------------+
```

We can continue to work with the statement to see in what regions the pople live. First we find the 7 top populated regions and then we find the 7 regions that has the smallest population (but not 0).

The first report can look like this.

```sql
+---------------------------+---------------+--------------+
| Region                    | Continent     | Population   |
+---------------------------+---------------+--------------+
| Eastern Asia              | Asia          | 1,507,328,00 |
| Southern and Central Asia | Asia          | 1,490,776,00 |
| Southeast Asia            | Asia          |  518,541,000 |
| South America             | South America |  345,780,000 |
| North America             | North America |  309,632,000 |
| Eastern Europe            | Europe        |  307,026,000 |
| Eastern Africa            | Africa        |  246,999,000 |
+---------------------------+---------------+--------------+
```

The second report can look like this.

```
+---------------------------+---------------+--------------+
| Region                    | Continent     | Population   |
+---------------------------+---------------+--------------+
| Micronesia                | Oceania       |      543,000 |
| Polynesia                 | Oceania       |      633,050 |
| Melanesia                 | Oceania       |    6,472,000 |
| Baltic Countries          | Europe        |    7,561,900 |
| Australia and New Zealand | Oceania       |   22,753,100 |
| Nordic Countries          | Europe        |   24,166,400 |
| Caribbean                 | North America |   38,140,000 |
+---------------------------+---------------+--------------+
```

How did you solve the second one, was it a HAVING or a WHERE or did it not matter in that context when you removed the regions that had population 0?

The statements could look something like this.

```sql
SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population'
FROM `country`
GROUP BY `Region`
HAVING
    SUM(`Population`) > 0
ORDER BY
    SUM(`Population`) ASC
LIMIT 7
;
```



Combine in one report
------------------------------

Would it be possible to combine all aggregated values in a single report? Sure, it is just to add each entry as you would with any column.

Consider the following statement.

```sql
SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population',
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy',
    COUNT(`Code`) AS 'Num of countries'
FROM `country`
GROUP BY `Region`
ORDER BY
    SUM(`Population`) DESC,
    COUNT(`Code`) DESC,
    AVG(`LifeExpectancy`)
LIMIT 7
;
```

The above statement will present a more complete report with all the aggregated values, by the region and ordered just how you want it.

```
+---------------------------+---------------+--------------+-----------------+------------------+
| Region                    | Continent     | Population   | Life expectancy | Num of countries |
+---------------------------+---------------+--------------+-----------------+------------------+
| Eastern Asia              | Asia          | 1,507,328,00 |            75.3 |                8 |
| Southern and Central Asia | Asia          | 1,490,776,00 |            61.4 |               14 |
| Southeast Asia            | Asia          |  518,541,000 |            64.4 |               11 |
| South America             | South America |  345,780,000 |            70.9 |               14 |
| North America             | North America |  309,632,000 |            75.8 |                5 |
| Eastern Europe            | Europe        |  307,026,000 |            69.9 |               10 |
| Eastern Africa            | Africa        |  246,999,000 |            50.8 |               20 |
+---------------------------+---------------+--------------+-----------------+------------------+
```

Well, the ordering is just to show that you can order by several parts, in the above report it was actualy only the part of order by population that made sense.



CREATE VIEW
------------------------------

Sometimes it can be handy to create a view for a report. It might be easier to work the a larger select statement by creating a view for it. You can consider a view as an alias for the report.

So, lets say we have the statement above that looks like this.

```sql
SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population',
    ROUND(AVG(`LifeExpectancy`), 1) AS 'Life expectancy',
    COUNT(`Code`) AS 'Num of countries'
FROM `country`
GROUP BY `Region`
ORDER BY
    SUM(`Population`) DESC,
    COUNT(`Code`) DESC,
    AVG(`LifeExpectancy`)
LIMIT 7
;
```

To create a view from it we add the following construct in the beginning of the statement.

```sql
CREATE VIEW `report`
AS
```

The comes the select statement. It can look like this and do note that I have removed the ORDER and LIMIT statement when I create the view.

```sql
DROP VIEW IF EXISTS `report`;
CREATE VIEW `report`
AS
SELECT
    `Region`,
    `Continent`,
    LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population',
    ROUND(AVG(`LifeExpectancy`), 1) AS 'LifeExpectancy',
    COUNT(`Code`) AS 'NumCountries'
FROM `country`
GROUP BY `Region`
;
```

I opted to also add the statement that drops the view if it exists, that is to make it easier to recreate the view if needed. 

It looks like this when you execute the statement.

```
MariaDB [world]> DROP VIEW IF EXISTS `report`;
Query OK, 0 rows affected (0,002 sec)

MariaDB [world]> CREATE VIEW `report`
    -> AS
    -> SELECT
    ->     `Region`,
    ->     `Continent`,
    ->     LPAD(FORMAT(SUM(`Population`), 0), 12) AS 'Population',
    ->     ROUND(AVG(`LifeExpectancy`), 1) AS 'LifeExpectancy',
    ->     COUNT(`Code`) AS 'NumCountries'
    -> FROM `country`
    -> GROUP BY `Region`
    -> ;
Query OK, 0 rows affected (0,002 sec)
```

The advantage of that view is that you now can use it as you would with a table.

```sql
SELECT * FROM `report`;
```

To show the simplicity by using a view, lets try to order the report by population and life expectancy in two separat reports.

First order by population.

```
MariaDB [world]> SELECT
    ->     *
    -> FROM `report`
    -> ORDER BY
    ->     `Population` DESC
    -> LIMIT 3
    -> ;
+---------------------------+-----------+--------------+----------------+--------------+
| Region                    | Continent | Population   | LifeExpectancy | NumCountries |
+---------------------------+-----------+--------------+----------------+--------------+
| Eastern Asia              | Asia      | 1,507,328,00 |           75.3 |            8 |
| Southern and Central Asia | Asia      | 1,490,776,00 |           61.4 |           14 |
| Southeast Asia            | Asia      |  518,541,000 |           64.4 |           11 |
+---------------------------+-----------+--------------+----------------+--------------+
```

The we show the life expectancy where it exceeds 75.

```
MariaDB [world]> SELECT
    ->     *
    -> FROM `report`
    -> WHERE 
    ->     `LifeExpectancy` >= 75
    -> ORDER BY
    ->     `LifeExpectancy` DESC
    -> LIMIT 3
    -> ;
+---------------------------+-----------+--------------+----------------+--------------+
| Region                    | Continent | Population   | LifeExpectancy | NumCountries |
+---------------------------+-----------+--------------+----------------+--------------+
| Australia and New Zealand | Oceania   |   22,753,100 |           78.8 |            5 |
| Western Europe            | Europe    |  183,247,600 |           78.3 |            9 |
| Nordic Countries          | Europe    |   24,166,400 |           78.3 |            7 |
+---------------------------+-----------+--------------+----------------+--------------+
```

A view is stored in the database as a table and you can list all tables and views like this.

```
MariaDB [world]> SHOW FULL TABLES;
+-----------------+------------+
| Tables_in_world | Table_type |
+-----------------+------------+
| city            | BASE TABLE |
| country         | BASE TABLE |
| countrylanguage | BASE TABLE |
| report          | VIEW       |
+-----------------+------------+
```

You can also limit the output to only the views.

```
MariaDB [world]> SHOW FULL TABLES 
    -> WHERE Table_Type LIKE 'VIEW';
+-----------------+------------+
| Tables_in_world | Table_type |
+-----------------+------------+
| report          | VIEW       |
+-----------------+------------+
```

You can always inspect how a view is created, the same way you do with the tables. Try it out with the following construct.

```sql
SHOW CREATE VIEW report \G
```

A view can sometimes be handy when you have a larger report that you want to work with.



About builtin functions
------------------------------

If you are a programmer than you know that each programming language has a set of builtin functions to make it easier to work with the language. This also applies to SQL. The [builtin functions](https://mariadb.com/kb/en/built-in-functions/) are spearated into several groups, like for example these.

* String functions
* Date & Time functions
* Aggregate functions
* Numeric functions
* Control flow functions
* (and more)

You can use these builtin functions to create nice looking reports by formatting the values directly in SQL. This helps so you do not need to further process the data in another language.

One helpful function is for example [CONCAT](https://mariadb.com/kb/en/concat/) that can help you construct a string from its parts.

```sql
SELECT 
    CONCAT(
        'The country ',
        `name`,
        ' (',
        `code`,
        ') is ruled by ',
        `HeadOfState`,
        '.'
    ) AS 'Details'
FROM `country`
WHERE
    `Region` LIKE 'Nordic%'
;
```

The report would then look like this.

```
+-----------------------------------------------------------------+
| Details                                                         |
+-----------------------------------------------------------------+
| The country Denmark (DNK) is ruled by Margrethe II.             |
| The country Finland (FIN) is ruled by Tarja Halonen.            |
| The country Faroe Islands (FRO) is ruled by Margrethe II.       |
| The country Iceland (ISL) is ruled by Ólafur Ragnar Grímsson.   |
| The country Norway (NOR) is ruled by Harald V.                  |
| The country Svalbard and Jan Mayen (SJM) is ruled by Harald V.  |
| The country Sweden (SWE) is ruled by Carl XVI Gustaf.           |
+-----------------------------------------------------------------+
```

It is a good suggestion to always consider that there "might be a builtin function" fo rthe task you want to do.



Summary
------------------------------

You have learned how to work with aggregate and builtin functions to create reports from the data in the database.
