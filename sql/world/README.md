---
revision: 
    "2024-01-17": "(A, mos) Moved from another document, rewritten and scaled down."
---
World database and SQL: Get going
======================

![work](.img/work.png)

This exercise is about get going with the world database and how to use SQL to inspect it.

Perform the exercises and save all your SQL to a file.

[[_TOC_]]



Prepare
------------------------------

You have installed MariaDB or MySQL and you have acces to the database using a terminal client or a desktop client.



The world database
------------------------------

You can read about the [world database in the MySQL documentation](https://dev.mysql.com/doc/world-setup/en/). It is a database example that is commonly used to practice the basics of managing a database.

You should have access to the backup file of the world database which can be downloaded from the above site.

This repo includes a copy of the backup file at [`databas/world.sql`](../../database/world.sql).



Import the database
----------------------

Create a working directory and ensure that you save all the SQL you write into the file `dml.sql`.

Start by importing the backup of the database. You can place the backupfile in your working wirectory and label it `world.sql`.

Open up the backup file in your texteditor and inspect it. Do you see it  contains SQL statement and that it starts to create a database called `world`?

Now proceed to import the database. Either from the terminal.

```
mariadb < world.sql
```

Or from within the terminal client.

```sql
MariaDB [world]> source world.sql
```

You can verify that it works by checking what tables you have in the database.

```sql
SHOW TABLES;
```



Import the database and show the tables.

```
+-----------------+
| Tables_in_world |
+-----------------+
| city            |
| country         |
| countrylanguage |
+-----------------+
```

Show how each table is created. Learn what columns exists in each table.



Investigate the country table using SELECT
----------------------

### SELECT

Start by trying out the following SELECT statements.

```
-- Show all columns in the table
SELECT * FROM country LIMIT 3;

-- Show some columns in the table
SELECT code, name FROM country LIMIT 3;

-- Show some columns and give them a name for visibility
SELECT
    code AS 'Country code',
    name AS 'Name'
FROM country
LIMIT 3;
```

Prepare and execute the following SELECT statments.

* Create a SQL statement that shows the 7 (first) countries in the country table.

<!--
SELECT
    code AS Code,
    name AS Name,
    surfaceArea AS Area,
    indepYear AS IndepYear,
    lifeExpectancy AS LifeExpect
FROM country
LIMIT 7
;
-->
```
+------+----------------------+------------+-----------+------------+
| Code | Name                 | Area       | IndepYear | LifeExpect |
+------+----------------------+------------+-----------+------------+
| ABW  | Aruba                |     193.00 |      NULL |       78.4 |
| AFG  | Afghanistan          |  652090.00 |      1919 |       45.9 |
| AGO  | Angola               | 1246700.00 |      1975 |       38.3 |
| AIA  | Anguilla             |      96.00 |      NULL |       76.1 |
| ALB  | Albania              |   28748.00 |      1912 |       71.6 |
| AND  | Andorra              |     468.00 |      1278 |       83.5 |
| ANT  | Netherlands Antilles |     800.00 |      NULL |       74.7 |
+------+----------------------+------------+-----------+------------+
7 rows in set (0.000 sec)
```



### SELECT WHERE

Start by trying out the following SELECT statements.

```
-- All countries with a name matching searchstring
SELECT code, name, population
FROM country
WHERE name LIKE '%land'
;

-- All countries with a population less than
SELECT code, name, population
FROM country
WHERE population < 10000
;
```

Prepare and execute the following SELECT statments.

* Create a SQL statement that shows the countries that has a population between 10 000 and 50 000 and where the life expectancy is less than 70 years.

<!--
SELECT
    code AS Code,
    name AS Name,
    region AS Region,
    continent AS Continent,
    population AS Population,
    lifeExpectancy AS LifeExpect
FROM country
WHERE
    population BETWEEN 10000 AND 50000
    AND lifeExpectancy < 70
;
-->
```
+------+--------+------------+-----------+------------+------------+  
| Code | Name   | Region     | Continent | Population | LifeExpect |  
+------+--------+------------+-----------+------------+------------+  
| NRU  | Nauru  | Micronesia | Oceania   |      12000 |       60.8 |  
| PLW  | Palau  | Micronesia | Oceania   |      19000 |       68.6 |  
| TUV  | Tuvalu | Polynesia  | Oceania   |      12000 |       66.3 |  
+------+--------+------------+-----------+------------+------------+  
3 rows in set (0.001 sec)
```

* Show the countries that has a country code as 'SWE', 'FIN' OR having the name 'Denmark' or 'Norway' OR having the color "green" in its name.

<!--
SELECT
    code AS Code,
    name AS Name,
    region AS Region,
    continent AS Continent,
    population AS Population,
    lifeExpectancy AS LifeExpect
FROM country
WHERE
    code IN('SWE', 'FIN')
    OR name LIKE 'Denmark'
    OR name LIKE 'Norway'
    OR name LIKE '%green%'
;
-->
```
+----+-----------+------------------+---------------+------------+------------+
| Code | Name      | Region           | Continent     | Population | LifeExpect |
+----+-----------+------------------+---------------+------------+------------+
| DNK  | Denmark   | Nordic Countries | Europe        |    5330000 |       76.5 |
| FIN  | Finland   | Nordic Countries | Europe        |    5171300 |       77.4 |
| GRL  | Greenland | North America    | North America |      56000 |       68.1 |
| NOR  | Norway    | Nordic Countries | Europe        |    4478500 |       78.7 |
| SWE  | Sweden    | Nordic Countries | Europe        |    8861400 |       79.6 |
+
----+-----------+------------------+---------------+------------+------------+
5 rows in set (0.001 sec)
```



### SELECT ORDER BY

Start by trying out the following SELECT statements.

```
-- All nordic countries, ordered by life expectancy with highest valie first
SELECT code, name, region, population, lifeExpectancy
FROM country
WHERE region LIKE 'Nordic%'
ORDER BY lifeExpectancy DESC
;

-- The 5 smallest countries in Europe, by population.
SELECT code, name, region, continent, population, lifeExpectancy
FROM country
WHERE continent = 'Europe'
ORDER BY population ASC
LIMIT 5
;
```

Prepare and execute the following SELECT statments.

* Create a SQL statement that shows the following report (exactly).

<!--
SELECT
    code AS Code,
    name AS Name,
    region AS Region,
    continent AS Continent,
    population AS Population,
    lifeExpectancy AS LifeExpect
FROM country
WHERE
    continent = 'Oceania'
    AND region NOT LIKE 'A%'
    AND population > 0
    AND lifeExpectancy IS NOT NULL
ORDER BY
    region ASC,
    lifeExpectancy DESC
;
-->
```
+------+---------------------------------+------------+-----------+------------+------------+
| Code | Name                            | Region     | Continent | Population | LifeExpect |
+------+---------------------------------+------------+-----------+------------+------------+
| NCL  | New Caledonia                   | Melanesia  | Oceania   |     214000 |       72.8 |
| SLB  | Solomon Islands                 | Melanesia  | Oceania   |     444000 |       71.3 |
| FJI  | Fiji Islands                    | Melanesia  | Oceania   |     817000 |       67.9 |
| PNG  | Papua New Guinea                | Melanesia  | Oceania   |    4807000 |       63.1 |
| VUT  | Vanuatu                         | Melanesia  | Oceania   |     190000 |       60.6 |
| GUM  | Guam                            | Micronesia | Oceania   |     168000 |       77.8 |
| MNP  | Northern Mariana Islands        | Micronesia | Oceania   |      78000 |       75.5 |
| FSM  | Micronesia, Federated States of | Micronesia | Oceania   |     119000 |       68.6 |
| PLW  | Palau                           | Micronesia | Oceania   |      19000 |       68.6 |
| MHL  | Marshall Islands                | Micronesia | Oceania   |      64000 |       65.5 |
| NRU  | Nauru                           | Micronesia | Oceania   |      12000 |       60.8 |
| KIR  | Kiribati                        | Micronesia | Oceania   |      83000 |       59.8 |
| ASM  | American Samoa                  | Polynesia  | Oceania   |      68000 |       75.1 |
| PYF  | French Polynesia                | Polynesia  | Oceania   |     235000 |       74.8 |
| COK  | Cook Islands                    | Polynesia  | Oceania   |      20000 |       71.1 |
| WSM  | Samoa                           | Polynesia  | Oceania   |     180000 |       69.2 |
| TON  | Tonga                           | Polynesia  | Oceania   |      99000 |       67.9 |
| TUV  | Tuvalu                          | Polynesia  | Oceania   |      12000 |       66.3 |
+------+---------------------------------+------------+-----------+------------+------------+
18 rows in set (0.001 sec)
```

<!-- SELECT from more tables and round up with a simple join -->


Aggregating functions
----------------------

<!-- Move this to own article -->

### COUNT, AVG, SUM

Start by trying out the following statements.

```
-- How many countries are there?
SELECT COUNT(code) AS 'Number of countries' FROM country;

-- How many cities are there?
SELECT COUNT(ID) AS 'Number of cities' FROM city;

-- How many languages are there?
SELECT COUNT(DISTINCT language) AS 'Number of languages' FROM countrylanguage;
```

Prepare and execute the following SELECT statments.

* Create a statement showing the average life expectancy.

<!--
SELECT
    AVG(lifeExpectancy) AS 'Average life expectancy'
FROM country
;
-->
```
+-------------------------+
| Average life expectancy |
+-------------------------+
|                66.48604 |
+-------------------------+
1 row in set (0.000 sec)
```

* Create a statement showing the total population for all countries.

<!--
SELECT
    SUM(population) AS 'Total population'
FROM country
;
-->
```
+------------------+
| Total population |
+------------------+
|       6078749450 |
+------------------+
1 row in set (0.000 sec)
```



### GROUP BY

Start by trying out the following statements.

```
-- Show the life expectancy by continent from top to lowest.
SELECT
     continent,
     AVG(lifeExpectancy) AS 'Average life expectancy'
FROM country
GROUP BY continent
ORDER BY AVG(lifeExpectancy) DESC;
```

Prepare and execute the following SELECT statments.

* Show the continents and count the number of countries in them, order by the continent with most countries.

<!--
SELECT
    continent AS Continent,
    COUNT(code) AS 'Number of countries'
FROM country
GROUP BY continent
ORDER BY COUNT(code) DESC
;
-->
```
+---------------+---------------------+
| Continent     | Number of countries |
+---------------+---------------------+
| Africa        |                  58 |
| Asia          |                  51 |
| Europe        |                  46 |
| North America |                  37 |
| Oceania       |                  28 |
| South America |                  14 |
| Antarctica    |                   5 |
+---------------+---------------------+
7 rows in set (0.001 sec)
```

* Create SQL to show the report below (exactly).

<!--
SELECT
    region AS Region,
    COUNT(code) AS 'Number of countries',
    AVG(lifeExpectancy) AS 'Average life expectancy'
FROM country
GROUP BY region
ORDER BY AVG(lifeExpectancy) DESC
;
-->
```
+---------------------------+---------------------+-------------------------+  
| Region                    | Number of countries | Average life expectancy |  
+---------------------------+---------------------+-------------------------+  
| Australia and New Zealand |                   5 |                78.80000 |  
| Nordic Countries          |                   7 |                78.33333 |  
| Western Europe            |                   9 |                78.25556 |  
| British Islands           |                   2 |                77.25000 |  
| Southern Europe           |                  15 |                76.52857 |  
| North America             |                   5 |                75.82000 |  
| Eastern Asia              |                   8 |                75.25000 |  
| Caribbean                 |                  24 |                73.05833 |  
| Central America           |                   8 |                71.02500 |  
| South America             |                  14 |                70.94615 |  
| Polynesia                 |                  10 |                70.73333 |  
| Middle East               |                  18 |                70.56667 |  
| Eastern Europe            |                  10 |                69.93000 |  
| Baltic Countries          |                   3 |                69.00000 |  
| Micronesia                |                   7 |                68.08571 |  
| Melanesia                 |                   5 |                67.14000 |  
| Northern Africa           |                   7 |                65.38571 |  
| Southeast Asia            |                  11 |                64.40000 |  
| Southern and Central Asia |                  14 |                61.35000 |  
| Western Africa            |                  17 |                52.74118 |  
| Eastern Africa            |                  20 |                50.81053 |  
| Central Africa            |                   9 |                50.31111 |  
| Southern Africa           |                   5 |                44.82000 |  
| Antarctica                |                   5 |                    NULL |  
| Micronesia/Caribbean      |                   1 |                    NULL |  
+---------------------------+---------------------+-------------------------+  
25 rows in set (0.001 sec)
```



### HAVING

Start by trying out the following statements.

```
-- Show the continents and how many countries they have, but only show the
-- continents having 10 or more countries.
SELECT
     continent,
     COUNT(code) AS 'Number of countries'
FROM country
GROUP BY continent
HAVING COUNT(code) >= 10
ORDER BY COUNT(code) DESC;
```

Prepare and execute the following SELECT statments.

* Create SQL to show the report below (exactly). Hint: No countries that has a null value in the lifeExpectancy is included and only regions with at least 10 countries and the average life expectancy for the region is not null.

<!--
SELECT
    region AS Region,
    COUNT(code) AS 'Number of countries',
    AVG(lifeExpectancy) AS 'Average life expectancy'
FROM country
WHERE lifeExpectancy IS NOT NULL
GROUP BY region
HAVING
    AVG(lifeExpectancy) IS NOT NULL
    AND COUNT(code) >= 10
ORDER BY AVG(lifeExpectancy) DESC
;
-->

```
+---------------------------+---------------------+-------------------------+
| Region                    | Number of countries | Average life expectancy |
+---------------------------+---------------------+-------------------------+
| Southern Europe           |                  14 |                76.52857 |
| Caribbean                 |                  24 |                73.05833 |
| South America             |                  13 |                70.94615 |
| Middle East               |                  18 |                70.56667 |
| Eastern Europe            |                  10 |                69.93000 |
| Southeast Asia            |                  11 |                64.40000 |
| Southern and Central Asia |                  14 |                61.35000 |
| Western Africa            |                  17 |                52.74118 |
| Eastern Africa            |                  19 |                50.81053 |
+---------------------------+---------------------+-------------------------+
9 rows in set (0.001 sec)
```



Builtin functions
----------------------

### Functions to use for formatting the report

Start by trying out the following statements.

```
-- Round the average life expectancy to one decimal
SELECT
    continent AS Continent,
    ROUND(AVG(lifeExpectancy), 1) AS 'Average life expectancy'
FROM country
GROUP BY continent
ORDER BY AVG(lifeExpectancy) DESC
;

-- Concatenate strings
SELECT
    CONCAT(name, ' (', code, ')') AS Country
FROM country
LIMIT 7
;
```

Prepare and execute the following SELECT statments.

* Show the parts of the current date and time as the following report does.

<!--
SELECT
    NOW() AS Now,
    YEAR(NOW()) AS Year,
    MONTH(NOW()) AS Month,
    DAY(NOW()) AS Day,
    DAYNAME(NOW()) AS 'Day name',
    HOUR(NOW()) AS Hour,
    MINUTE(NOW()) AS Minute,
    SECOND(NOW()) AS Second
;
-->

```
+---------------------+------+-------+------+----------+------+--------+--------+
| Now                 | Year | Month | Day  | Day name | Hour | Minute | Second |
+---------------------+------+-------+------+----------+------+--------+--------+
| 2022-02-03 16:56:53 | 2022 |     2 |    3 | Thursday |   16 |     56 |     53 |
+---------------------+------+-------+------+----------+------+--------+--------+
```



* Find a mathematical expression that uses the number 2 to produce the following results.

<!--
SELECT
    2 AS 'The number',
    ROUND(SQRT(2), 4) AS 'The calculated result'
;
-->

```
+------------+-----------------------+
| The number | The calculated result |
+------------+-----------------------+
|          2 |                1.4142 |
+------------+-----------------------+
```

* Show the three first characters of the continents.

<!--
SELECT DISTINCT
    SUBSTR(continent, 1, 3) AS 'Continent with three chars'
FROM country
ORDER BY continent;
-->

```
+----------------------------+
| Continent with three chars |
+----------------------------+
| Asi                        |
| Eur                        |
| Nor                        |
| Afr                        |
| Oce                        |
| Ant                        |
| Sou                        |
+----------------------------+
7 rows in set (0.001 sec)
```
