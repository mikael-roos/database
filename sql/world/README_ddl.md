World database and SQL
======================

Exercises with the world database.

Perfomr the exercises and save all your SQL to a file.






# Create a database

> "Create a database for all the countries in the world and their capitals."

```
--
-- Create a sample world database called "earth" for the
-- slides of SQL
--
DROP DATABASE IF EXISTS earth;
CREATE DATABASE earth;

USE earth;
```


# Create a country table

```
-- Create the table
DROP TABLE IF EXISTS country;
CREATE TABLE country (
    `code` CHAR(3) NOT NULL DEFAULT '',
    `name` CHAR(52) NOT NULL DEFAULT '',
    `continent` ENUM('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL DEFAULT 'Asia',
    `surface_area` FLOAT(10,2) NOT NULL DEFAULT '0.00',
    `indep_year` SMALLINT DEFAULT NULL,
    `population` INT NOT NULL DEFAULT '0',
    `life_expectancy` FLOAT(3,1) DEFAULT NULL,
    `capital` INT DEFAULT NULL,
    PRIMARY KEY (`code`)
);

SHOW CREATE TABLE country \G;
```


# Common SQL datatypes

* CHAR, VARCHAR
* TEXT, TINYTEXT, MEDIUMTEXT, LONGTEXTTEXT
* BLOB, TINYBLOB, MEDIUMBLOB, LONGBLOB
* INTEGER, SMALLINT, TINYINT, MEDIUMINT, BIGINT
* BOOLEAN
* FLOAT, DOUBLE (approximate)
* NUMERIC, DECIMAL (exact)
* DATE, DATETIME, TIME, TIMESTAMP


# Primary keys


# Foreign keys


# ALTER

Create table for capital/city?

* Modify an existing database schema

```
mysql> ALTER TABLE country DROP COLUMN capital;
Query OK, 0 rows affected (0.24 sec)
Records: 0  Duplicates: 0  Warnings: 0
```



# INSERT INTO

```sql
DELETE FROM country;

INSERT INTO country
    (`code`, `name`, `surface_area`, `indep_year`)
VALUES
    ('SWE', 'Sweden', 449964.00, 836),
    ('DNK', 'Denmark', 43094.00, 800),
    ('NOR', 'Norway', 323877.00, 1905),
    ('ISL', 'Iceland', 103000.00, 1944),
    ('FIN', 'Finland', 338145.00, 1917),
    ('GRL', 'Greenland', 2166090.00, NULL)
;
```

# SELECT

```sql
mysql> SELECT code, name, surface_area, indep_year FROM country;
+------+-----------+--------------+------------+
| code | name      | surface_area | indep_year |
+------+-----------+--------------+------------+
| DNK  | Denmark   |     43094.00 |        800 |
| FIN  | Finland   |    338145.00 |       1917 |
| GRL  | Greenland |   2166090.00 |       NULL |
| ISL  | Iceland   |    103000.00 |       1944 |
| NOR  | Norway    |    323877.00 |       1905 |
| SWE  | Sweden    |    449964.00 |        836 |
+------+-----------+--------------+------------+
6 rows in set (0.00 sec)
```

# Add and update content in the database

# UPDATE

To which continent do the country belong?

```sql
mysql> SELECT code, name, continent FROM country;
+------+-----------+-----------+
| code | name      | continent |
+------+-----------+-----------+
| DNK  | Denmark   | Asia      |
| FIN  | Finland   | Asia      |
| GRL  | Greenland | Asia      |
| ISL  | Iceland   | Asia      |
| NOR  | Norway    | Asia      |
| SWE  | Sweden    | Asia      |
+------+-----------+-----------+
6 rows in set (0.00 sec)
```



# UPDATE...

```sql
UPDATE country SET continent = 'Europe';

UPDATE country
SET
    continent = 'North America'
WHERE
    name = 'Greenland'
;
```



# UPDATE...

```sql
mysql> SELECT code, name, continent FROM country;
+------+-----------+---------------+
| code | name      | continent     |
+------+-----------+---------------+
| DNK  | Denmark   | Europe        |
| FIN  | Finland   | Europe        |
| GRL  | Greenland | North America |
| ISL  | Iceland   | Europe        |
| NOR  | Norway    | Europe        |
| SWE  | Sweden    | Europe        |
+------+-----------+---------------+
```


# UPDATE

A recent report comes with updated values of the population and life expectancy for the countries.

```sql
mysql> SELECT code, name, population, life_expectancy FROM country;
+------+-----------+------------+-----------------+
| code | name      | population | life_expectancy |
+------+-----------+------------+-----------------+
| DNK  | Denmark   |          0 |            NULL |
| FIN  | Finland   |          0 |            NULL |
| GRL  | Greenland |          0 |            NULL |
| ISL  | Iceland   |          0 |            NULL |
| NOR  | Norway    |          0 |            NULL |
| SWE  | Sweden    |          0 |            NULL |
+------+-----------+------------+-----------------+
```

# UPDATE...

Update values in each row.

```sql
UPDATE country SET
    population = 5330000,
    life_expectancy = 79.6
WHERE
    code = 'SWE'
;

UPDATE country SET
    population = 8861400,
    life_expectancy = 76.5
WHERE
    code = 'DNK'
;
```


# UPDATE...

```sql
mysql> SELECT code, name, population, life_expectancy FROM country;
+------+-----------+------------+-----------------+
| code | name      | population | life_expectancy |
+------+-----------+------------+-----------------+
| DNK  | Denmark   |    5330000 |            76.5 |
| FIN  | Finland   |    5171300 |            77.4 |
| GRL  | Greenland |      56000 |            68.1 |
| ISL  | Iceland   |     279000 |            79.4 |
| NOR  | Norway    |    4478500 |            78.7 |
| SWE  | Sweden    |    8861400 |            79.6 |
+------+-----------+------------+-----------------+
```



# Remove rows from table

# DELETE

* Use with WHERE to only delete specific rows

```
mysql> DELETE FROM country
    -> WHERE code IN ('SWE', 'DNK');
Query OK, 2 rows affected (0.02 sec)

mysql> DELETE FROM country
    -> WHERE name LIKE '%land'
    -> LIMIT 1;
Query OK, 1 row affected (0.02 sec)

mysql> DELETE FROM country;
Query OK, 3 rows affected (0.02 sec)
```
