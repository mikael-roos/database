--
-- Import the database
--
-- source world.sql



--
-- Check what the table it contains
--
SHOW TABLES;



--
-- Inspect the schema and the tables
--
SHOW CREATE TABLE `city` \G
SELECT * FROM `city` LIMIT 5;

SELECT
    `Code`,
    `Name`
FROM `country`
WHERE
    `Code`= 'SWE'
;



--
-- Find details about Sweden - country
--
SELECT `Code`, `Name` FROM `country` WHERE `Name` = 'Sweden';
SELECT `Code`, `Name` FROM `country` WHERE `Code` = 'SWE';

SELECT
    `Code`,
    `Name`,
    `LocalName`,
    `Continent`,
    `Region`,
    `HeadOfState`,
    `IndepYear`,
    `Population`,
    `LifeExpectancy`
FROM `country`
WHERE
    `Code`= 'SWE'
;



--
-- Find details about Sweden - countrylanguage
--
SELECT * FROM `countrylanguage` WHERE `CountryCode`= 'SWE';
SELECT * FROM `countrylanguage` WHERE `CountryCode`= 'SWE' ORDER BY `Percentage` DESC;



--
-- Find details about Sweden - city
--
SELECT * FROM `city` WHERE `CountryCode`= 'SWE';



--
-- Capital
--
SELECT `Code`, `Name`, `Capital` FROM `country` WHERE `Code`= 'SWE';
SELECT `ID`, `Name` FROM `city` WHERE `CountryCode`= 'SWE' AND `ID` = 3048;



--
-- Join country and capital (ugly) and nice
--
SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital'
FROM 
    `country` AS co,
    `city` AS ca
WHERE
    co.`Code` = 'SWE'
    AND ca.`ID` = co.`Capital`
;

SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital'
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
WHERE
    co.`Code` = 'SWE'
;



--
-- Join country, capital and countrylanguage
--
SELECT * FROM `countrylanguage` WHERE `CountryCode`= 'SWE';

SELECT 
    co.`Code`,
    co.`Name` AS 'Language',
    ca.`Name` AS 'Capital',
    la.`Language`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    co.`Code` = 'SWE'
    AND `IsOfficial` = 'T'
;


--
-- Show details from the nordic countries
--
select Code, Name, Region, Continent from country limit 5

SELECT DISTINCT `Region` FROM `country` ORDER BY `Region`;
SELECT DISTINCT `Continent` FROM `country` ORDER BY `Continent`;


SELECT 
    co.`Code`,
    co.`Name`,
    ca.`Name`,
    la.`Language`,
    co.`Region`,
    co.`Continent`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    co.`Continent` = 'Europe'
    AND `IsOfficial` = 'T'
;

SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital',
    la.`Language` AS 'Language',
    co.`Region`,
    co.`Continent`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    co.`Region` LIKE 'Nordic%'
    AND `IsOfficial` = 'T'
;

SELECT
    *
FROM `countrylanguage`
WHERE
    `CountryCode`IN ('FIN', 'FRO')
    AND `IsOfficial` = 'T'
;


SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital',
    GROUP_CONCAT(la.`Language`) AS 'Language',
    co.`Region`,
    co.`Continent`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    co.`Region` LIKE 'Nordic%'
    AND `IsOfficial` = 'T'
GROUP BY
    co.`code`
;



--
-- View countries by area
--
SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital',
    GROUP_CONCAT(la.`Language`) AS 'Language',
    co.`Region`,
    co.`Continent`,
    co.`SurfaceArea`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    co.`Region` LIKE 'Nordic%'
    AND `IsOfficial` = 'T'
GROUP BY
    co.`code`
ORDER BY
    co.`SurfaceArea`
;

SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital',
    GROUP_CONCAT(la.`Language`) AS 'Language',
    co.`Region`,
    co.`Continent`,
    co.`SurfaceArea`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    -- co.`Continent` = 'Europe'
     `IsOfficial` = 'T'
GROUP BY
    co.`code`
ORDER BY
    co.`SurfaceArea` DESC
LIMIT 3
;



--
-- Inspect lifeExpectancy
--
SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital',
    GROUP_CONCAT(la.`Language`) AS 'Language',
    co.`Region`,
    co.`Continent`,
    co.`LifeExpectancy`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    `IsOfficial` = 'T'
    AND `LifeExpectancy` IS NOT NULL
    -- AND co.`Region` LIKE 'Nordic%'
    -- AND co.`Continent` = 'Europe'
GROUP BY
    co.`code`
ORDER BY
    co.`LifeExpectancy` DESC
LIMIT 10
;

