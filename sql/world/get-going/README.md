---
revision: 
    "2024-01-17": "(A, mos) Moved from another document, rewritten and scaled down."
---
World database and SQL: Get going
==============================

![world-report](.img/world-report.png)

This exercise is about get going with the world database and how to use SQL to inspect it. You will see how the backup file is imported to create a database and SQL is used to inspect the database. We use basic SELECT statements to build up reports and we see how to join two and three tables.

Perform the exercises and save all your SQL to a file.

[[_TOC_]]



Prepare
------------------------------

You have installed MariaDB or MySQL and you have access to the database using a terminal client or a desktop client.

The SQL code used in this exercise is available in the file [`dml.sql`](./dml.sql), use it as a way to view suggestions on solutions.



The world database
------------------------------

You can read about the [world database in the MySQL documentation](https://dev.mysql.com/doc/world-setup/en/). It is a database example that is commonly used to practice the basics of working with a database.

You should have access to the backup file of the world database which can be downloaded from the above site.

This repo includes a copy of the backup file at [`database/world.sql`](../../database/world.sql).



Import the database
------------------------------

Create a working directory and ensure that you save all the SQL you write into the file `dml.sql`.

Start by importing the backup of the database. You can place the backupfile in your working directory and label it `world.sql`.

Open up the backup file in your texteditor and inspect it. Do you see it contains SQL statement and that it starts to create a database called `world`?

Now proceed to import the database. Either from the terminal.

```
mariadb < world.sql
```

Or from within the terminal client.

```sql
MariaDB [world]> source world.sql
```

You can verify that it works by checking what tables you have in the database.

```sql
SHOW TABLES;
```

It should look something like this.

```
+-----------------+
| Tables_in_world |
+-----------------+
| city            |
| country         |
| countrylanguage |
+-----------------+
```



Verify the database schema and the content of the tables
------------------------------

When you first meet a new database you might want to know the structure of the schema. You can view that by checking how the tables were created and what they contain.

Run the following statements on each table.

```sql
SHOW CREATE TABLE `city` \G
```

The check what type of data that is included in the tables. Execute the following statement for each table.

```sql
SELECT * FROM `city` LIMIT 5;
```

When you try to view the country table you will find it contians a lot of columns. You might want to select just a few of those columns in your report.

```sql
SELECT
    `Code`,
    `Name`
FROM `country`
WHERE
    `Code`= 'SWE'
;
```

The above statement only shows Sweden, can you change it to also include Norway?

Save all SQL statements into your file. You might need them later on.



Reverse engineer the database model
------------------------------

If you have access to the desktop client Workbench, then you can try to extract the database model by reverse engineer the database schema and get a picture of how the database model looks like.

![world model](.img/world.png)

_Figure. The database model for the world database, reversed engineered by Workbench._

You can see that Workbench detected how the tables are organised and linked to each other. This image might be useful when you now starts to work with the database, you can see the table names and the names of the columns.



Investigate a country, for example `SWE`
------------------------------

What can we find out about a specific country by looking at the tables in the database?

Lets try to see what we can find out about Sweden.



### The country table

Start by finding the entry about Sweden in the country table.

```sql
SELECT `Code`, `Name` FROM `country` WHERE `Name` = 'Sweden';
SELECT `Code`, `Name` FROM `country` WHERE `Code` = 'SWE';
```

Both WHERE statements are valid to select Sweden from the table. I usually use the shorter one since it looks more like an id.

It is a lot of details about the country, can you limit it to output something like this?

```
+------+--------+-----------+-----------+------------------+-----------------+-----------+------------+----------------+
| Code | Name   | LocalName | Continent | Region           | HeadOfState     | IndepYear | Population | LifeExpectancy |
+------+--------+-----------+-----------+------------------+-----------------+-----------+------------+----------------+
| SWE  | Sweden | Sverige   | Europe    | Nordic Countries | Carl XVI Gustaf |       836 |    8861400 |           79.6 |
+------+--------+-----------+-----------+------------------+-----------------+-----------+------------+----------------+
```

Or make your own special report containing the details you want to see for the country.



### The countrylanguage table

Lets find out what languages that are spoken in Sweden.

```sql
SELECT * FROM `countrylanguage` WHERE `CountryCode`= 'SWE';
```

Which is the official language? Can you identify that from the table?

Try to order the report so you get the highest percentage on top. Like this.

```
+-------------+---------------------------+------------+------------+
| CountryCode | Language                  | IsOfficial | Percentage |
+-------------+---------------------------+------------+------------+
| SWE         | Swedish                   | T          |       89.5 |
| SWE         | Finnish                   | F          |        2.4 |
| SWE         | Southern Slavic Languages | F          |        1.3 |
| SWE         | Arabic                    | F          |        0.8 |
| SWE         | Spanish                   | F          |        0.6 |
| SWE         | Norwegian                 | F          |        0.5 |
+-------------+---------------------------+------------+------------+
```



### The city table

What are the cities in Sweden that are included in the city table?

```sql
SELECT * FROM `city` WHERE `CountryCode`= 'SWE';
```

Thats a lot of cities, do you know which one is the capital? Can you see that from the data in the city table?

There is an ID in the city table, what do you think it referes to? Can you find a similair ID in the country table? Try to investigate and find it out.

Here is what you can find out. Write your own SQL to verify that it is correct.

First in the country table that seems to specify an id of the capital.

```
+------+--------+---------+
| Code | Name   | Capital |
+------+--------+---------+
| SWE  | Sweden |    3048 |
+------+--------+---------+
```

Then in the city table where each city has an id.

```
+------+-----------+
| ID   | Name      |
+------+-----------+
| 3048 | Stockholm |
+------+-----------+
```

Can you make any sense of it? Do you think it is possible to connect the two tables and get one report containing the language and its capital?



Join the country and city tables
------------------------------

Lets create a report that looks like this. It is the country with its capital.

```
+------+---------+-----------+
| Code | Country | Capital   |
+------+---------+-----------+
| SWE  | Sweden  | Stockholm |
+------+---------+-----------+
```

The idea is to join the data from two tables and link the matching rows using the id of the capital. That way we can match the country with its correct capital.



### Join in a ugly way

Our first try can be an "ugly join" that selects from two tables and connects the capital with the country using a WHERE statement.

```sql
--
-- Join country and capital (ugly)
--
SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital'
FROM 
    `country` AS co,
    `city` AS ca
WHERE
    ca.`ID` = co.`Capital`
;
```

The above  statement has two important parts when it comes to a JOIN, first the part where we select from two tables.

```sql
FROM 
    `country` AS co,
    `city` AS ca
```

Then the part that matches the entires in the two tables so only tha valid and matching rows appear in the report.

```sql
WHERE
    ca.`ID` = co.`Capital`
```



### Cross join, good to know about

If you take the join statement above and remove the WHERE statement you will get a CROSS JOIN. A cross join is usually not what you want since it creates a report all rows matching all rows.

Lets say the city table contains 10 rows and the country table contains 10 rows. A cross join will result in 10 x 10 rows, that is 100 rows that in this case does not match in a logical sence.

So, if you get to many rows while joining, suspect a cross join and check the statement you use to connect the tables.



### Join nicely

The ugly join can be rewritten into a more nice looking join, First we use the JOIN statement to select the tables to join from.

```sql
FROM `country` AS co
    JOIN `city` AS ca
```

This states that we are selecting data from one table and we are joining with another table. 

Then we add the ON condition to match the rows.

```sql
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
```

This is a more nice structure to perform the join. It looks like this in total when we also exclude all countries except Sweden.

```sql
SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital'
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
WHERE
    co.`Code` = 'SWE'
;
```

This is how it looks when we execute the statement.

```sql
MariaDB [world]> SELECT 
    ->     co.`Code`,
    ->     co.`Name` AS 'Country',
    ->     ca.`Name` AS 'Capital'
    -> FROM `country` AS co
    ->     JOIN `city` AS ca
    ->         ON ca.`ID` = co.`Capital`
    -> WHERE
    ->     co.`Code` = 'SWE'
    -> ;
+------+---------+-----------+
| Code | Country | Capital   |
+------+---------+-----------+
| SWE  | Sweden  | Stockholm |
+------+---------+-----------+
```



Join three tables
------------------------------

It is rather easy to add a join with a third table. Lets say we want to include the primary language spoken in the country. In the countrylanguage table we will find the country code that can match with the languages for a specific country.

It looks like this in the countrylanguage table.

```sql
MariaDB [world]> SELECT * FROM `countrylanguage` WHERE `CountryCode`= 'SWE';
+-------------+---------------------------+------------+------------+
| CountryCode | Language                  | IsOfficial | Percentage |
+-------------+---------------------------+------------+------------+
| SWE         | Arabic                    | F          |        0.8 |
| SWE         | Finnish                   | F          |        2.4 |
| SWE         | Norwegian                 | F          |        0.5 |
| SWE         | Southern Slavic Languages | F          |        1.3 |
| SWE         | Spanish                   | F          |        0.6 |
| SWE         | Swedish                   | T          |       89.5 |
+-------------+---------------------------+------------+------------+
```

The join statement we need to add looks like this.

```sql
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
```

The final statement looks like this when we alos include to limit the report to the official languages.

```sql
SELECT 
    co.`Code`,
    co.`Name` AS 'Language',
    ca.`Name` AS 'Capital',
    la.`Language`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    co.`Code` = 'SWE'
    AND `IsOfficial` = 'T'
;
```

The resulting report looks like this.

```
+------+----------+-----------+----------+
| Code | Language | Capital   | Language |
+------+----------+-----------+----------+
| SWE  | Sweden   | Stockholm | Swedish  |
+------+----------+-----------+----------+
```

If you would join a fourth table would you know how to do it?

Just add a JOIN ON part.



Find details on all europeean/nordic countries
------------------------------

The countries are ordered in continents and regions. Try to find out what continents and regions are available.

The DISTINCT part lets us exclude the duplicates.

```sql
SELECT DISTINCT `Region` FROM `country` ORDER BY `Region`;
SELECT DISTINCT `Continent` FROM `country` ORDER BY `Continent`;
```

Can you now create a report that displays all the nordic countries? It should look like this.

```
+------+------------------------+------------------------+-----------+------------------+-----------+
| Code | Country                | Capital                | Language  | Region           | Continent |
+------+------------------------+------------------------+-----------+------------------+-----------+
| DNK  | Denmark                | København              | Danish    | Nordic Countries | Europe    |
| FIN  | Finland                | Helsinki [Helsingfors] | Finnish   | Nordic Countries | Europe    |
| FIN  | Finland                | Helsinki [Helsingfors] | Swedish   | Nordic Countries | Europe    |
| FRO  | Faroe Islands          | Tórshavn               | Danish    | Nordic Countries | Europe    |
| FRO  | Faroe Islands          | Tórshavn               | Faroese   | Nordic Countries | Europe    |
| ISL  | Iceland                | Reykjavík              | Icelandic | Nordic Countries | Europe    |
| NOR  | Norway                 | Oslo                   | Norwegian | Nordic Countries | Europe    |
| SJM  | Svalbard and Jan Mayen | Longyearbyen           | Norwegian | Nordic Countries | Europe    |
| SWE  | Sweden                 | Stockholm              | Swedish   | Nordic Countries | Europe    |
+------+------------------------+------------------------+-----------+------------------+-----------+
```

If we view the report we see that there are two rows for Finland and two for Faroe Islands. Those countries seems to have two primary language. We can see that if the look in the countrlanguage table in detail.

```
MariaDB [world]> SELECT
    ->     *
    -> FROM `countrylanguage`
    -> WHERE
    ->     `CountryCode`IN ('FIN', 'FRO')
    ->     AND `IsOfficial` = 'T'
    -> ;
+-------------+----------+------------+------------+
| CountryCode | Language | IsOfficial | Percentage |
+-------------+----------+------------+------------+
| FIN         | Finnish  | T          |       92.7 |
| FIN         | Swedish  | T          |        5.7 |
| FRO         | Danish   | T          |        0.0 |
| FRO         | Faroese  | T          |      100.0 |
+-------------+----------+------------+------------+
```

If we then really would like to have a single row for each country, then we can use the construct GROUP_CONCAT that groups several entries into a single column. 

Let me show you how it is used.

```sql
SELECT 
    co.`Code`,
    co.`Name` AS 'Country',
    ca.`Name` AS 'Capital',
    GROUP_CONCAT(la.`Language`) AS 'Language',
    co.`Region`,
    co.`Continent`
FROM `country` AS co
    JOIN `city` AS ca
        ON ca.`ID` = co.`Capital`
    JOIN `countrylanguage` AS la
        ON co.`Code` = la.`CountryCode` 
WHERE
    co.`Region` LIKE 'Nordic%'
    AND `IsOfficial` = 'T'
GROUP BY
    co.`code`
;
```

The important parts are these two.

```sql
    GROUP_CONCAT(la.`Language`) AS 'Language',
...
GROUP BY
    co.`code`
```

The first concatenates all values intoa a single column. The second states that it is the country code that is important to list all entries for.

The result looks like this.

```
+------+------------------------+------------------------+-----------------+------------------+-----------+
| Code | Country                | Capital                | Language        | Region           | Continent |
+------+------------------------+------------------------+-----------------+------------------+-----------+
| DNK  | Denmark                | København              | Danish          | Nordic Countries | Europe    |
| FIN  | Finland                | Helsinki [Helsingfors] | Finnish,Swedish | Nordic Countries | Europe    |
| FRO  | Faroe Islands          | Tórshavn               | Danish,Faroese  | Nordic Countries | Europe    |
| ISL  | Iceland                | Reykjavík              | Icelandic       | Nordic Countries | Europe    |
| NOR  | Norway                 | Oslo                   | Norwegian       | Nordic Countries | Europe    |
| SJM  | Svalbard and Jan Mayen | Longyearbyen           | Norwegian       | Nordic Countries | Europe    |
| SWE  | Sweden                 | Stockholm              | Swedish         | Nordic Countries | Europe    |
+------+------------------------+------------------------+-----------------+------------------+-----------+
```

Are you getting the hang of how to create a larger and larger report?



Find the countries with the largest and smallest area
------------------------------

Lets create a few reports to learn how large and small the countries might be. Try to create select statemenets that help you answer the following questions.

1. Show all nordic countries and order the report by area. First show the largest area on top and then change the report to show the smallest area on top. This is the largest and smallest country in the nordic countries?
1. Now show the same for the European countries. Create a report that shows the top three countries with the largest area and another report showing the smallest countries by area.

There area some really small countries in Europe.

```
+------+-------------------------------+---------------------+----------+-----------------+-----------+-------------+
| Code | Country                       | Capital             | Language | Region          | Continent | SurfaceArea |
+------+-------------------------------+---------------------+----------+-----------------+-----------+-------------+
| VAT  | Holy See (Vatican City State) | Città del Vaticano  | Italian  | Southern Europe | Europe    |        0.40 |
| MCO  | Monaco                        | Monaco-Ville        | French   | Western Europe  | Europe    |        1.50 |
| GIB  | Gibraltar                     | Gibraltar           | English  | Southern Europe | Europe    |        6.00 |
+------+-------------------------------+---------------------+----------+-----------------+-----------+-------------+
```

Can you edit your report to find the smallest and largest countries, by area, in the whole world? In my report it seems like the "Vatican City State" is on top of one list and "Russia" on the other. 



Find the places with the highest and lowest life expectancy
------------------------------

The value of LifeExpectancy shows how long the people live their lifes in their respective country. It might be an indicator on how healthy the life is in the country.

1. Show the life expectancy in the nordic countries and review which country is in top and which is in the bottom.
1. Review the same report for the european countries. Which country is in top and which is in the bottom? Can you calculate the percentage in how longer the people live in the top country, compared to the bottom country?
1. Review the same report for the top 10 countries in the whole world and the 10 bottom countries in the world. Exclude all countries that has NULL as LifeExpectancy (IS NOT NULL).

YOu might find that there can be a large difference in the expected amount of years one live depending on where you were born and live.

Here is what I get of the highest life expectancy in the world.

```
+------+-------------+------------------+-------------------------------+---------------------------+---------------+----------------+
| Code | Country     | Capital          | Language                      | Region                    | Continent     | LifeExpectancy |
+------+-------------+------------------+-------------------------------+---------------------------+---------------+----------------+
| AND  | Andorra     | Andorra la Vella | Catalan                       | Southern Europe           | Europe        |           83.5 |
| MAC  | Macao       | Macao            | Portuguese                    | Eastern Asia              | Asia          |           81.6 |
| SMR  | San Marino  | San Marino       | Italian                       | Southern Europe           | Europe        |           81.1 |
| JPN  | Japan       | Tokyo            | Japanese                      | Eastern Asia              | Asia          |           80.7 |
| SGP  | Singapore   | Singapore        | Chinese,Malay,Tamil           | Southeast Asia            | Asia          |           80.1 |
| AUS  | Australia   | Canberra         | English                       | Australia and New Zealand | Oceania       |           79.8 |
| CHE  | Switzerland | Bern             | French,German,Italian,Romansh | Western Europe            | Europe        |           79.6 |
| SWE  | Sweden      | Stockholm        | Swedish                       | Nordic Countries          | Europe        |           79.6 |
| HKG  | Hong Kong   | Victoria         | English                       | Eastern Asia              | Asia          |           79.5 |
| CAN  | Canada      | Ottawa           | English,French                | North America             | North America |           79.4 |
+------+-------------+------------------+-------------------------------+---------------------------+---------------+----------------+
```



Summary
------------------------------

This exercise was to get you going with a database that you did not know about, how to import it and start inspecting its schema and content. You learned how to create reports and how to join tables. All together, this should be a good start of your journey to become skilled in database management.

Just continue to practice.

Perhaps you can continue to the next exercise "[World database and SQL: Aggregate and builtin functions](../aggregate/README.md)".
