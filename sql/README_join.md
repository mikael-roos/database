Visual representation of INNER and OUTER JOIN
==========================

When you learn to join tables, it can sometimes be easier if you have a visual representation of how a JOIN works on two tables. Here are resources that can help with that.

[[_TOC_]]



Most common ways to JOIN
-------------------------

Normally, there are three common JOINS you work with initially when learning databases.

1. INNER JOIN (show the rows from the tables that match the condition)
2. LEFT [OUTER] JOIN (show all rows in the **left** table and the rows from the **right** table that match the condition)
RIGHT [OUTER] JOIN (show all rows in the **right** table and the rows from the **left** table that match the condition)

LEFT and RIGHT OUTER are thus the same thing, the only difference is which table you start from and where you want to see all values, even the rows that do not match the condition.

Details about which variants of JOIN are supported in the database can be studied in the manual.

* [MariaDB JOIN](https://mariadb.com/kb/en/join-syntax/)
* [MySQL JOIN](https://dev.mysql.com/doc/refman/8.0/en/join.html)



Resources for JOIN
-------------------------

Example of a visual explanation of JOIN with [Venn diagram](https://en.wikipedia.org/wiki/Venn_diagram) which shows a logical relationship between two sets.

* [SQL Join Chart - Custom Poster Size](https://www.reddit.com/r/SQL/comments/aysflk/sql_join_chart_custom_poster_size/)

![Visual_SQL_JOINS_orig](img/join-red.jpg)

* [Visual Representation of SQL Joins](https://www.codeproject.com/Articles/33052/Visual-Representation-of-SQL-Joins)

![dyqnzpuddxk21](img/join-blue.png)

* [SQL Joins](https://commons.wikimedia.org/wiki/File:SQL_Joins.svg)

![SQL_Joins svg](img/join-wiki.png)



Explaining more ways to JOIN
-------------------------

### CROSS JOIN

For all rows in table A, match all rows in table B.

If table A has 3 rows and table B has 5 rows, then the resultset will have 3 x 5 = 15 rows.

This is like an INNER JOIN with no condition. Add the condition to make it a INNER JOIN.

* [SQL CROSS JOIN with examples](https://www.sqlshack.com/sql-cross-join-with-examples/)



### NATURAL JOIN

JOINS where the columns has the same name. Matching the rows is purely done using the column names. No other condition is used.

* [Difference between Natural join and Inner Join in SQL](https://www.geeksforgeeks.org/difference-between-natural-join-and-inner-join-in-sql/)



### FULL OUTER JOIN

FULL OUTER JOIN returns a result set that includes rows from both left and right tables.

When no matching rows exist for the row in the left table, the columns of the right table will contain NULL. Likewise, when no matching rows exist for the row in the right table, the column of the left table will contain NULL.

* [SQL Server Full Outer Join](https://www.sqlservertutorial.net/sql-server-basics/sql-server-full-outer-join/)

FULL OUTER JOIN is not supported in MariaDB or MySQL but you solve it using two OUTER JOINS that you perform a UNION on.

```sql
SELECT * FROM t1
LEFT JOIN t2 ON t1.id = t2.id
UNION
SELECT * FROM t1
RIGHT JOIN t2 ON t1.id = t2.id
```

Or using UNION ALL to avoid duplicate rows in the resultset.

```sql
SELECT * FROM t1
LEFT JOIN t2 ON t1.id = t2.id
UNION ALL
SELECT * FROM t1
RIGHT JOIN t2 ON t1.id = t2.id
WHERE t1.id IS NULL
```
