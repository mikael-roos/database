---
revision: 
    "2024-01-17": "(A, mos) Moved from another document and improved."
---
Get going with a database and SQL
==============================

![work](.img/work.png)

This exercise shows how to create a database from scratch and let you use SQL to add, update and query data in the table.

Perform the exercises and save all your SQL to a file.

[[_TOC_]]



<!--
Video
------------------------------

This is a recorded presentation, 12 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/tELvRoiho9I/0.jpg)](https://www.youtube.com/watch?v=tELvRoiho9I)
-->



Prepare
------------------------------

You have installed MariaDB or MySQL and you have acces to the database using a terminal client or a desktop client.



Save and execute SQL script
------------------------------

Save all the code you write in a file and name it `ladok.sql`.

Ensure that you can rerun all commands from the file, without any errors, like this in the terminal.

```
mariadb --table < ladok.sql
```

Or like this from inside the terminal client.

```
MariaDB [ladok]> source ladok.sql
```



The database model
------------------------------

We are to create a database were we can store courses with their course code, the name of the course and the amount of hp for each course.



Create the database
------------------------------

Login to the database server and use SQL to create the database and name it "ladok".

```sql
--
-- CREATE/DROP database
--
DROP DATABASE IF EXISTS `ladok`;
CREATE DATABASE IF NOT EXISTS `ladok`;

USE `ladok`;
```



Create the database schema
------------------------------

In the database we need one table like this.

* course
    * course code
    * name of course
    * hp of course

We can simplify that by writing like this.

```
course (code, name, hp)
```

We use SQL to create the database table.

```sql
--
-- CREATE/DROP table
--
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
    `code` CHAR(6) NOT NULL,
    `name` VARCHAR(80) NOT NULL,
    `hp` DECIMAL(3,1) NOT NULL DEFAULT 7.5,

    PRIMARY KEY (`code`)
);
```

Did we use tha correct data types? Check what [data types that are available in MariaDB](https://mariadb.com/kb/en/data-types/).

Do we need a primary key? Did we choose the correct primary key?

Use the following statement to check how the table was actually created.

```sql
SHOW CREATE TABLE `course` \G;
```

The argument `\G` creates row-based output instead of tabular output.

Check i the table contain any data (most likely no).

```sql
SELECT * FROM `course`;
```



Add rows to the table
------------------------------

Lets add some rows to the table.

First we add one row.

```sql
--
-- INSERT rows into the table
--
INSERT INTO `course` VALUES
    ('DV1605', 'Databasteknologier för webben', 10)
;
```

Ensure that the row is in the table using a SELECt statement.

Then we add another two rows into the table.

```sql
INSERT INTO `course` VALUES
    ('DV1606', 'Databasteknologier för webben', 7.5),
    ('DV1531', 'Programmering och Problemlösning i Python', 7.5)
;
```

Try to merge it all into one single INSERT statement. 

Rerun the whole script and assure that tha table contains the the rows using a SELECT statement.

It should look something like this.

```
+--------+--------------------------------------------+------+
| code   | name                                       | hp   |
+--------+--------------------------------------------+------+
| DV1531 | Programmering och Problemlösning i Python  |  7.5 |
| DV1605 | Databasteknologier för webben              | 10.0 |
| DV1606 | Databasteknologier för webben              |  7.5 |
+--------+--------------------------------------------+------+
3 rows in set (0,000 sec)
```



Create reports from the table
------------------------------

We can create various reports from the data in the table by using the SELECT statement.

We have already tried to view all the rows in the table.

```sql
--
-- SELECT to show reports
--
SELECT * FROM `course`;
```

We can decide that we only want to see one column.

```sql
SELECT name FROM `course`;
```

We can write a more complex report to include a WHERE statement and decide on how to ORDER the results.

```sql
SELECT
    * 
FROM `course`
WHERE 
    `hp` > 7.5
    OR `code` = 'DV1531'
    OR `name` LIKE 'Data%'
ORDER BY `code`
;
```

The result is still that all rows are displayed. Can you edit the statement to only show a specific row in the table?



Update columns and rows in the table
------------------------------

With the UPDATE statement can you update rows and columns in the table.

Lets say we want to change the course code from DV1531 to DV1670. Here is an UPDATE statement that does that.

```sql
--
-- UPDATE data in the rows
--
UPDATE `course` SET
    `code` = 'DV1670'
WHERE
    `code` = 'DV1531'
;
```

Verify that the change happened.

What happens if you remove the WHERE part? Try it. If needed, rerun the script to reset your data.



Delete rows from the table
------------------------------

You can delete rows from the table using the DELETE statement.

This is how to delete a specific row using the WHERE part.

```sql
--
-- DELETE rows from table
--
DELETE FROM `course` WHERE `code` = 'DV1670';
```

Verify that the row was deleted.

This is how to delete all rows from a table,


```sql
DELETE FROM `course`;
```

Verify that the table is empty.



Remove a table and a database
------------------------------

You saw how we used the DROP statment in the beginning of this article.

This is how we can remove the table from the schema.

```sql
DROP TABLE `course`;
```

This is how to remove the schema (database) completely.

```sql
DROP DATABASE `ladok`;
```

For the final time, rerun your whole script and ensure that it works as expected.



Summary
------------------------------

You have practised building a database using SQL.

You can see all the SQL commands in the file [`ladok.sql`](./ladok.sql) and compare with your own file.
