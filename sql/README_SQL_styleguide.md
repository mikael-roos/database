SQL Style Guide
========================

Is is of importance to follow style guides when coding in all programming languages.

When we code SQL we could follow the following guide:

* [SQL Style Guide](https://www.sqlstyle.guide/)

If you disagree with things in the styleguide, then read:

* [SQL style guide misconceptions](https://www.simonholywell.com/post/2016/12/sql-style-guide-misconceptions/)
