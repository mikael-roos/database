import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    public static void main(String args[]) {
        Connection conn = null;
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";

        try {
            System.out.println("Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            conn = DriverManager.getConnection(dsn);

            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
}
