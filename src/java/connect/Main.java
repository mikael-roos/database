import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


public class Main {
    public static void main(String args[]) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet res = null;
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";
        String sql = null;

        try {
            System.out.println("Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            conn = DriverManager.getConnection(dsn);

            stmt = conn.createStatement();
            sql = "SELECT 1 + 1 AS Sum";
            res = stmt.executeQuery(sql);

            System.out.println("The SQL query is:\n " + sql);
            System.out.println("The resultset is:\n " + res);
            res.next();
            System.out.println("Sum is: " + res.getInt("Sum"));

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
}
