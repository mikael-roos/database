How to connect Java to MySQL
============================

This is a working code example that connects a Java program to a MySQL server using the "[MySQL Connector/J 8.0 Developer Guide](https://dev.mysql.com/doc/connector-j/8.0/en/)".



Database world
----------------------------

The example is setup to use the world database. If you do not have it you could set it up or you can edit the example programs to use another database.



Set your CLASSPATH
----------------------------

Add the jar file to your classpath.

```
export CLASSPATH="../mysql-connector-java-8.0.28.jar:$CLASSPATH"
```

Check your current classpath.

```
$ echo $CLASSPATH
../mysql-connector-java-8.0.28.jar:
```



Run the Connect.java
----------------------------

The first sample is a small program that just performs a connection to the database manager.

Run it.

```
javac Connect.java
java Connect
```

The output could look like this.

```
$ java Connect
Lets connect to the database!
The connection string is:
 jdbc:mysql://localhost/world?user=maria&password=P@ssw0rd
```

That means success.



Run the Main.java
----------------------------

This example program connects and executes a generic SQL SELECT query to the database.

Run it.

```
javac Main.java
java Main
```

The output could look like this.

```
Lets connect to the database!
The connection string is:
 jdbc:mysql://localhost/world?user=maria&password=P@ssw0rd
The SQL query is:
 SELECT 1 + 1 AS Sum
The resultset is:
 com.mysql.cj.jdbc.result.ResultSetImpl@7a3d45bd
Sum is: 2
```

That means success.
