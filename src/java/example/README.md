How to connect Java to MySQL
============================

This is a set of Java example programs that uses JDBC and the package `java.sql` to connect to a MySQL database through a MySQL connector jar file.

You can [download this example as a zip file](../example.zip) to make it easy to use it on your own environment.

The example has been tested on Windows, Linux and Mac.

[[_TOC_]]



Video
---------------------------

The following video show when I work through parts of this article.

[![](http://img.youtube.com/vi/NimK3ZAo8jY/0.jpg)](http://www.youtube.com/watch?v=NimK3ZAo8jY "YouTube: Use Java and JDBC  to connect to the MySQL database")



Precondition
----------------------------

You have installed MySQL server and it is up and running.



Documentation
----------------------------

MySQL has the connector and you can read about it in "[MySQL Connector/J 8.0 Developer Guide](https://dev.mysql.com/doc/connector-j/8.0/en/)".

The documentation for the Java API for SQL (JDBC) is available in the [documentation of the package java.sql](https://docs.oracle.com/javase/8/docs/api/java/sql/package-summary.html).

JDBC means "[Java Database Connectivity](https://en.wikipedia.org/wiki/Java_Database_Connectivity)" and is a Java implementation of how to access a relational database.

To learn more you can read the article "[Lesson: JDBC Basics](https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html)" on The Java Tutorials.



Database world
----------------------------

Parts of the examples uses the world database. [Download it and set it up](https://dev.mysql.com/doc/world-setup/en/world-setup-installation.html) to fully enjoy the examples.

You can verify that it worked by this.

```
$ mysql world -e "SHOW TABLES"
+-----------------+
| Tables_in_world |
+-----------------+
| city            |
| country         |
| countrylanguage |
+-----------------+
```



Connection details
----------------------------

To be able to connect to the database you need connection details. The example programs are set upp to use the following connection details.

* host = localhost
* user = maria
* pass = P@ssw0rd
* database = world
* port = 3306

You can edit the connection details in the example programs to match your environment or you can [create the user 'maria' using SQL](https://gitlab.com/mikael-roos/database/-/blob/main/sql/utility/create-user-maria.sql).

There is a [video showing how to create the 'maria' user in the database](https://youtu.be/YbycGfMA1XY), so check that out if you are unfamiliar with how to create a user.

You can verify that it works like this.

```
$ mysql -umaria -pP@ssw0rd -hlocalhost world -e "SELECT USER()"
+-----------------+
| USER()          |
+-----------------+  
| maria@localhost |
+-----------------+
```

These connection details are also sometimes called a [Data Source Name (DSN)](https://en.wikipedia.org/wiki/Data_source_name).



MySQL Connector Java jar file
----------------------------

The MySQL Connector Java jar is included with the example programs, so there is nothing you need to do to get that working. The jar file is in the `lib/` directory.

```
lib/mysql-connector-java-8.0.28.jar  
```



### Upgrade the connector jar file

If you want to get a later version then there are complete instructions on how to [get and setup the jar file](https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-binary-installation.html).

Here follows the short story on how to do it.

Download the jar file from here.

* https://dev.mysql.com/downloads/connector/j/

Choose "Platform Independant" and choose the zip (or tar) distribution.

Move the file into your working directory. It could look like this if you choosed the zip file. You should be using the latest version.

```
mysql-connector-java-8.0.28.zip
```

Extract the zip archive file and the jar-file is available in the directory.

```
mysql-connector-java-8.0.28/mysql-connector-java-8.0.28.jar
```

Then move it to the lib directory and remove the older version.

```
lib/mysql-connector-java-8.0.28.jar  
```



Set your CLASSPATH
----------------------------

To be able to run the example programs you need to add the jar file to your classpath.

You may add the classpath when running the programs like this, in the example the `Connect` is representing the example main program.

```
# On Windows CMD, Powershell, Git Bash, Cygwin
java -cp "lib/*;" Connect

# On Linux, Mac
java -cp "lib/*:" Connect
```

The main difference between the platforms is that Windows uses `;` as separator and Mac/Linux uses `:` to separate items in the classpath.

You can also set the environment variable CLASSPATH to reflect your setup.

```
# Windows CMD
# Use the absolute path to the library
set CLASSPATH=".;C:\Users\me\database\src\java\example\lib\*;"

# Windows Powershell
$env:CLASSPATH="lib\*;"

# Windows Git Bash, Cygwin
export CLASSPATH="lib/*;"

# Linux, Mac
export CLASSPATH="lib/*:"
```

You can now compile and run the program like this.

```
javac Connect.java
java Connect
```

You can troubleshoot by checking the current value of your CLASSPATH like this.

```
# Windows CMD
echo %CLASSPATH%

# Windows Powershell
$env:CLASSPATH

# Windows Git Bash, Cygwin
# Linux, Mac
echo $CLASSPATH
```



Run Connect.java
----------------------------

The first sample is a small program that just performs a connection to the database manager and print out some details.

Run it.

```
javac Connect.java
java Connect
```

The output could look like this.

```
$ javac Connect.java && java Connect
Lets connect to the database!
The connection string is:
 jdbc:mysql://localhost/world?user=maria&password=P@ssw0rd
The connection was successful
com.mysql.cj.jdbc.ConnectionImpl@2d8f65a4
```

An output like above means success.



Run SelectOne.java
----------------------------

This example program connects and executes a generic SQL SELECT query to the database.

The resultset only contains one row and one value.

Run it.

```
javac SelectOne.java
java SelectOne
```

The output could look like this.

```
$ javac SelectOne.java && java SelectOne
# Lets connect to the database!
The connection string is:
 jdbc:mysql://localhost/world?user=maria&password=P@ssw0rd
The connection was successful
com.mysql.cj.jdbc.ConnectionImpl@2d8f65a4

# Create a statement & execute a query
The SQL query is:     
 SELECT 1 + 1 AS Sum
The resultset is:     
 com.mysql.cj.jdbc.result.ResultSetImpl@1e7c7811    
Sum is: 2
```

An output like above means success.



Run more examples
----------------------------

The other examples are setup the same way. Run them and inspect the output and the source code.



### SelectMany.java

This example uses prepared statements to execute a query that shows a subset of the countries in the world database.



### Search.java

This examples let the user search through the countries in the world database by entering a string to search for.



### Insert.java

This examples inserts a new country into the database.



### Update.java

This examples updates the name and region of a specific country.



### Delete.java

This examples deletes a country from the database.
