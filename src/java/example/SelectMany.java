import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SelectMany {
    public static void main(String args[]) {
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";

        try {
            // Do the connection
            System.out.println("# Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            Connection con = DriverManager.getConnection(dsn);

            System.out.println("The connection was successful");
            System.out.println(con);

            // Create statement
            System.out.println("\n# Create a statement & execute a query");
            String sql = "SELECT "
                       + "    code AS Code, "
                       + "    name AS Name, "
                       + "    region AS Region "
                       + "FROM country "
                       + "LIMIT ? ";
            PreparedStatement stm = con.prepareStatement(sql);

            // Execute the query and get the resultset
            Integer limit = 7;
            stm.setInt(1, limit);
            ResultSet res = stm.executeQuery();

            System.out.println("The SQL query is:\n " + sql + "\n");
            System.out.println("The PreparedStatement is:\n " + stm);
            System.out.println("The resultset is:\n " + res);

            // Print out the resultset
            String sep = "+------+---------------------------+---------------------------+";
            System.out.println(sep);
            System.out.printf("| %4s | %-25s | %-25s |\n",
                "Code",
                "Name",
                "Region"
            );
            System.out.println(sep);
            while(res.next()) {
                System.out.printf("| %-4s | %-25s | %-25s |\n",
                    res.getString("Code"),
                    res.getString("Name"),
                    res.getString("Region")
                );
            }
            System.out.println(sep);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
}
