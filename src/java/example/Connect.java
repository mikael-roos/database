import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    public static void main(String args[]) {
        String jdbcUrl = "jdbc:mysql://localhost:3306/world";
        String user = "maria";
        String password = "P@ssw0rd";

        try {
            // Do the connection
            System.out.println("# Lets connect to the database!");
            System.out.println("The connection string is:\n " + jdbcUrl + ";" + user + ";" + password);

            Connection con = DriverManager.getConnection(jdbcUrl, user, password);

            System.out.println("The connection was successful");
            System.out.println(con);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
}
