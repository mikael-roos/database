import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class Update {
    public static void main(String args[]) {
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";

        try {
            // Do the connection
            System.out.println("# Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            Connection con = DriverManager.getConnection(dsn);

            System.out.println("The connection was successful");
            System.out.println(con);

            // Create statement
            System.out.println("\n# Create a statement & execute a query");
            String sql = "UPDATE country SET "
                       + "    name = ?, "
                       + "    region = ? "
                       + "WHERE "
                       + "    code = ? ";
            PreparedStatement stm = con.prepareStatement(sql);

            // Read input from the user
            Scanner sc = new Scanner(System.in);
            System.out.println("\n# Update country name and region");
            System.out.print("Code (which country to update): ");
            String code = sc.nextLine();
            System.out.print("Name (update name): ");
            String name = sc.nextLine();
            System.out.print("Region (update region): ");
            String region = sc.nextLine();

            // Execute the query and get the resultset
            stm.setString(3, code);
            stm.setString(1, name);
            stm.setString(2, region);
            System.out.println("The PreparedStatement is:\n " + stm);
            stm.executeUpdate();

            // Check details on the update
            Integer rowsAffected = stm.getUpdateCount();
            System.out.println("Rows affected of last statement: " + rowsAffected);

        } catch (SQLException ex) {
            printSQLException(ex);
        }
    }

    public static void printSQLException(SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
    }
}
