import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class Search {
    public static void main(String args[]) {
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";

        try {
            // Do the connection
            System.out.println("# Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            Connection con = DriverManager.getConnection(dsn);

            System.out.println("The connection was successful");
            System.out.println(con);

            // Create statement
            System.out.println("\n# Create a statement & execute a query");
            String sql = "SELECT "
                       + "    code AS Code, "
                       + "    name AS Name, "
                       + "    region AS Region "
                       + "FROM country "
                       + "WHERE "
                       + "    code LIKE ? "
                       + "    OR name LIKE ? "
                       + "    OR region LIKE ? "
                       + "LIMIT ?";
            PreparedStatement stm = con.prepareStatement(sql);

            // Read searchstring from the user
            Scanner sc = new Scanner(System.in);
            String search = null;
            String searchAll = null;
            Integer limit = 7;
            while (true) {
                System.out.print("\n# Enter the searchstring (q to quit): ");
                search = sc.nextLine();
                if (search.equals("q")) {
                    break;
                }

                // Execute the query and get the resultset
                searchAll = "%" + search + "%";
                stm.setString(1, search);
                stm.setString(2, searchAll);
                stm.setString(3, searchAll);
                stm.setInt(4, limit);
                System.out.println("The PreparedStatement is:\n " + stm);
                ResultSet res = stm.executeQuery();

                // Print out the resultset
                printCountryTable(res);
            }

        } catch (SQLException ex) {
            printSQLException(ex);
        }
    }

    public static void printCountryTable(ResultSet res)
        throws SQLException
    {
        String sep = "+------+---------------------------+---------------------------+";
        System.out.println(sep);
        System.out.printf("| %4s | %-25s | %-25s |\n",
            "Code",
            "Name",
            "Region"
        );
        System.out.println(sep);
        while(res.next()) {
            System.out.printf("| %-4s | %-25s | %-25s |\n",
                res.getString("Code"),
                res.getString("Name"),
                res.getString("Region")
            );
        }
        System.out.println(sep);
    }

    public static void printSQLException(SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
    }
}
