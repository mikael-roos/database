import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class SelectOne {
    public static void main(String args[]) {
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";

        try {
            // Do the connection
            System.out.println("# Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            Connection con = DriverManager.getConnection(dsn);

            System.out.println("The connection was successful");
            System.out.println(con);

            // Create statement
            System.out.println("\n# Create a statement & execute a query");
            Statement stm = con.createStatement();
            String sql = "SELECT 1 + 1 AS Sum";

            // Execute the query and get the resultset
            ResultSet res = stm.executeQuery(sql);
            res.next();

            System.out.println("The SQL query is:\n " + sql);
            System.out.println("The resultset is:\n " + res);
            System.out.println("Sum is: " + res.getInt("Sum"));

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
}
