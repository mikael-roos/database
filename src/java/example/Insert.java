import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class Insert {
    public static void main(String args[]) {
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";

        try {
            // Do the connection
            System.out.println("# Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            Connection con = DriverManager.getConnection(dsn);

            System.out.println("The connection was successful");
            System.out.println(con);

            // Create statement
            System.out.println("\n# Create a statement & execute a query");
            String sql = "INSERT INTO country "
                       + "    (code, name, region) "
                       + "VALUES "
                       + "    (?, ?, ?) ";
            PreparedStatement stm = con.prepareStatement(sql);

            // Read input from the user
            Scanner sc = new Scanner(System.in);
            System.out.println("\n# Add a new country: ");
            System.out.print("Code: ");
            String code = sc.nextLine();
            System.out.print("Name: ");
            String name = sc.nextLine();
            System.out.print("Region: ");
            String region = sc.nextLine();

            // Execute the query and get the resultset
            stm.setString(1, code);
            stm.setString(2, name);
            stm.setString(3, region);
            System.out.println("The PreparedStatement is:\n " + stm);
            stm.executeUpdate();

            // Check details on the update
            Integer rowsAffected = stm.getUpdateCount();
            System.out.println("Rows affected of last statement: " + rowsAffected);

        } catch (SQLException ex) {
            printSQLException(ex);
        }
    }

    public static void printSQLException(SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
    }
}
