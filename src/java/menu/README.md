Java connect to MySQL
=======================

This a menu driven program connecting to the world database and performing some queries and printing out the resultsets.



Prepare
-----------------------

You need to have the world database installed.

```
# In the root of this repo
mysql < database/world.sql

mysql world -e "SELECT name FROM country WHERE code='SWE'"
+--------+
| name   |
+--------+
| Sweden |
+--------+
```



Run the program
-----------------------

The main program is Main. Compile, set the classpath and run.

```
# Compile
javac Main.java

# Set the classpath and run in one command
CLASSPATH="../mysql-connector-java-8.0.28.jar:$CLASSPATH" java Main
```

The file `database.java` contains the connection details to the database, update those if needed.



Read more
-----------------------

Learn through the tutorial.

* [Lesson: JDBC Basics](https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html)
    * [Using Prepared Statements](https://docs.oracle.com/javase/tutorial/jdbc/basics/prepared.html)

Read the documentation.

* [Documentation java.sql package](https://docs.oracle.com/javase/8/docs/api/java/sql/package-summary.html)
* [MySQL Connector/J 8.0 Developer Guide](https://dev.mysql.com/doc/connector-j/8.0/en/)
