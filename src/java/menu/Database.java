import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Database {

    private String dsn =
        "jdbc:mysql://localhost/"
        + "world"
        + "?user=maria"
        + "&password=P@ssw0rd";

    private Connection con = null;

    public void connect() {
        System.out.println("The connection string is:\n " + dsn);
        try {
            con = DriverManager.getConnection(dsn);
        } catch (SQLException ex) {
            printSQLException(ex);
        }
    }

    public void disconnect() {
        if (con == null) {
            System.out.println("You are not connected, please connect before disconnect.");
            return;
        }

        try {
            con.close();
            con = null;
        } catch (SQLException ex) {
            printSQLException(ex);
        }
    }

    public void showCountries(Integer limit) {
        String sql = "SELECT "
                   + "    code AS Code, "
                   + "    name AS Name, "
                   + "    region AS Region "
                   + "FROM country "
                   + "LIMIT ? ";

        try {
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, limit);
            ResultSet res = stm.executeQuery();
            printCountryTable(res);
        } catch (SQLException ex) {
            printSQLException(ex);
        }
    }

    public void searchCountries(String searchFor) {
        String sql = "SELECT "
                   + "    code AS Code, "
                   + "    name AS Name, "
                   + "    region AS Region "
                   + "FROM country "
                   + "WHERE "
                   + "    code LIKE ? "
                   + "    OR name LIKE ? "
                   + "    OR region LIKE ? ";
        searchFor = "%" + searchFor + "%";

        try {
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, searchFor);
            stm.setString(2, searchFor);
            stm.setString(3, searchFor);
            ResultSet res = stm.executeQuery();
            printCountryTable(res);
        } catch (SQLException ex) {
            printSQLException(ex);
        }
    }

    public void printCountryTable(ResultSet res) throws SQLException {
        String sep = "+------+---------------------------+---------------------------+";
        System.out.println(sep);
        System.out.printf("| %4s | %-25s | %-25s |\n",
            "Code",
            "Name",
            "Region"
        );
        System.out.println(sep);
        while(res.next()) {
            System.out.printf("| %-4s | %-25s | %-25s |\n",
                res.getString("Code"),
                res.getString("Name"),
                res.getString("Region")
            );
        }
        System.out.println(sep);
    }

    public void printSQLException(SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
    }
}
