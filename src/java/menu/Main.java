import java.util.Scanner;

public class Main {

    private static Database db;



    public static void main(String args[]) {
        System.out.println("Welcome to MAIN!\n");

        db = new Database();

        while (true) {
            printMenu();
        }
    }



    public static void printMenu() {
        String menu = "=== Menu ===\n"
                + "1) Connect\n"
                + "2) Disconnect\n"
                + "country) Show some countries\n"
                + "search)  Search through the countries\n"

                + "q) Quit\n"
                + "Select: ";
        Scanner sc= new Scanner(System.in);
        String str;

        System.out.print(menu);

        try  {
            String choice = sc.nextLine();
            switch(choice) {
                case "1":
                    System.out.println("Connecting.");
                    db.connect();
                    promptEnterKey();
                    break;

                case "2":
                    System.out.println("Disconnecting.");
                    db.disconnect();
                    promptEnterKey();
                    break;

                case "country":
                    System.out.println("Show some countries.");
                    System.out.print("How many: ");
                    String limitStr = sc.nextLine();

                    db.showCountries(Integer.parseInt(limitStr));
                    promptEnterKey();
                    break;

                case "search":
                    System.out.println("Search through the countries.");
                    System.out.print("Search string: ");
                    String searchFor = sc.nextLine();

                    db.searchCountries(searchFor);
                    promptEnterKey();
                    break;

                case "quit":
                case "q":
                    System.exit(0);
                    break;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    static public void promptEnterKey(){
        System.out.println("Press \"ENTER\" to continue...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
}
