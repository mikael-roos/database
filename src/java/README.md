How to connect Java to MySQL
============================

The basis for these exampe programs are the "[MySQL Connector/J 8.0 Developer Guide](https://dev.mysql.com/doc/connector-j/8.0/en/)".

These are the steps to get going with your own example program where you connect to a MySQL database from Java.



Precondition
----------------------------

You have installed MySQL server and it is up and running.



Prepare
----------------------------

Create a working directory, for example `connect`, move into it.

```
mkdir connect
cd connect
```



Get the jar file
----------------------------

There are instructions on how to [get and setup the jar file](https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-binary-installation.html). Here follows the short story.

Download the jar file from here.

* https://dev.mysql.com/downloads/connector/j/

Choose "Platform Independant" and choose the zip or tar distribution.

Move the file into your working directory. It could look like this if you choosed the zip file.

```
$ ls
mysql-connector-java-8.0.28.zip
```

Extract the zip archive file and the connector is available in the directory.

```
$ unzip mysql-connector-java-8.0.28.zip

$ ls -1
README.md
mysql-connector-java-8.0.28/
mysql-connector-java-8.0.28.zip

$ ls -l mysql-connector-java-8.0.28/mysql-connector-java-8.0.28.jar                               
-rw-r--r-- 1 mos mos 2.4M Dec 16 00:25 mysql-connector-java-8.0.28/mysql-connector-java-8.0.28.jar
```

I will move it to the root of my directory.

```
$ mv mysql-connector-java-8.0.28/mysql-connector-java-8.0.28.jar .  
desktop:~/git/mos/course/database/src/java/connect

$ ls -1
README.md
mysql-connector-java-8.0.28/
mysql-connector-java-8.0.28.jar  
mysql-connector-java-8.0.28.zip
```



Small java program that uses the database
----------------------------

This is a really small program that uses the database to show that it works.

You need to update the `dsn` variable to match a database, user and password in your own database.

```
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static void main(String args[]) {
        Connection conn = null;
        String dsn = "jdbc:mysql://localhost/"
            + "world"
            + "?user=maria"
            + "&password=P@ssw0rd";

        try {
            System.out.println("Lets connect to the database!");
            System.out.println("The connection string is:\n " + dsn);

            conn = DriverManager.getConnection(dsn);

            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
}
```


Save this into `Main.java`, compile, set the classpath and run.

```
# Compile
javac Main.java

# Set the classpath and run in one command
CLASSPATH="mysql-connector-java-8.0.28.jar:$CLASSPATH" java Main
```
