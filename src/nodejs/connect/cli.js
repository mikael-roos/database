/**
 * To verify that mysql is installed and is working.
 * Create a connection to the database and execute
 * a query without actually using the database.
 */
import db from './src/database.js'

(async () => {
    await db.selectOnePlusOne()
    process.exit()
})()
