/**
 * Connect and query the database.
 */
import mysql from 'promise-mysql'

export default {
  selectOnePlusOne
}

const config = {
    "host":     "localhost",
    "user":     "user",
    "password": "pass",
//    "database": "skolan"
}

const db = await mysql.createConnection(config)

process.on('exit', () => {
  db.end()
})


/**
 * Execute a simple query just to verify that the database connection works.
 *
 * @returns {Array} res
 */
async function selectOnePlusOne () {
  const sql = 'SELECT 1+1 AS Sum'
  console.info(`Executing query:\n${sql}`)

  const res = await db.query(sql)
  console.info(res)
  console.table(res)

  return res
}
