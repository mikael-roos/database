#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Perform queries to the database.

https://dev.mysql.com/doc/connector-python/en/
"""

import mysql.connector


class Database():
    """The database class."""

    config = {
      'user': 'user',
      'password': 'pass',
      'host': '127.0.0.1',
      'raise_on_warnings': True
    }

    def __init__(self):
        """Init the object."""
        self.cnx = None
        self.cursor = None

    def connect(self):
        """Connect to the database."""
        self.cnx = mysql.connector.connect(**self.config)
        self.cursor = self.cnx.cursor(prepared=True)

    def close(self):
        """Close the database connection."""
        self.cursor.close()
        self.cnx.close()

    def sum_one_plus_one(self):
        """Make a real trivial SELECT."""
        self.cursor.execute("SELECT 1+1 AS Sum")
        row = self.cursor.fetchone()
        print(f"* The sum is {row[0]}")

    def show_version(self):
        """Select the version of the database."""
        self.cursor.execute("SELECT VERSION() AS Version")
        row = self.cursor.fetchone()
        print(f"Database version: {row[0]}")

    def show_countries(self, limit):
        """Show all/some countries."""
        sql = """
            SELECT
                code AS Code,
                name AS Name,
                region AS Region
            FROM country
            LIMIT %s
        """
        args = (limit,)
        self.cursor.execute(sql, args)
        res = self.cursor.fetchall()

        print("{0:<5} {1:<20} {2:<20}".format(*self.cursor.column_names))
        print("-" * 60)
        for row in res:
            print(f"{row[0]:<5} {row[1]:<20} {row[2]:<20}")

    def search_countries(self, search):
        """Search through the countries."""
        sql = """
            SELECT
                code AS Code,
                name AS Name,
                region AS Region
            FROM country
            WHERE
                code LIKE %s
                OR name LIKE %s
                OR region LIKE %s
        """
        args = (
            search,
            '%' + search + '%',
            '%' + search + '%',
        )
        self.cursor.execute(sql, args)
        res = self.cursor.fetchall()

        print("{0:<5} {1:<20} {2:<20}".format(*self.cursor.column_names))
        print("-" * 60)
        for row in res:
            print(f"{row[0]:<5} {row[1]:<20} {row[2]:<20}")
