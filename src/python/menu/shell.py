#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Using the cmd module to create a shell for the main program.

You can read about the cmd module in the docs:
    cmd — support for line-oriented command interpreters
    https://docs.python.org/3/library/cmd.html
"""

import cmd
import database


class Shell(cmd.Cmd):
    """Example of class with command actions to perform database queries."""

    intro = 'Welcome to the database. Type help or ? to list commands.\n'
    prompt = '(db) '

    def __init__(self):
        """Init the object."""
        super().__init__()
        self.dbx = database.Database()
        self.dbx.config['database'] = 'world'
        self.dbx.connect()

    def do_sum(self, _):
        """Sum values using the database."""
        self.dbx.sum_one_plus_one()

    def do_version(self, _):
        """Show the version of the database."""
        self.dbx.show_version()

    def do_country(self, _):
        """Show some countries."""
        self.dbx.show_countries(7)

    def do_search(self, search):
        """Search through the countries."""
        msg = "Missing argument to search for."

        if not search:
            print(msg)
            return

        self.dbx.search_countries(search)

    def do_exit(self, _):
        # pylint: disable=no-self-use
        """Leave the program."""
        self.dbx.close()
        print("Bye bye - see ya soon again")
        return True

    def do_quit(self, arg):
        """Leave the program."""
        return self.do_exit(arg)

    def do_q(self, arg):
        """Leave the program."""
        return self.do_exit(arg)

    def do_EOF(self, arg):
        # pylint: disable=invalid-name
        """Leave the program."""
        return self.do_exit(arg)
