Python connect to MySQL
=======================

A simple program to connect to MySQL from Python and perform queries and print out the resultsets.

The documentation on the Python connector for MySQL can be reached here:

* https://dev.mysql.com/doc/connector-python/en/connector-python-examples.html

When working with the connector you will eventually need the reference documentation.

* https://dev.mysql.com/doc/connector-python/en/connector-python-reference.html

There might be some useful examples at w3schools to get you started.

* https://www.w3schools.com/python/python_mysql_getstarted.asp
