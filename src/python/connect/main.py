#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Connect to the MySQL database and execute a query.

The documentation on the Python connector for MySQL can be reached here:

* https://dev.mysql.com/doc/connector-python/en/connector-python-examples.html
* https://dev.mysql.com/doc/connector-python/en/connector-python-reference.html

There might be some useful examples at w3schools to get you started.

* https://www.w3schools.com/python/python_mysql_getstarted.asp
"""

import mysql.connector

config = {
  'user': 'user',
  'password': 'pass',
  'host': '127.0.0.1',
  'raise_on_warnings': True
}


def connect():
    """Connect to the database and execute some sample queries."""
    cnx = mysql.connector.connect(**config)
    print(cnx)
    cursor = cnx.cursor(dictionary=True)

    cursor.execute("SHOW DATABASES")
    print(cursor)

    for row in cursor:
        print(f"* {row['Database']}")

    cursor.execute("SELECT 1+1 AS Sum")
    print(cursor)

    for row in cursor:
        print(f"* The sum is {row['Sum']}\n")

    cursor.close()
    cnx.close()


if __name__ == '__main__':
    print(__doc__)
    connect()
