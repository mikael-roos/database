#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Connect to the MySQL database and execute a query using prepared statements.

This script let you remove one country from the world database.
"""


from mysql.connector import connect, Error

dsn = {
    "user": "maria",
    "password": "P@ssw0rd",
    "host": "127.0.0.1",
    "port": "3306",
    "database": "world",
    "raise_on_warnings": True,
}


def main():
    """Do example code."""
    try:
        with connect(**dsn) as cnx:

            # Create a cursor object for prepared statements
            cursor = cnx.cursor(prepared=True)

            # Execute the query
            sql = """
                DELETE FROM country
                WHERE
                    code = ?
            """
            print(f"# The SQL is:\n{sql}")

            code = input("Code of country to delete: ")
            args = (code,)
            print(f" the args are: {args}")
            cursor.execute(sql, args)

            # Check how many rows were affected
            print(f"Rows affected. {cursor.rowcount}")

            # Make sure data is committed to the database
            cnx.commit()

    except Error as err:
        print(err)


if __name__ == "__main__":
    print(__doc__)
    main()
