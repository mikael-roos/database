#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Connect to the MySQL database and execute a query using prepared statements.

The resultset contains a set of countries from the world database.
"""


from mysql.connector import connect, Error

dsn = {
    "user": "maria",
    "password": "P@ssw0rd",
    "host": "127.0.0.1",
    "port": "3306",
    "database": "world",
    "raise_on_warnings": True,
}


def main():
    """Do example code."""
    try:
        with connect(**dsn) as cnx:

            # Create a cursor object for prepared statements
            cursor = cnx.cursor(prepared=True)

            # Execute the query
            sql = """
                SELECT
                    code AS Code,
                    name AS Name,
                    region AS Region
                FROM country
                LIMIT %s
            """
            limit = 7
            args = (limit,)
            print(f"# The SQL is:\n{sql}")
            print(f" the args are: {args}")
            cursor.execute(sql, args)

            # Fetch the resultset
            res = cursor.fetchall()
            print("\n# Printing out the resultset")
            print(res)

            print("{0:<5} {1:<20} {2:<20}".format(*cursor.column_names))
            print("-" * 60)
            for row in res:
                print(f"{row[0]:<5} {row[1]:<20} {row[2]:<20}")

    except Error as err:
        print(err)


if __name__ == "__main__":
    print(__doc__)
    main()
