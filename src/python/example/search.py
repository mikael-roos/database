#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Connect to the MySQL database and execute a query using prepared statements.

This script lets you search though all the countries in the world
database and shows the rows that match your searchstring.
"""


from mysql.connector import connect, Error

dsn = {
    "user": "maria",
    "password": "P@ssw0rd",
    "host": "127.0.0.1",
    "port": "3306",
    "database": "world",
    "raise_on_warnings": True,
}


def main():
    """Do example code."""
    try:
        with connect(**dsn) as cnx:

            # Create a cursor object for prepared statements
            cursor = cnx.cursor(prepared=True)

            # Execute the query
            sql = """
                SELECT
                    code AS Code,
                    name AS Name,
                    region AS Region
                FROM country
                WHERE
                    code LIKE %s
                    OR name LIKE %s
                    OR region LIKE %s
                LIMIT %s
            """
            print(f"# The SQL is:\n{sql}")

            limit = 7
            while True:
                search = input("Search for (q to quit): ")
                if search == "q":
                    break

                args = (search, "%" + search + "%", "%" + search + "%", limit)
                print(f" the args are: {args}")
                cursor.execute(sql, args)

                # Fetch and print the resultset
                res = cursor.fetchall()
                print_resultset(res, cursor)

    except Error as err:
        print(err)


def print_resultset(res, cursor):
    """Print out resultset in a table."""
    print(f"\n# Printing out the resultset")
    print(res)

    print("{0:<5} {1:<20} {2:<20}".format(*cursor.column_names))
    print("-" * 60)
    for row in res:
        print(f"{row[0]:<5} {row[1]:<20} {row[2]:<20}")


if __name__ == "__main__":
    print(__doc__)
    main()
