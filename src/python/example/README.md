How to connect Python to MySQL
============================

This is a set of Python example programs that uses the connector `mysql-connector-python` to connect to a MySQL database.

You can [download this example as a zip file](../example.zip) to make it easy to use it on your own environment.

The example has been tested on Windows, Linux and Mac.

[[_TOC_]]



Video
---------------------------

The following video show when I work through parts of this article.

[![](http://img.youtube.com/vi/fBnYC2CzGYI/0.jpg)](http://www.youtube.com/watch?v=fBnYC2CzGYI "YouTube: Use Python to connect to a MySQL database")



Precondition
----------------------------

You have installed MySQL server and it is up and running.



Documentation
----------------------------

MySQL has the connector and you can read about it in "[MySQL Connector/Python Developer Guide](https://dev.mysql.com/doc/connector-python/en/)".

The package [`mysql-connector-python` is distributed as a PIP package](https://pypi.org/project/mysql-connector-python/).

You will find both [example programs](https://dev.mysql.com/doc/connector-python/en/connector-python-examples.html) and [reference documentation](https://dev.mysql.com/doc/connector-python/en/connector-python-reference.html) on the MySQL documentation site.

You can find some starter examples and information in the [W3Schools website on Python MySQL connection](https://www.w3schools.com/python/python_mysql_getstarted.asp).

There is a larger article "[Python and MySQL Database: A Practical Introduction](https://realpython.com/python-mysql/)" that can be helpful when learning how to write your code.



Database world
----------------------------

Parts of the examples uses the world database. [Download it and set it up](https://dev.mysql.com/doc/world-setup/en/world-setup-installation.html) to fully enjoy the examples.

You can verify that it worked by this.

```
$ mysql world -e "SHOW TABLES"
+-----------------+
| Tables_in_world |
+-----------------+
| city            |
| country         |
| countrylanguage |
+-----------------+
```



Connection details
----------------------------

To be able to connect to the database you need connection details. The example programs are set upp to use the following connection details.

* host = localhost
* user = maria
* pass = P@ssw0rd
* database = world
* port = 3306

You can edit the connection details in the example programs to match your environment or you can [create the user 'maria' using SQL](https://gitlab.com/mikael-roos/database/-/blob/main/sql/utility/create-user-maria.sql).

There is a [video showing how to create the 'maria' user in the database](https://youtu.be/YbycGfMA1XY), so check that out if you are unfamiliar with how to create a user.

You can verify that it works like this.

```
$ mysql -umaria -pP@ssw0rd -hlocalhost world -e "SELECT USER()"
+-----------------+
| USER()          |
+-----------------+  
| maria@localhost |
+-----------------+
```

These connection details are also sometimes called a [Data Source Name (DSN)](https://en.wikipedia.org/wiki/Data_source_name).



MySQL Connector Python package
----------------------------

The MySQL Connector Python connector needs to be installed by using PIP. There is a file `requirements.txt` that contains the package needed to be installed.

You can install like this (using Python 3)

```
$ python -m pip install -r requirements.txt
```

You can then check what version you got installed.

```
$ python3 -m pip list
Package                Version  
---------------------- -------  
mysql-connector-python 8.0.28   
```



Run connect.py
----------------------------

The first sample is a small program that just performs a connection to the database manager and print out some details.

Run it.

```
python connect.py
```

The output could look like this.

```
$ python3 connect.py
Connect to the MySQL database.
The database cursor object is:
<mysql.connector.connection_cext.CMySQLConnection object at 0x7f6ced9514e0>
```

An output like above means success.



Run select_one.py
----------------------------

This example program connects and executes a generic SQL SELECT query to the database.

The resultset only contains one row and one value.

Run it.

```
python select_one.py
```

The output could look like this.

```
$ python3 select_one.py
Connect to the MySQL database and execute a query using
prepared statements.

The resultset contains one row and one column.

# The SQL is:

SELECT 1+1 AS Sum

# Printing out the resultset
(2,)
The sum is 2
```

An output like above means success.



Run more examples
----------------------------

The other examples are setup the same way. Run them and inspect the output and the source code.



### select_many.py

This example uses prepared statements to execute a query that shows a subset of the countries in the world database.



### search.py

This examples let the user search through the countries in the world database by entering a string to search for.



### insert.py

This examples inserts a new country into the database.



### update.py

This examples updates the name and region of a specific country.



### delete.py

This examples deletes a country from the database.
