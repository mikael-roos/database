#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Connect to the MySQL database and execute a query using prepared statements.

The resultset contains one row and one column.
"""


from mysql.connector import connect, Error

dsn = {
    "user": "maria",
    "password": "P@ssw0rd",
    "host": "127.0.0.1",
    "port": "3306",
    "raise_on_warnings": True,
}


def main():
    """Do example code."""
    try:
        with connect(**dsn) as cnx:

            # Create a cursor object for prepared statements
            cursor = cnx.cursor(prepared=True)

            # Execute the query
            sql = "SELECT 1+1 AS Sum"
            print(f"# The SQL is:\n{sql}")
            cursor.execute(sql)

            # Fetch the resultset
            row = cursor.fetchone()
            print("\n# Printing out the resultset")
            print(row)
            print(f"The sum is {row[0]}")

    except Error as err:
        print(err)


if __name__ == "__main__":
    print(__doc__)
    main()
