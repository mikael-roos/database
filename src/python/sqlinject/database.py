#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Perform queries to the database.

https://dev.mysql.com/doc/connector-python/en/
"""

import mysql.connector


class Database():
    """The database class."""

    config = {
      'user': 'maria',
      'password': 'P@ssw0rd',
      'host': '127.0.0.1',
      'port': 3306,
      #'port': 33306,
      'raise_on_warnings': True
    }

    def __init__(self):
        """Init the object."""
        self.cnx = None
        self.cursor = None

    def connect(self):
        """Connect to the database."""
        self.cnx = mysql.connector.connect(**self.config)
        self.cursor = self.cnx.cursor(prepared=True)

    def close(self):
        """Close the database connection."""
        self.cursor.close()
        self.cnx.close()

    def show_version(self):
        """Select the version of the database."""
        self.cursor.execute("SELECT VERSION() AS Version")
        row = self.cursor.fetchone()
        print(f"Database version: {row[0]}")

    def show_students(self):
        """Show students."""
        sql = """
            SELECT
                name AS Name,
                email AS Email,
                program AS Program
            FROM student
            WHERE
                name != 'Admin'
        """
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        self.print_students(res)

    def search_students(self, search):
        """Search for students."""
        sql = """
            SELECT
                name AS Name,
                email AS Email,
                program AS Program
            FROM student
            WHERE
                name != 'Admin' AND
                (
        """
        sql = sql + "name LIKE '%" + search + "%' OR "
        sql = sql + "email LIKE '%" + search + "%' OR "
        sql = sql + "program LIKE '%" + search + "%' )"
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        self.print_students(res)

    def login_student(self, email, password):
        """Login as student."""
        sql = """
            SELECT
                name AS Name,
                email AS Email,
                password_hash AS Password
            FROM student
            WHERE
        """
        sql = sql + "email = '" + email + "' AND "
        sql = sql + "password_hash = md5('" + password + "')"
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        self.print_students(res)

    def print_students(self, res):
        """Print resultset for students."""
        print("{0:<20} {1:<20} {2:<20}".format(*self.cursor.column_names))
        print("-" * 60)
        for row in res:
            print(f"{row[0]:<20} {row[1]:<20} {row[2]:<20}")
