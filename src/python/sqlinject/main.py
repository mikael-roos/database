#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Do some queries towards the database and show the results."""

import shell


if __name__ == '__main__':
    print(__doc__)
    shell.Shell().cmdloop()
