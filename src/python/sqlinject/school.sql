--
-- School database
--
DROP DATABASE IF EXISTS school;
CREATE DATABASE school;
USE school;

DROP TABLE IF EXISTS student;
CREATE TABLE student (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20),
    email VARCHAR(20),
    password CHAR(8),
    password_hash CHAR(32),
    program VARCHAR(20)
);

DELETE FROM student;
INSERT INTO student
(name, email, password, password_hash, program)
VALUES
('Little Bobby', 'bobby@tables.com', 'bobby', md5('bobby'), 'Computer Science'),
('John Doe', 'john@doe.com', 'john', md5('john'), 'Unknown'),
('Jane Doe', 'jane@doe.com', 'jane', md5('jane'), 'Unknown'),
('Admin', 'admin@school.com', 'admin', md5('admin'), 'Administrator')
;
