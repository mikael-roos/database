#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Using the cmd module to create a shell for the main program.

You can read about the cmd module in the docs:
    cmd — support for line-oriented command interpreters
    https://docs.python.org/3/library/cmd.html
"""

import cmd
import database


class Shell(cmd.Cmd):
    """Example of class with command actions to perform database queries."""

    intro = 'Welcome to the database. Type help or ? to list commands.\n'
    prompt = '(db) '

    def __init__(self):
        """Init the object."""
        super().__init__()
        self.dbx = database.Database()
        self.dbx.config['database'] = 'school'
        self.dbx.connect()

    def do_version(self, _):
        """Show the version of the database."""
        self.dbx.show_version()

    def do_students(self, _):
        """Show students."""
        self.dbx.show_students()

    def do_search(self, search):
        """Search for students."""
        msg = "Missing argument to search for."

        if not search:
            print(msg)
            return

        self.dbx.search_students(search)

    def do_login(self, credential):
        """Login as student students."""
        msg = "Missing argument email and/or password."

        if not credential:
            print(msg)
            return

        creds = credential.split()
        if not len(creds) == 2:
            print(msg)
            return

        self.dbx.login_student(creds[0], creds[1])

    def do_exit(self, _):
        # pylint: disable=no-self-use
        """Leave the program."""
        self.dbx.close()
        print("Bye bye - see ya soon again")
        return True

    def do_quit(self, arg):
        """Leave the program."""
        return self.do_exit(arg)

    def do_q(self, arg):
        """Leave the program."""
        return self.do_exit(arg)

    def do_EOF(self, arg):
        # pylint: disable=invalid-name
        """Leave the program."""
        return self.do_exit(arg)
