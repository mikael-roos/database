The database modelling process
============================

There are three steps in database modelling for relational databases.

1. Conceptual modelling with an ER diagram, independent of database model
1. Logical modelling for the relational model
1. Physical modelling for a particular database vendor

This article also talkes about drawing tools and drawing notations.

[[_TOC_]]



Conceptual modelling - ER diagram
----------------------------



Logical modelling - Relational modelling
----------------------------



Physical modelling - Database vendor
----------------------------



Drawing tools
----------------------------

[Dia](http://dia-installer.de/) is a opensource desktop tool for drawing various diagrams. The [ER diagram](http://dia-installer.de/shapes/ER/index.html.en) (conceptual modelling) and the [Database diagram](http://dia-installer.de/shapes/Database/index.html.en) (Relation modelling aka Logical modelling) are included in the tool.

[DrawIO](https://app.diagrams.net/) is an online tool for drawing various diagrams. The tool is multi user so it can be helpful if you are working in teams.



ER notations
----------------------------

There exists several drawing notations on how to draw a ER diagram.
