---
revision: 
    "2024-01-30": "(A, mos) Enhanced and improved structure."
---
Exercise ER modelling: Bank
========================

![conceptual er diagram](./conceptual_er_diagram.png)

Draw an conceptual ER diagram of a database for a bank. Include the following.

1. Entities
1. Relations
1. Cardinality
1. Attributes
1. Candidate keys

[[_TOC_]]



Step 1 - The basics
------------------------

The bank has customers. Each customer has one account in the bank.

The bank saves details on each customer, such as name, age, address and social number.

Each account has an account number and a balance.



Step 2 - 1:N relation
------------------------

The customers in the bank can have several accounts.



Step 3 - Calculate and store interest
------------------------

Each account can have an interest.

The interest is calculated on a daily basis and stored. On a monthly basis the interest is added to the customer account.

You can view the solution as [Dia file](./solution_step3.dia) or as an image [solution_step3.png](./solution_step3.png).



<!--
Step 4 - N:M relation
------------------------

Step 5 - Type of account
------------------------

Step 6 - Transsaction log
------------------------

Ensure there are different keys:
* One thats unique
* Combined key
* Auto generated

-->
