Eshop
========================

The database needs to handle a customer register (customers with contact details), a product register (products with product code, name, short description and price) where each product is in one or more product categories.

The database also needs to contain a warehouse where you can see how many of each product are in the warehouse and a note about where the product is in the warehouse (which shelf). One and the same product can be spread over different shelves in the warehouse.

When the customer orders a product, an order is created that contains the customer's details together with which products have been ordered and its ordered number.

Based on the order, a pick list is created that can be sent to the warehouse for delivery. The pick list contains the same information as the order, but with the addition that each product line is mapped to a warehouse shelf so that the warehouse staff can see which shelf they can pick up the product on.

When the delivery is packed, an invoice is attached that has the same content as the order but now with the price per product line and the summed price.

There should be a log where you can see important events in the system, what happened, when it happened. This can be, for example, when an order / invoice was created or deleted.
