---
revision: 
    "2024-02-06": "(A, mos) First try."
---
Solution ER modelling: Library, logical model
========================

This is how a solution of the logical model can look like, when its done.

![solution](.img/er-step8.png)

_Figure. A solution of the ER diagram for the logical model._

[[_TOC_]]



7\. Modify the ER according the relational model
------------------------

This is the starting point when creating the logical model of the database.

![er tables](.img/er-step7-start.png)

_Figure. ER diagram with entities and relations converted to look like tables._

The first thing is to remove any N:M relations by adding a connection table.

![removed N:M](.img/er-step7.png)

_Figure. ER diagram with no N:M relations and connection tables added._

Other things we need to consider are these:

* No multi value attributes.
* Only one value in each cell
* Each row is unique
* No complex relations

Review the logical model and ensure it fullfills all criteria.



8\. Add primary and foreign keys (and more attributes)
------------------------

As the final step, review the primary and foreign keys and add any additional attributes.

![Add PK FK](.img/er-step8.png)

_Figure. ER diagram with primary and foreign keys._

Review the logical model a final time to ensure it fullfills all criteria being a relational logical model.
