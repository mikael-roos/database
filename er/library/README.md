---
revision: 
    "2024-02-12": "(B, mos) Added physical model."
    "2024-02-06": "(A, mos) Enhanced and improved structure."
---
Exercise ER modelling: Library
========================

The library stores books and has a database for all books. A book has an ISBN, title and author. Each book is categorized into one or several categories, a category can have several books. A category has a name, description and an id.

Details about the author are also stored, such as name, age and birth country. The author can write several books and each book can have one or several authors.

A publisher publishes books. Each book has one publisher. A publisher has a id, name and country. 

Create a database by working through the steps below.

[[_TOC_]]

<!--
TODO

Reenginenr the model to see how it looks like.

Put values into the tables to verify that the functions can be supproted.

Add utlåning to further enhance the model, som extrauppgift längst ned
 
1. Populate the database and assure that it works as expected.

Ensure there are different keys in the conceptual model:
* One thats unique
* Combined key
* Auto generated

Ensure 1:1, 1:N, N:M is included.

* Diagram finns
-->



Conceptual model
------------------------

Start by creating a conceptual model for the database.

1. Describe the datebase in text
1. Write down all entities (with attributes)
1. Write down all relations (matrix)
1. Draw a ER diagram with entities and relations
1. Add cardinality to the ER diagram
1. Add attributes and candidate keys to the ER diagram

There is a [solution proposal for the conceptual model](.img/er-step6.png) that you can peek into.

You can also review the "[Solution ER modelling: Library, conceptual model](./SOLUTION_conceptual.md)"



Logical model
------------------------

Then create a logical model for a relational database.

7\. Modify the ER according the relational model

8\. Add primary and foreign keys (and more attributes)

There is a [solution proposal for the logical model](.img/er-step8.png) that you can peek into.

You can also review the "[Solution ER modelling: Library, logical model](./SOLUTION_logical.md)".



Physical model
------------------------

Then create the physical model with DDL for your selected database.

9\. Create SQL DDL to create the tables

10\. List functions that the database should support

Then you might want to verify that the database actually works by putting in some values into it and try out a few statements.

You can review the "[Solution ER modelling: Library, physical model](./SOLUTION_physical.md)"

The SQL code to create the database and insert some values into it are in its bits and pieces in the directory [`sql/`](./sql/).
