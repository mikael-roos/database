---
revision: 
    "2024-02-06": "(A, mos) First try."
---
Solution ER modelling: Library, physical model
========================

This is how a solution of the physical model can look like, when its done.

<!--
![solution](.img/er-step9.png)

_Figure. A solution of the physical model with SQL DDL._
-->

[[_TOC_]]



9\. Create SQL DDL to create the tables
------------------------

One way to get going is to write down all tables and column names into the text editor, like this.

Save it all into a file like `ddl.sql`.

Start with a rough structure, like this, for each table.

```text
book
(
    id,
    isbn,
    title,
    publish_year,
    publisher_id
)
```

Then rewrite it with the datatypes, constraints and SQL structure. Add default values if suitable,

```sql
DROP TABLE IF EXISTS book;
CREATE TABLE book
(
    id INTEGER NOT NULL,
    isbn VARCHAR(17) NOT NULL,
    title VARCHAR(120) NOT NULL,
    publish_year INTEGER DEFAULT YEAR(NOW)),
    publisher_id INTEGER
)
```

Then add the primary, foreign keys and any unique constraints.

```sql
DROP TABLE IF EXISTS book;
CREATE TABLE book
(
    id INTEGER NOT NULL AUTO_INCREMENT,
    isbn VARCHAR(17) NOT NULL,
    title VARCHAR(120) NOT NULL,
    publish_year INTEGER,
    publisher_id INTEGER,

    PRIMARY KEY (id),
    FOREIGN KEY (publisher_id) REFERENCES publisher(id)
)
```

The file [`ddl-step-by-step.sql`](sql/ddl-step-by-step.sql) contains the steps on how to create the SQL DDL for the library database.

When you are done, organise it all in the file `ddl.sql` and then create your database.



10\. List functions that the database should support
------------------------

Create a list with the functions that your database should support, it does not need to be complete, it is just so you get an overview on what you want to do with the data.

1. Show all books, authors, publishers.
1. Show the books and how they are categorized.
1. Show the books an author has written.
1. Show the books a publisher has published.



Verify that the database works
------------------------

The file [`ddl.sql`](sql/ddl.sql) contains sample code to create the database. Execute it and verify that it works by showing the tables.

```
MariaDB [library]> SHOW TABLES;        
+-------------------+                  
| Tables_in_library |                  
+-------------------+                  
| author            |                  
| book              |                  
| book2author       |                  
| book2category     |                  
| category          |                  
| publisher         |                  
+-------------------+                  
```

You can then insert a few rows into the tables. The file [`insert.sql`](sql/insert.sql) contains code that helps you with that.

Check that the tables contain some data.

```
MariaDB [library]> SELECT * FROM book;                     
+----+-------+---------+--------------+--------------+     
| id | isbn  | title   | publish_year | publisher_id |     
+----+-------+---------+--------------+--------------+     
|  1 | isbn1 | Title 1 |         2024 |            1 |     
|  2 | isbn2 | Title 2 |         2024 |            2 |     
|  3 | isbn3 | Title 3 |         2024 |            3 |     
+----+-------+---------+--------------+--------------+     
```

```                                                           
MariaDB [library]> SELECT * FROM category;                 
+----+------------+-------------+                          
| id | name       | description |                          
+----+------------+-------------+                          
|  1 | Category 1 | NULL        |                          
|  2 | Category 2 | NULL        |                          
|  3 | Category 3 | NULL        |                          
+----+------------+-------------+                          
```

```
MariaDB [library]> SELECT * FROM author;                   
+----+----------+------------+---------------+             
| id | name     | birth_date | birth_country |             
+----+----------+------------+---------------+             
|  1 | Author 1 | 2024-02-12 | Sweden        |             
|  2 | Author 2 | 2024-02-12 | Norway        |             
|  3 | Author 3 | 2024-02-12 | Denmark       |             
+----+----------+------------+---------------+             
```

```
MariaDB [library]> SELECT * FROM publisher;                
+----+-------------+---------+                             
| id | name        | country |                             
+----+-------------+---------+                             
|  1 | Publisher 1 | Sweden  |                             
|  2 | Publisher 2 | Norway  |                             
|  3 | Publisher 3 | Denmark |                             
+----+-------------+---------+                             
```

```
MariaDB [library]> SELECT * FROM book2author;     
+---------+-----------+                           
| book_id | author_id |                           
+---------+-----------+                           
|       1 |         1 |                           
|       1 |         2 |                           
|       1 |         3 |                           
|       2 |         2 |                           
+---------+-----------+                           
```

```
MariaDB [library]> SELECT * FROM book2category;   
+---------+-------------+                         
| book_id | category_id |                         
+---------+-------------+                         
|       1 |           1 |                         
|       1 |           2 |                         
|       1 |           3 |                         
|       2 |           2 |                         
+---------+-------------+                         
```



Verify some reports
------------------------

Now when we have the database setup we can verify that some of the reports works. The example code is available in the file [`dml.sql`](sql/dml.sql).

* Show the books and how they are categorized.

```sql
SELECT 
    b.*,
    GROUP_CONCAT(c.name) AS 'Category' 
FROM book AS b
    LEFT OUTER JOIN book2category AS b2c
        ON b.id = b2c.book_id
    LEFT OUTER JOIN category AS c
        ON b2c.category_id = c.id
GROUP BY
    b.id
;
```

It looks like this when the statement is executed.

```
MariaDB [library]> SELECT
    ->     b.*,
    ->     GROUP_CONCAT(c.name) AS 'Category'
    -> FROM book AS b
    ->     LEFT OUTER JOIN book2category AS b2c
    ->         ON b.id = b2c.book_id
    ->     LEFT OUTER JOIN category AS c
    ->         ON b2c.category_id = c.id
    -> GROUP BY
    ->     b.id
    -> ;
+----+-------+---------+--------------+--------------+----------------------------------+
| id | isbn  | title   | publish_year | publisher_id | Category                         |
+----+-------+---------+--------------+--------------+----------------------------------+
|  1 | isbn1 | Title 1 |         2024 |            1 | Category 1,Category 2,Category 3 |
|  2 | isbn2 | Title 2 |         2024 |            2 | Category 2                       |
|  3 | isbn3 | Title 3 |         2024 |            3 | NULL                             |
+----+-------+---------+--------------+--------------+----------------------------------+
```

* Show the books an author has written.

```sql
SELECT 
    a.*,
    GROUP_CONCAT(b.id) AS 'Books' 
FROM author AS a
    LEFT OUTER JOIN book2author AS b2a
        ON a.id = b2a.author_id
    LEFT OUTER JOIN book AS b
        ON b2a.book_id = b.id
GROUP BY
    a.id
;
```

It looks like this when the statement is executed.

```
MariaDB [library]> SELECT                              
    ->     a.*,                                        
    ->     GROUP_CONCAT(b.id) AS 'Books'               
    -> FROM author AS a                                
    ->     LEFT OUTER JOIN book2author AS b2a          
    ->         ON a.id = b2a.author_id                 
    ->     LEFT OUTER JOIN book AS b                   
    ->         ON b2a.book_id = b.id                   
    -> GROUP BY                                        
    ->     a.id                                        
    -> ;                                               
+----+----------+------------+---------------+-------+ 
| id | name     | birth_date | birth_country | Books | 
+----+----------+------------+---------------+-------+ 
|  1 | Author 1 | 2024-02-12 | Sweden        | 1     | 
|  2 | Author 2 | 2024-02-12 | Norway        | 1,2   | 
|  3 | Author 3 | 2024-02-12 | Denmark       | 1     | 
+----+----------+------------+---------------+-------+ 
```

* Show the books a publisher has published.

```sql
SELECT 
    p.*,
    GROUP_CONCAT(b.id) AS 'Books' 
FROM publisher AS p
    LEFT OUTER JOIN book AS b
        ON p.id = b.publisher_id
GROUP BY
    p.id
;
```

It looks like this when the statement is executed.

```
MariaDB [library]> SELECT                   
    ->     p.*,                             
    ->     GROUP_CONCAT(b.id) AS 'Books'    
    -> FROM publisher AS p                  
    ->     LEFT OUTER JOIN book AS b        
    ->         ON p.id = b.publisher_id     
    -> GROUP BY                             
    ->     p.id                             
    -> ;                                    
+----+-------------+---------+-------+      
| id | name        | country | Books |      
+----+-------------+---------+-------+      
|  1 | Publisher 1 | Sweden  | 1     |      
|  2 | Publisher 2 | Norway  | 2     |      
|  3 | Publisher 3 | Denmark | 3     |      
+----+-------------+---------+-------+      
```



Summary
------------------------

This was how you can create the physical model of the database and verify that it works.
