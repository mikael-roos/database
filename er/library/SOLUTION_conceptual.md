---
revision: 
    "2024-02-06": "(A, mos) First try."
---
Solution ER modelling: Library, conceptual model
========================

This is how a solution of the conceptual model can look like, when its done.

![solution](.img/er-step6.png)

_Figure. A solution of the ER diagram for the conceptual model._

[[_TOC_]]



1\. Describe the datebase in text
------------------------

The library stores books and has a database for all books.

A book has an ISBN, title and author.

Each book is categorized into one or several categories, a category can have several books.

A category has a name, description and an id.

Details about the author are also stored, such as name, age and birth country.

The author can write several books and each book can have one or several authors.

A publisher publishes books. Each book has one publisher.

A publisher has a id, name and country.



2\. Write down all entities (with attributes)
------------------------

* book (isbn, title, author)
* category (name, description, id)
* author (name, age, birth_date, birth_country)
* publisher (id, name, country)



3\. Write down all relations (matrix)
------------------------

* book is categorized into one or several categories
    * category can have several books
* author can write several books
    * each book can have one or several authors
* publisher publishes books
    * each book has one publisher



4\. Draw a ER diagram with entities and relations
------------------------

This is how the first ER digram could look like.

![er with entities and relations](.img/er-step4.png)

_Figure. ER diagram with entities and relations._



5\. Add cardinality to the ER diagram
------------------------

Lets review the cardinality and add to the diagram.

* [N:M] book is categorized into one or several categories
    * category can have several books
* [N:M] author can write several books
    * each book can have one or several authors
* [1:N] publisher publishes books
    * each book has one publisher

![er with cardinality](.img/er-step5.png)

_Figure. ER diagram with cardinality added to the relations._



6\. Add attributes and candidate keys to the ER diagram
------------------------

Continue to add candidate keys and extra attributes (if needed).

* book (_id_, _isbn_, title, author)
* category (_name_, description, _id_)
* author (_id_, name, age, birth_date, birth_country)
* publisher (_id_, _name_, country)

Where to put the year when the book is published?

![er with candidate keys](.img/er-step6.png)

_Figure. ER diagram with candidate keys and extra attributes._
