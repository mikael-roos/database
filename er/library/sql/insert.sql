--
-- Library ddl
--

--
-- DELETE in order
--
DELETE FROM book2category;
DELETE FROM book2author;
DELETE FROM book;
DELETE FROM author;
DELETE FROM publisher;
DELETE FROM category;

--
-- Author
--
INSERT INTO author 
VALUES
    (1, 'Author 1', NOW(), 'Sweden'),
    (2, 'Author 2', NOW(), 'Norway'),
    (3, 'Author 3', NOW(), 'Denmark')
;

--
-- Publisher
--
INSERT INTO publisher 
VALUES
    (1, 'Publisher 1', 'Sweden'),
    (2, 'Publisher 2', 'Norway'),
    (3, 'Publisher 3', 'Denmark')
;

--
-- Category
--
INSERT INTO category 
    (id, name)
VALUES
    (1, 'Category 1'),
    (2, 'Category 2'),
    (3, 'Category 3')
;

--
-- Book
--
ALTER TABLE book AUTO_INCREMENT=1;

INSERT INTO book 
    (isbn, title, publisher_id)
VALUES
    ('isbn1', 'Title 1', 1),
    ('isbn2', 'Title 2', 2),
    ('isbn3', 'Title 3', 3)
;

--
-- book2category
--
INSERT INTO book2category 
VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (2, 2)
;

--
-- book2author
--
INSERT INTO book2author 
VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (2, 2)
;

