--
-- Library ddl
--

--
-- DROP all tables
--
DROP DATABASE IF EXISTS library;
CREATE DATABASE library;
USE library;



--
-- DROP all tables
--
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS publisher;
DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS book;
DROP TABLE IF EXISTS book2author;
DROP TABLE IF EXISTS book2category;



--
-- CREATE all tables
--
CREATE TABLE author
(
    id INTEGER NOT NULL,
    name VARCHAR(80) NOT NULL,
    birth_date DATE,
    birth_country VARCHAR(120),

    PRIMARY KEY (id)
);

CREATE TABLE publisher
(
    id INTEGER NOT NULL,
    name VARCHAR(120) NOT NULL,
    country VARCHAR(120),

    PRIMARY KEY (id)
);

CREATE TABLE category
(
    id INTEGER NOT NULL,
    name VARCHAR(40) NOT NULL,
    description VARCHAR(120),

    PRIMARY KEY (id),
    UNIQUE (name)
);

CREATE TABLE book
(
    id INTEGER AUTO_INCREMENT NOT NULL,
    isbn VARCHAR(17),
    title VARCHAR(120),
    publish_year INTEGER DEFAULT YEAR(NOW()),
    publisher_id INTEGER,

    PRIMARY KEY (id),
    FOREIGN KEY (publisher_id) REFERENCES publisher (id)
);

CREATE TABLE book2category
(
    book_id INTEGER NOT NULL,
    category_id INTEGER NOT NULL,

    PRIMARY KEY (book_id, category_id),
    FOREIGN KEY (book_id) REFERENCES book (id),
    FOREIGN KEY (category_id) REFERENCES category (id)
);

CREATE TABLE book2author
(
    book_id INTEGER NOT NULL,
    author_id INTEGER NOT NULL,

    PRIMARY KEY (book_id, author_id),
    FOREIGN KEY (book_id) REFERENCES book (id),
    FOREIGN KEY (author_id) REFERENCES author (id)
);
