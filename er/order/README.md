Order with products
==========================

The company has customers (name, id, city) which places orders (date, status, id) which contains products (id, name, price).

A order contains several products. The customer can order 1 or several items of each product within the same order.


* customer (name, *id, city)
* order (date, status, *id)
* product (*id, name, price)

* customer places many orders 1:N
* order contains many products, a product can be in several orders N:M


### Move to logical model

* Replace N:M relations
* Add PK/FK

* Draw in DIA

customer (
    name VARCHAR(50),
    id INTEGER,
    city VARCHAR(50)
)
order (
    date DATE,
    status VARCHAR(20),
    id INTEGER
)
product (
    id INTEGER,
    name VARCHAR(50),
    price DECIMAL
)

### SQL
USE company;

DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
    name VARCHAR(50),
    id INTEGER,
    city VARCHAR(50),
    
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
    date DATE,
    status VARCHAR(20),
    id INTEGER,
    customerId INTEGER,
    
    PRIMARY KEY (id),
    FOREIGN KEY (customerId) REFERENCES customer (id)
);

DROP TABLE IF EXISTS product;
CREATE TABLE product (
    id INTEGER,
    name VARCHAR(50),
    price DECIMAL,
    
    PRIMARY KEY (id)
);

SELECT * FROM customer;
SELECT * FROM `order`;
SELECT * FROM product;
