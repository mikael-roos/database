A flight system
==========================

An airplane is assigned to several flights. A flight is assigned to only one airplane.

A pilot can perform several flights. A flight is performed by several (normally at least two) pilots.

Each flight is connected to at least two airports, the departure and the arrival.

pilot name, number
flight number, time, duration, arrivaltime
airplane code, type
