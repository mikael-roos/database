-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema movie
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `movie` DEFAULT CHARACTER SET latin1 ;
USE `movie` ;

-- -----------------------------------------------------
-- Table `movie`.`collection`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`collection` (
  `name` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `movie`.`director`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`director` (
  `name` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `movie`.`movie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`movie` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(80) NULL DEFAULT NULL,
  `plot` VARCHAR(200) NULL DEFAULT NULL,
  `year` INT(11) NULL DEFAULT NULL,
  `collection_name` VARCHAR(80) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `collection_name` (`collection_name` ASC) VISIBLE,
  CONSTRAINT `movie_ibfk_1`
    FOREIGN KEY (`collection_name`)
    REFERENCES `movie`.`collection` (`name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `movie`.`director2movie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`director2movie` (
  `director_name` VARCHAR(80) NOT NULL,
  `movie_id` INT(11) NOT NULL,
  PRIMARY KEY (`director_name`, `movie_id`),
  INDEX `movie_id` (`movie_id` ASC) VISIBLE,
  CONSTRAINT `director2movie_ibfk_1`
    FOREIGN KEY (`director_name`)
    REFERENCES `movie`.`director` (`name`),
  CONSTRAINT `director2movie_ibfk_2`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie`.`movie` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `movie`.`genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`genre` (
  `name` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `movie`.`movie2genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`movie2genre` (
  `genre_name` VARCHAR(80) NOT NULL,
  `movie_id` INT(11) NOT NULL,
  PRIMARY KEY (`genre_name`, `movie_id`),
  INDEX `movie_id` (`movie_id` ASC) VISIBLE,
  CONSTRAINT `movie2genre_ibfk_1`
    FOREIGN KEY (`genre_name`)
    REFERENCES `movie`.`genre` (`name`),
  CONSTRAINT `movie2genre_ibfk_2`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie`.`movie` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `movie`.`rating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`rating` (
  `movie_id` INT(11) NOT NULL,
  `imdb` DOUBLE(2,1) NULL DEFAULT NULL,
  `own` DOUBLE(2,1) NULL DEFAULT NULL,
  PRIMARY KEY (`movie_id`),
  CONSTRAINT `rating_ibfk_1`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie`.`movie` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `movie`.`trailer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `movie`.`trailer` (
  `title` VARCHAR(80) NOT NULL,
  `url` VARCHAR(120) NULL DEFAULT NULL,
  `movie_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`title`),
  INDEX `movie_id` (`movie_id` ASC) VISIBLE,
  CONSTRAINT `trailer_ibfk_1`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie`.`movie` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
