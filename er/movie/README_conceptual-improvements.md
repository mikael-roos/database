My movie database
============================

Improve the ER diagram.

[[_TOC_]]



Entities
----------------------------

### Weak entity types



Relationships
----------------------------

### Ternary relationship
### Recursive relationship
### Constraints
#### Participation



Cardinality
----------------------------

1:1
0:1
1:N
0:N
M:N



Attributes
----------------------------

### Multi valued attributes
### Composite attributes
### Attribute on relationship


Attribute keys
----------------------------

If there is one attribute that makes an entry in an entity unique, then draw an underline of that attribute.

If there are several attributes that need to be combined to make an entry unique, then mark those with a dotted underline. "weak key"
