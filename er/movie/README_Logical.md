My movie database
============================

Logical modelling according to the relational model and physical modelling to a relational database like MariaDB or MySQL.

This is an exercise in ER modelling and this part relates to the logical modelling where a diagram according to the relational model together with DDL SQL can be a result.

[[_TOC_]]



The starting point after the conceptual modelling
----------------------------

The following entities and attributes with candidate keys.

* movie (title, plot, release year)
* rating (imdb, own)
* director (_name_)
* trailer (url, _name_)
* collection (_name_)
* genre (_name_)

Relationships.

* M:N movie has one or several directors that directed
    * a director can direct several movies
* 1:N trailers that presents the movie
    * each trailer belongs to a specific movie
* 1:N movie can belong to a collection of movies
    * a collection has several movies
* 1:1 movie has a rating
    * a rating is connected to a movie
* M:N movie belongs to one or several genre
    * a genre can exist on several movies



7\. Modifiera ER-diagram enligt relationsmodellen
----------------------------

8\. Utöka ER-diagram med primära/främmande nycklar samt kompletterande attribut
----------------------------

* movie (#id, title, plot, release year, _collection_name_)
* rating (#movie_id, imdb, own)
* director (#name)
* director2movie (#director_name, #movie_id)
* trailer (#title, url, _movie_id_)
* collection (#name)
* genre (#name)
* movie2genre (#genre_name, #movie_id)



9\. Fysisk modellering: Skapa SQL DDL för tabellerna
----------------------------

```
DROP TABLE IF EXISTS movie;
DROP TABLE IF EXISTS rating;
DROP TABLE IF EXISTS director;
DROP TABLE IF EXISTS director2movie;
DROP TABLE IF EXISTS trailer;
DROP TABLE IF EXISTS collection;
DROP TABLE IF EXISTS genre;
DROP TABLE IF EXISTS movie2genre;

CREATE TABLE movie (
    id INT AUTO_INCREMENT,
    title VARCHAR(80),
    plot VARCHAR(200),
    year INT,
    collection_name VARCHAR(80)

    PRIMARY KEY (id),
    FOREING KEY collection_name REFERENCES collection(name)
);

CREATE TABLE rating (
    movie_id INT,
    imdb REAL(2,1),
    own REAL(2,1),

    PRIMARY KEY(movie_id),
    FOREIGN KEY movie_id REFERENCES movie(id)
);

CREATE TABLE director (
    name VARCHAR(80) PRIMARY KEY
);

CREATE TABLE director2movie (
    director_name VARCHAR(80),
    movie_id INT,

    PRIMARY KEY (director_name, movie_id),
    FOREIGN KEY director_name REFERENCES director(name),
    FOREIGN KEY movie_id REFERENCES movie(id)
);

CREATE TABLE trailer (
    title VARCHAR(80),
    url VARCHAR(120),
    movie_id INT,

    PRIMARY KEY (title),
    FOREIGN KEY movie_id REFERENCES movie(id)
);

CREATE TABLE collection (
    name VARCHAR(80) PRIMARY KEY
);

CREATE TABLE genre (
    name VARCHAR(80) PRIMARY KEY
);

CREATE TABLE movie2genre (
    genre_name VARCHAR(80),
    movie_id INT,

    PRIMARY KEY (genre_name, movie_id),
    FOREIGN KEY genre_name REFERENCES genre(name),
    FOREIGN KEY movie_id REFERENCES movie(id)
);
```




10\. Lista funktioner som databasen skall stödja (API)
----------------------------
