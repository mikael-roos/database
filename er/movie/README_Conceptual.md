My movie database
============================

Conceptual modelling.

This is an exercise in ER modelling and this part relates to the conceptual modelling where an ER diagram can be a result.

[[_TOC_]]



1\. Text
----------------------------

The database shall manage movies and details on them. Details can for example be the title, the plot, the release year.

Each movie has one or several directors that directed the movie. The director has a name.

There can be trailers that presents the movie. These are video recordings that are available online though a web link.

A movie can belong to a collection of movies. For example, the movies about Harry Potter is a collection of eight movies.

The movie has a rating with various ratings of the movie. One rating is the IMDB rating and another rating is my own rating.

Each movie belongs to one or several genre.

EXTRA.

I want to have a watchlist where I can store the movies I want to see. Each movie has a priority number on how urgent it is to see it.

For each movie I can add a review of it. It should be possible to add several reviews for a movie, I might change my view the second time I look at the movie.



2\. Entities
----------------------------

From the text above I extract the entites, this is what to be stored in the database.

* movie
* director
* trailer
* collection
* rating
* genre

EXTRA

* watchlist
* review



3\. Relationships
----------------------------

From the text above I try to find the most important relationship between the entities. There might be many relationships so I try to note them all and then I can select the most important ones later on.

* movie has one or several directors that directed
* trailers that presents the movie
* movie can belong to a collections of movies
* movie has a rating
* movie belongs to one or several genre

EXTRA

* watchlist where I can store the movies
* movie I can add a review of




4\. Cardinality
----------------------------

For each relationship, label it with a cardinality. Start with a limited selection to describe the cardinality of the relationship with these three alternatives.

* 1:1 (One to One)
* 1:N (One to Many)
* M:N (Many to Many)

Try to elaborate abound the relationships do decide what cardinality it has.

* M:N movie has one or several directors that directed
    * a director can direct several movies
* 1:N trailers that presents the movie
    * each trailer belongs to a specific movie
* 1:N movie can belong to a collection of movies
    * a collection has several movies
* 1:1 movie has a rating
    * a rating is connected to a movie
* M:N movie belongs to one or several genre
    * a genre can exist on several movies

EXTRA

What would the cardinality be for the following?

* watchlist where I can store the movies
* movie I can add a review of



5\. Attributes
----------------------------

From the text above I try to extract the most important attributes for each entity. I can add attributes even if they are not menchened in the text. However, try to avoid adding to much "fancy and not needed attributes", keep it clean and only add the important attributes.

* movie (title, plot, release year)
* director (name)
* trailer (url)
* collection (name)
* rating (imdb, own)
* genre (name)

EXTRA

* watchlist (prio)
* review (comment, date)



6\. Attribute keys
----------------------------

An entity should have a key attribute which uniquely identifies each entity in the entity set.

If there is one attribute, or a combination of attributes, that makes an entry in an entity unique, then mark them as a possible attribute key.

You might want to add more attributes to find a key attribute for each entity.

* director (_name_)
* trailer (url, _name_)
* collection (_name_)
* genre (_name_)

You can also leave the entities without a key, if no attribute nor combination of attributes make an entry unique in the set. We can leave this to be solved at a later stage.

* movie (title, plot, release year)
* rating (imdb, own)



7\. The resulting ER diagram
----------------------------

Now you have the details to drawn an Entity Relationship diagram.
