University school database
==========================

The school needs an administration system for the courses it provides. The courses are held at course occasions all throughout the year. Each course has students registered to the course. The students can take many courses at the same time. The courses belong to education programs. There are a handful of education programs at the school. A student gets a grade from each course.

https://drive.google.com/file/d/0ByJ-Zb8R9RVmb2Y4enlVU2w4SVE/view?usp=sharing&resourcekey=0-wkJnal7qsqDGNbbkzmNbOw
