Company invoices
==========================

The company produces invoices. The invoice contains products and the amount of products that are sold. An invoice can contain several different products. Each row in the invoice has a sum and the invoice contains a total sum and some tax. An invoice is connected to a customer and a customer can have many invoices.
