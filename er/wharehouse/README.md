A wharehouse
==========================

The company is doing business as a large warehouse. The warehouse area is divided into several buildings and each building has shelfs. On each shelf they store products. They need to keep track of the amount of the specific product stored on each shelf. There can only be one specific product on one shelf at the time. A product can be spread over several shelves.
