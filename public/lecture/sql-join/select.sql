--
-- Create a sample world database called "earth" for the
-- slides of SQL
--
DROP DATABASE IF EXISTS earth;
CREATE DATABASE earth;

USE earth;

--
-- Create the country table
--
DROP TABLE IF EXISTS country;
CREATE TABLE country (
    `code` CHAR(3) NOT NULL DEFAULT '',
    `name` CHAR(52) NOT NULL DEFAULT '',
    `continent` ENUM('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL DEFAULT 'Asia',
    `surface_area` FLOAT(10,2) NOT NULL DEFAULT '0.00',
    `indep_year` SMALLINT DEFAULT NULL,
    `population` INT NOT NULL DEFAULT '0',
    `life_expectancy` FLOAT(3,1) DEFAULT NULL,
    `capital` INT DEFAULT NULL,
    PRIMARY KEY (`code`)
);

SHOW CREATE TABLE country \G;

--
-- Insert some countries
--
-- +------+-----------+---------------+-------------+-----------+------------+----------------+---------+
-- | Code | Name      | Continent     | SurfaceArea | IndepYear | population | LifeExpectancy | Capital |
-- +------+-----------+---------------+-------------+-----------+------------+----------------+---------+
-- | DNK  | Denmark   | Europe        |    43094.00 |       800 |    5330000 |           76.5 |    3315 |
-- | FIN  | Finland   | Europe        |   338145.00 |      1917 |    5171300 |           77.4 |    3236 |
-- | GRL  | Greenland | North America |  2166090.00 |      NULL |      56000 |           68.1 |     917 |
-- | ISL  | Iceland   | Europe        |   103000.00 |      1944 |     279000 |           79.4 |    1449 |
-- | NOR  | Norway    | Europe        |   323877.00 |      1905 |    4478500 |           78.7 |    2807 |
-- | SWE  | Sweden    | Europe        |   449964.00 |       836 |    8861400 |           79.6 |    3048 |
-- +------+-----------+---------------+-------------+-----------+------------+----------------+---------+

DELETE FROM country;
INSERT INTO country
    (`code`, `name`, `surface_area`, `indep_year`)
VALUES
    ('SWE', 'Sweden', 449964.00, 836),
    ('DNK', 'Denmark', 43094.00, 800),
    ('NOR', 'Norway', 323877.00, 1905),
    ('ISL', 'Iceland', 103000.00, 1944),
    ('FIN', 'Finland', 338145.00, 1917),
    ('GRL', 'Greenland', 2166090.00, NULL)
;

SELECT code, name, surface_area, indep_year FROM country;

SELECT code, name, continent FROM country;

UPDATE country SET continent = 'Europe';

UPDATE country
SET
    continent = 'North America'
WHERE
    name = 'Greenland'
;

SELECT code, name, population, life_expectancy FROM country;

UPDATE country SET
    population = 8861400,
    life_expectancy = 79.6
WHERE
    code = 'SWE'
;

UPDATE country SET
    population = 5330000,
    life_expectancy = 76.5
WHERE
    code = 'DNK'
;

UPDATE country SET population = 4478500, life_expectancy = 78.7 WHERE code = 'NOR';
UPDATE country SET population = 279000, life_expectancy = 79.4 WHERE code = 'ISL';
UPDATE country SET population = 5171300, life_expectancy = 77.4 WHERE code = 'FIN';
UPDATE country SET population = 56000, life_expectancy = 68.1 WHERE code = 'GRL';

SELECT code, name, population, life_expectancy FROM country;

-- Show all columns in the table
SELECT * FROM country;

-- Show some columns in the table
SELECT code, name FROM country;

-- Show some columns and give them a name for visibility
SELECT
    code AS 'Country code',
    name AS 'Name'
FROM country;

-- Show rows where column match a criteria
SELECT code, name, population
FROM country
WHERE population < 1000000;

-- Match condition for several columns
SELECT code, name, population
FROM country
WHERE
    name LIKE '%land'
    AND population > 100000;

-- Order by column values
SELECT code, name, population, life_expectancy
FROM country
WHERE continent = 'Europe'
ORDER BY life_expectancy DESC;

-- LIMIT
SELECT code, name, population, life_expectancy
FROM country
WHERE continent = 'Europe'
ORDER BY life_expectancy DESC
LIMIT 2;

-- Aggregating functions
SELECT COUNT(code) AS 'Number of countries' FROM country;

SELECT AVG(life_expectancy) AS 'Average life expectancy'
FROM country;

SELECT SUM(population) AS 'Tot population'
FROM country
WHERE continent = 'Europe';

-- GROUP BY
SELECT
    continent,
    AVG(life_expectancy) AS 'Average life expectancy'
FROM country
GROUP BY continent;

SELECT
    continent,
    COUNT(code) AS 'Number of countries'
FROM country
GROUP BY continent;

-- Built in functions
SELECT
    continent,
    ROUND(AVG(life_expectancy), 1) AS 'Average life expectancy'
FROM country
GROUP BY continent;

SELECT
    CONCAT(name, ' (', code, ')') AS Country
FROM country;

--
-- Remove row from the table
--
DELETE FROM country
WHERE code IN ('SWE', 'DNK');

DELETE FROM country
WHERE name LIKE '%land'
LIMIT 1;

DELETE FROM country;
