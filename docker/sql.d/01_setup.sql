--
-- Setup a user, create a database and grant access for the user
-- to the database.
--

-- Create databases if they do not exists
CREATE DATABASE IF NOT EXISTS name;
CREATE DATABASE IF NOT EXISTS classicmodels;
CREATE DATABASE IF NOT EXISTS company;
CREATE DATABASE IF NOT EXISTS world;
